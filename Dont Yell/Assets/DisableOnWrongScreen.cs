﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisableOnWrongScreen : MonoBehaviour {

	public GameObject myChild;
	public SupportCamMovement supCam;
	// Use this for initialization
	void Start () {

	}
	
	public void DisableObject()
	{
		myChild.SetActive(false);
	}

	public void EnableObject()
	{
		myChild.SetActive(true);
	}
}

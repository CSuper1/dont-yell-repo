﻿

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Tutorial4 : MonoBehaviour
{

	//public PilotTextRotater pilotText;
	public Text pilotText;
	public Text supportText;
	public SupportCamMovement supCam;
	public WeaponChangeScript wc;
	public GameObject[] enemies;

	private int pilotTextPos = 0;

	private string[] supWords =     {"Some enemies in the game can only be affected by certain types of damage from your laser.",
									"Go to the weapon changing system.",
									"Currently, the blue laser is equipped. Your pilot will ask for different colored lasers."};

	private string[] pilotWords =   {"Some enemies in the game can only be affected by certain types of damage from your laser.",
									"Currently, the blue laser is equipped. Ask your support to change your weapon colors."};
	private int stateChanger = 0;

	private bool showedPilText, showedSupText;
	// Use this for initialization
	void Start()
	{
		wc.changeWeapon(4);
	}

	// Update is called once per frame
	void FixedUpdate()
	{
		tutorialMachine();
		/*if (Input.GetKey(KeyCode.Alpha0))
        {
            SceneManager.LoadScene(0);
        }*/
	}

	private void tutorialMachine()
	{

		switch (stateChanger)
		{
			case 0:
				if (showedPilText == false)
				{
					supportText.text = supWords[0];
					showedSupText = true;
					pause();
					InvokeRepeating("pilotTimer", 0.0f, 7.0f);
				}

				break;

			case 1:

				// This handles the support side text
				if (showedSupText == false)
				{
					supportText.text = supWords[1];
					showedSupText = true;

				}

				// When the support transitions to the weaponchange panel, continue the state machine.
				if (supCam.weaponChanging)
				{
					stateChanger++;
					showedSupText = false;
				}
				break;

			case 2:
				if (showedSupText == false)
				{
					supportText.text = supWords[1];
					showedSupText = true;
					pause();
				}
				break;

			case 3:
				if (showedSupText == false)
				{
					supportText.text = "";
					pilotText.text = "";
					showedSupText = true;
					pause();
				}

				if (enemies[0] == null && enemies[1] == null && enemies[2] == null)
					SceneManager.LoadSceneAsync("levelMenu");

				break;
			default:
				Debug.LogError("tutorialMachine in Tutorial2 should not be reaching a default case " + stateChanger);
				break;
		}


	}

	// Point of this is to have a uniform changing of the scene while still giving the support and pilot some time to recover

	private IEnumerator pause()
	{

		yield return new WaitForSeconds(8f);
		showedSupText = false;
		stateChanger++;
	}

	private IEnumerator pilotTimer()
	{
		changePilotText(pilotTextPos);
		pilotTextPos++;
		// Stop this at the end; the last phrase just tells the pilot to wait for support
		if (pilotTextPos > pilotWords.Length - 1)
			CancelInvoke();
		yield return new WaitForSeconds(7f);
	}

	private void changePilotText(int newtext)
	{
		//pilotText.resetRotation();
		//pilotText.setText(pilotWords[newtext]);
		//StartCoroutine(pilotText.rotate(5f));
		pilotText.text = pilotWords[newtext];
		showedPilText = true;
	}
}

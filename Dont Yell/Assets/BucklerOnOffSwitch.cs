﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BucklerOnOffSwitch : MonoBehaviour {

    public MeshCollider col;
    public MeshRenderer ren;

	// Use this for initialization
	void Start () {
        setOn();
	}
	
	// Update is called once per frame
	void Update () {

	}
    
    public void setOff()
    {
        col.enabled = false;
        ren.enabled = false;
    }

    public void setOn()
    {
        col.enabled = true;
        ren.enabled = true;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class AnimationSpeed : MonoBehaviour {

	public Animator anim;
	private float animationSpeed = 0;
	// Use this for initialization
	void Start () {
		anim.speed = animationSpeed;
	}

	public void setSpeed(float animationSpeed)
	{
		anim.speed = animationSpeed;
		
	}
}

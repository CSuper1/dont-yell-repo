﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class tutorial3SupportText : MonoBehaviour {
	Text message;
	// Use this for initialization
	void Start () {
		message = gameObject.GetComponent<Text>();
		message.color = Color.black;
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKeyDown(KeyCode.D)){
			message.color = Color.white;
		}
	}
}

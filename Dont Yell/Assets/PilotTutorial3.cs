﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PilotTutorial3 : MonoBehaviour {
	Text message;
	// Use this for initialization
	void Start () {
		message = gameObject.GetComponent<Text>();
		message.text = "1.When support asks, tell them the order of colors found in middle panel. \n 2.When given the signal, point the right controller up to change weapon.";
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}

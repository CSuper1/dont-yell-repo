﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PilotTextRotater : MonoBehaviour {
	float percentage = 0;
	float radius = .002f;
	float turnTime = 3;
	public Canvas textCanvas;
	public Text myText;
	Vector3 initialPos;
	Quaternion initialRot;
	private bool spinning = false;

	// Use this for initialization
	void Start () {
		initialPos = transform.position;
		initialRot = textCanvas.transform.rotation;
	}

	public void rotate()
	{
		StartCoroutine("rotate90Left");
	}

	public IEnumerator rotate(float waitSeconds)
	{
		resetRotation();
		yield return new WaitForSeconds(waitSeconds);
		rotate();
	}

	private IEnumerator rotate90Left()
	{
		spinning = true;
		float time = 0;
		percentage = 0;
		while(time < turnTime)
		{
			percentage = time / turnTime;
			time += Time.deltaTime;
			float x = Mathf.Cos(percentage) * radius;
			float z = Mathf.Sin(percentage) * radius;
			transform.position = new Vector3(transform.position.x + x, transform.position.y, transform.position.z + z);
			textCanvas.transform.rotation = Quaternion.Euler(textCanvas.transform.rotation.x, (percentage * -90) - 180, textCanvas.transform.rotation.z);
			yield return null;
		}

		spinning = false;
	}

	public void resetRotation()
	{
		transform.position = initialPos;
		textCanvas.transform.rotation = initialRot;
	}

	public void setText(string newText)
	{
		myText.text = newText;
	}
}

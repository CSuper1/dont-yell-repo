﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class powerLegs : MonoBehaviour {

    public PoweredComponent poweredcomponent;
    public float speed;
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        if (poweredcomponent.power == 0)
        {
            speed = 1;
        }
        else if (poweredcomponent.power == 1)
            speed = 2.0f;
        else if (poweredcomponent.power == 2)
            speed = 3.5f;
        else if (poweredcomponent.power == 3)
            speed = 5.0f;

		if (poweredcomponent.heat >= 95)
			speed = 0;
    }
}

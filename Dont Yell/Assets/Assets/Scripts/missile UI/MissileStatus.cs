﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class MissileStatus : MonoBehaviour
{
    public bool missileReady, loadMissile;
    public MissileUIController mc;
    public GuidanceLaserLogic gl;
    public GameObject myTarget;

    // Use this for initialization
    public void Start()
    {
        myTarget = null;
    }

    // fire the missile
    public void releaseTheLittleKraken()
    {
        missileReady = false;
        myTarget = null;
    }

    public void startLoadingMissile()
    {
        if (mc.doorsOpen)
            return;
        myTarget = gl.getTarget();
        missileReady = true;
    }
}

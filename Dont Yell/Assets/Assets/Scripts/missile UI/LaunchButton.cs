﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaunchButton : MonoBehaviour {

	List<MissileStatus> tubes = new List<MissileStatus>();
	// Use this for initialization
	void Start () {
		foreach(GameObject i in GameObject.FindGameObjectsWithTag("UITube"))
		{
			tubes.Add(i.GetComponent<MissileStatus>());
		}
		//Debug.Log(tubes.Count);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void releaseTheBigKraken()
	{
		foreach(MissileStatus littleKraken in tubes)
		{
			littleKraken.releaseTheLittleKraken();
		}
	}
}

﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class NewRadar : MonoBehaviour
{

    private Vector3 radarPos;
    private GameObject player;
    //private GameObject platform;
    //private GameObject platformHeight;
    //private GameObject symbol;
    private RectTransform rt;
    private Text t;
    public Font f;
    private GameObject[] platforms;
    private GameObject[] symbols;
    private GameObject[] Heights;
    private int platformNum;

    void Start()
    {
        platforms = GameObject.FindGameObjectsWithTag("Platform");
        platformNum = platforms.Length;
        player = GameObject.Find("Pilot");
        symbols = new GameObject[platformNum];
        Heights = new GameObject[platformNum];
        for(int i = 0; i < platformNum; i++)
        {
            symbols[i] = new GameObject("PlatformSymbol" + i.ToString());
            symbols[i].transform.SetParent(this.transform);
            rt = symbols[i].AddComponent<RectTransform>();
            rt.sizeDelta = cal_width(platforms[i].GetComponent<MeshRenderer>());
            symbols[i].AddComponent<Image>();
            Heights[i] = new GameObject("Height" + i.ToString());
            Heights[i].transform.SetParent(GameObject.Find("HeightRadar").transform);
            t = Heights[i].AddComponent<Text>();
            t.text = Mathf.Floor(platforms[i].transform.position.y + 22).ToString();
            t.font = f;
            t.fontSize = 15;
            t.alignment = TextAnchor.MiddleCenter;
            Heights[i].GetComponent<RectTransform>().sizeDelta = new Vector3(30, 30);
            Heights[i].GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);
        }
    }

    void FixedUpdate()
    {
        for(int i = 0; i < platformNum; i++)
        {
            radarPos = platforms[i].transform.position - player.transform.position;
            symbols[i].transform.position = new Vector3(radarPos.x / 6.25f / 3, radarPos.z / 6.25f / 3, 0) + this.transform.position + new Vector3(0, -10f, 0);
            Heights[i].transform.position = new Vector3(GameObject.Find("HeightRadar").transform.position.x, symbols[i].transform.position.y, 10);
        }
        GameObject.Find("HeightText").GetComponent<TextMesh>().text = "Mech Attitude: \n" + Mathf.Floor(player.transform.position.y).ToString();
    }

    Vector2 cal_width(MeshRenderer m)
    {
        float x = m.bounds.size.x / 6.25f / 3;
        float y = m.bounds.size.z / 6.25f / 3;
        return new Vector2(x, y);
    }
}

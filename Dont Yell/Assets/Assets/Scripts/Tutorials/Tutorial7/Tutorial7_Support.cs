﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Tutorial7_Support : MonoBehaviour {

    public Text supportText;
    public PoweredComponent radar;
    public SupportCamMovement sup;
    public float timer = 0.0f;

    private bool enteredPowerSystem;
    private bool investedPower;
    private bool enteredRadar;
    private bool supportCompleted;


    // Use this for initialization
    void Start () {
        enteredPowerSystem = false;
        investedPower = false;
        enteredRadar = false;
        supportCompleted = false;
    }
	
	// Update is called once per frame
	void Update () {
    if (!investedPower && radar.user_power > 0)
        {
            investedPower = true;
            supportText.text = "Click on the mech's head\nto go to the Radar System.";
        }
        else if (investedPower && !enteredRadar && sup.radar)
        {
            supportText.transform.position = sup.transform.position + new Vector3(-30.0f, 0.0f, 9.11f);
			supportText.text = "The Radar will display information on a 2D map using icons.\nThis Radar is a tool that provides more situational awareness\nand can reveal hidden items like invisible platforms.\nWork with the Pilot to figure out what each means.\nMore power will allow greater detail to be seen in the Radar.";
				supportCompleted = true;
        }
        else if(supportCompleted)
        {
            if (timer == 0.0f)
            {
                timer = Time.time + 5.0f;
            }
            else if (Time.time >= timer)
            {
                //Exit Tutorial
                SceneManager.LoadScene(0);
            }
        }
        if (Input.GetKey(KeyCode.Alpha0))
        {
            SceneManager.LoadScene(0);
        }
    }

    public bool status()
    {
        return supportCompleted;
    }
}

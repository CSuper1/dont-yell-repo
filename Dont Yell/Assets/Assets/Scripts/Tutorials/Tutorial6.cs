﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Tutorial6 : MonoBehaviour
{

    //public PilotTextRotater pilotText;
    public Text pilotText;
	public Text supportText;
	public HackerController hc;
	public SupportCamMovement supCam;

    private string[] pilotWords = {"Enemies in the game will hack you. Work with your support to restore proper functionality to your mech.",//"There are enemies in the game that will want to hack you. You will need to work with your support in order to repel any effects that they have on your mech.",
								   "Tell the support you are experiencing a sight hack. If you get dizzy from spinning, hold both hand triggers down to temporarily disable movement.",//"This is a sight hack. Let your support know what your sight is hacked and pay attention to how they want you to put your controllers.",
								   "Your movement has been hacked!",//"Your movement has been hacked!",
								   "Heat hacks are dangerous because you won't always know it happens."};//"Heat hacks are dangerous. Even though you may not be directly affected, your core health will take damage unless heat is balanced quickly."};

    private string[] supWords =   {"Hackers in the game will hack you. Press the unhacking button to access your countermeasure system.",//"There are enemies in the game that will hack you. Press 'A' to access your countermeasure system.",
								   "Each set of buttons is related to a specific kind of hack. The first row is associated with a sight hack.",//"This is your panel. When you are hacked, the pilot has to tell you what has been hacked. Each set of buttons is related to a specific kind of hack. Press the first button in each row for this hack.",
								   "The pilot needs to adjust their controllers according to this graph. When they've done something right, the outline will turn green.",//"Now that you've got the right buttons for the hack, the pilot needs to adjust their controllers according to what lights up. Tell the pilot what they need to do, and when the pilot says they're ready, push the green button to unhack yourself.",
								   "Press the buttons in the middle row for movement hack.",//"You've been hacked again! If you know what hack the pilot is experiencing, go through the same process as before, but now press the buttons in the middle row.",
								   "This is a heat hack. A high powered system will turn off, or low power system will go to 3. Press the buttons in the last row."};//"Your last hack is a heat hack! This means a high powered system will go down to 0 power, or an unpowered system will be boosted to three power. Watch your core health and press the third button in each row to get rid of the hack!"};
	private int stateChanger = 0;

	private bool showedPilText, showedSupText;
	private bool hackIsReady;
	private bool clickedOnce;
	// Use this for initialization
	void Start()
	{
		hc.thisIsTheTutorial();
    }

	// Update is called once per frame
	void FixedUpdate()
	{
		tutorialMachine();
        /*if (Input.GetKey(KeyCode.Alpha0))
        {
            SceneManager.LoadScene(0);
        }*/
	}

	private void tutorialMachine()
	{
	
		switch (stateChanger)
		{
			// Introduction to the hacking system
			case 0:
				// This handles the pilot side text
				if (showedPilText == false)
				{

					changePilotText(0);
					hc.StartHack();
				}

				// This handles the support side text
				if (showedSupText == false)
				{
                    supportText.text = supWords[0];
					showedSupText = true;

				}

				// When the support transitions to the hacker panel, continue the state machine.
				if (supCam.unhacking)
				{
					stateChanger ++;
					showedPilText = false;
					showedSupText = false;
				}
				break;

			// Pilot learns that they need to coordinate with support. Support learns that buttons are tied to certain hacks
			case 1:

				// This handles the pilot side text
				if (showedPilText == false)
				{
				
					
					changePilotText(1);
					showedPilText = true;
				}

				// This handles the support side text
				if (showedSupText == false)
				{
                    supportText.text = supWords[1];
					showedSupText = true;
				}

				// Prompt for changing the support text again
				if (hc.correctLightsShown() && !hackIsReady)
				{
                    supportText.text = supWords[2];
                    hackIsReady = true;
				}

				// Add in this check because this could trigger by milliseconds
				// When we're unhacked, progress the statechanger
				if (hackIsReady)
				{
					if (!hc.areHacked() && !clickedOnce)
					{
						clickedOnce = true;
						StartCoroutine(celebrate());
					}
				}
				break;

			case 2:
				// This handles the pilot side text
				if (showedPilText == false)
				{
				
					hc.StartHack();
					changePilotText(2);
					showedPilText = true;
				}

				// This handles the support side text
				if (showedSupText == false)
				{
                    supportText.text = supWords[3];
                    showedSupText = true;
				}

				// Prompt for changing the support text again
				if (hc.correctLightsShown() && !hackIsReady)
				{
					hackIsReady = true;
				}

				// Add in this check because this could trigger by milliseconds
				// When we're unhacked, progress the statechanger
				if (hackIsReady)
				{
					if (!hc.areHacked() && !clickedOnce)
					{
						clickedOnce = true;
						StartCoroutine(celebrate());
					}
				}
				break;

			case 3:
				// This handles the pilot side text
				if (showedPilText == false)
				{
			
					hc.StartHack();
					changePilotText(3);
					showedPilText = true;
				}

				// This handles the support side text
				if (showedSupText == false)
				{
                    supportText.text = supWords[4];;
                    showedSupText = true;
				}

				// Prompt for changing the support text again
				if (hc.correctLightsShown() && !hackIsReady)
				{
					hackIsReady = true;
				}

				// Add in this check because this could trigger by milliseconds
				// When we're unhacked, progress the statechanger
				if (hackIsReady)
				{
					if (!hc.areHacked() && !clickedOnce)
					{
						clickedOnce = true;
						StartCoroutine(celebrate());
					}
				}
				break;

			case 4:
				//Debug.Log("We finished our tutorial");
				SceneManager.LoadSceneAsync("levelMenu");
				break;



			default:
				Debug.LogError("tutorialMachine in Tutorial6 should not be reaching a default case " + stateChanger);
				break;
		}
	}

	// Point of this is to have a uniform changing of the scene while still giving the support and pilot some time to recover
	private IEnumerator celebrate()
	{
	
		//pilotText.resetRotation();
		//pilotText.setText("Congratulations! You did it!");
		supportText.text = "Great job! You did it!";
		pilotText.text = "Great job! You did it!";
		yield return new WaitForSeconds(3f);
		showedPilText = false;
		showedSupText = false;
		hackIsReady = false;
		clickedOnce = false;
		stateChanger++;
	}

	private void changePilotText(int newtext)
	{
        //pilotText.resetRotation();
        //pilotText.setText(pilotWords[newtext]);
        //StartCoroutine(pilotText.rotate(5f));
        pilotText.text = pilotWords[newtext];
		showedPilText = true;
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Tutorial5 : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        /*
		if(GameObject.Find("Enemy").transform.GetChild(0).name != "Pink")
        {
            GameObject.Find("Enemy").transform.GetChild(0).gameObject.SetActive(true);
        }
        if(GameObject.Find("Enemy").transform.GetChild(0).GetComponent<HealthScript>() != null && GameObject.Find("Enemy").transform.GetChild(0).GetComponent<HealthScript>().hitPoints <= 0)
        {
            GameObject.Destroy(GameObject.Find("Enemy").transform.GetChild(0));
        }
        */

        int i = GameObject.Find("Pilot").GetComponent<WeaponChangeScript>().weaponIndex;
        if (i == 2)
        {
            GameObject.Find("TutorialText").GetComponent<Text>().text = "Good Job.";
            //GameObject.Find("TutorialText2").GetComponent<Text>().text = "Oh my god you actually do it why would you do that???";//"Well done! Now tell the pilot to shoot the enemy";
            //GameObject.Find("TutorialText3").GetComponent<Text>().text = "Oh my god you actually do it why would you do that???";
        }
        if(GameObject.Find("Pink") == null)
        {
            SceneManager.LoadScene(0);
        }
        if (GameObject.Find("Pink").GetComponent<HealthScript>().hitPoints < 100)
        {
            GameObject.Find("TutorialText").GetComponent<Text>().text = "Remember only colored lasers can kill colored enemy.";
            GameObject.Find("TutorialText2").GetComponent<Text>().text = "Remember only colored lasers can kill colored enemy.";
            GameObject.Find("TutorialText3").GetComponent<Text>().text = "Remember only colored lasers can kill colored enemy.";
        }
        else if (GameObject.Find("Pink").GetComponent<HealthScript>().hitPoints < 300)
        {
            //GameObject.Find("TutorialText").GetComponent<Text>().text = "It has a good amount of health. Keep going";
            GameObject.Find("TutorialText2").GetComponent<Text>().text = "Your pilot is eliminating the enemy.";
            GameObject.Find("TutorialText3").GetComponent<Text>().text = "Your pilot is eliminating the enemy.";
        }
	}
}

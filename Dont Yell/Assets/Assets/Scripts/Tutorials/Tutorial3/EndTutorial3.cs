﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EndTutorial3 : MonoBehaviour {

    public Tutorial3_Pilot pilot;
    public Tutorial3_Support supp;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (pilot.pCompStatus() && supp.isSupportComplete())
        {
            //end tutorial here
            SceneManager.LoadScene(0);
        }
  /*      if (Input.GetKey(KeyCode.Alpha0))
        {
            SceneManager.LoadScene(0);
        }*/
    }
}

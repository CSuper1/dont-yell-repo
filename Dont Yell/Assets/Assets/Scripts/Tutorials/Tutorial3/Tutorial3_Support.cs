﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Tutorial3_Support : MonoBehaviour
{

    public Text supportText;
    public Camera suppCam;
    public Tutorial1_PilotLook pilotside;
    public bool onWeaponChange;
    public bool missilePressed;
    public bool weaponConfirmed;
    private SupportCamMovement supCam;
    public GridLayout weaponLayout;
    public string weaponName;
    public int whichWeapon;
    public bool needCheck;

    //private bool enteredPowerSystem;

    public bool supportComplete;


    // Use this for initialization
    void Start()
    {
        supportComplete = false;
        onWeaponChange = false;
        missilePressed = false;
        supCam = GameObject.Find("support camera").GetComponent<SupportCamMovement>();
        weaponName = GameObject.Find("WeaponChangeUI 1").GetComponent<GridLayout>().weaponSelect;
        weaponLayout = GameObject.Find("WeaponChangeUI 1").GetComponent<GridLayout>();
        whichWeapon = GameObject.Find("Pilot").GetComponent<WeaponChangeScript>().weaponIndex;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (supCam.weaponChanging){
            onWeaponChange = true;
        }
        if (onWeaponChange)
        {
            supportText.text = "Click on the missile button\n then ask your pilot for the color\n order and connect them.";
            if ((weaponLayout.weaponSelect).Equals("Missiles"))
            {
                onWeaponChange = false;
                missilePressed = true;
            }
        }
    /*    else if (missilePressed)
        {
            supportText.text = "Ask your pilot to tell you the color order.\nConnect the colors in the correct order\nby clicking and dragging";
            weaponConfirmed = true;
            missilePressed = false;
        }*/
        else if (missilePressed)
        {
            supportText.text = "When done tell your pilot you\n are ready to complete weapon changing\n and hit 'Change Weapon'.";
            if(whichWeapon == 5)
            {
                supportComplete = true;
            }
        }
    }

    public bool isSupportComplete()
    {
        return supportComplete;
    }
}

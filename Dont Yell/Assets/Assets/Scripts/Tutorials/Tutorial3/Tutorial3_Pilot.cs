﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Tutorial3_Pilot : MonoBehaviour
{

    public Text pilotText;
    public bool onWeaponChange;
    public Tutorial3_Support suppStuff;


    public bool pilotComplete;

    // Use this for initialization
    void Start()
    {
        onWeaponChange = false;
        pilotComplete = false;
        suppStuff = GameObject.Find("Stage3Tutorial").GetComponent<Tutorial3_Support>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (suppStuff.onWeaponChange) {
            pilotText.text = "Wait for your support to ask for the color order. Now tell them.";
        }
        else if (suppStuff.missilePressed)
        {
            pilotText.text = "When your support says they are ready to change weapons point your right controller up";

        }
        else if (suppStuff.supportComplete) {
            pilotComplete = true;
            pilotText.text = "Tutorial completed";
        }

    }



    public bool pCompStatus()
    {
        return pilotComplete;
    }


}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class NewTut4 : MonoBehaviour {

    public Text supportText;
    public Text pilotText;
    public MissileUIController fcs;
    public SupportCamMovement sup;
    public missileMatch pod;
    public GameObject enemy;
    public float timer = 0.0f;

    private bool enteredMissileLoader;
    private bool loadedMissiles;

	// Use this for initialization
	void Start () {
        enteredMissileLoader = false;
        loadedMissiles = false;
	}
	
	// Update is called once per frame
	void Update () {
        //Debug.Log(pod.getLoaded());
        if (!enteredMissileLoader && sup.missile)
        {
            supportText.transform.position = new Vector3(80.0f, 94.0f, 9.0f); //-272, -103.54, 9
            supportText.text = "Missiles can only be loaded when doors are closed.\nMissiles can only be fired when doors are open.\nTo load a missile click on matching numbers\nfrom top to bottom.\nClick Load Missile when the entire column is lit.";
            pilotText.text = "The Support is loading missiles right now.\nIf you point your laser on an enemy when a missile is loaded\nit will lock on.\nOtherwise the missile will follow your laser.";
            enteredMissileLoader = true;
        }
        else if (enteredMissileLoader && !loadedMissiles && pod.getLoaded() > 0)
        {
            loadedMissiles = true;
            supportText.text = "Now open the doors so that the Pilot can fire.";
            pilotText.text = "Eliminate the enemy.";
            enemy.SetActive(true);
        }
        else if (enteredMissileLoader && loadedMissiles && GameObject.Find("Elite") == null) 
        {
            pilotText.text = "Tutorial Complete";
            supportText.text = "Tutorial Complete";
            if(timer == 0.0f)
            {
                timer = Time.time + 5.0f;
            }
            else if(Time.time >= timer)
            {
                //Exit Tutorial
                SceneManager.LoadScene(0);
            }
        }
	}
}

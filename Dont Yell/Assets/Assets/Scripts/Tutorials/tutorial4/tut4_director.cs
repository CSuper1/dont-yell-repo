﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class tut4_director : MonoBehaviour {

	public GameObject pilot;
	public GameObject support;
	public GameObject obstruction;
	public GameObject enemy;
	public PodFiring pod;

	private string state = "begin";

	// Use this for initialization
	void Start () {
		obstruction.transform.Find("destroytext").GetComponent<TextMesh>().text = "";
		support.transform.Find("twoI/missilepowertext1").GetComponent<Text>().text = "";
		support.transform.Find("twoI/missilepowertext2").GetComponent<Text>().text = "";
		support.transform.Find("oneI/missileloadtext").GetComponent<Text>().text = "";
		enemy.transform.Find("lockontext").GetComponent<TextMesh>().text = "";

	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKey(KeyCode.Alpha0))
        {
            SceneManager.LoadScene(0);
        }
		switch (state) {
		case "begin":
			if (pilot.GetComponent<WeaponChangeScript> ().weaponIndex == 5) {
				support.transform.Find ("sixI/switchmissiletext").GetComponent<Text> ().text = "Go to missile loading panel";
				support.transform.Find ("oneI/doorstext").GetComponent<Text> ().text = "Close missile doors ↓";
				state = "load1";
			}
			break;
		case "load1":
			if (support.transform.Find ("oneI/MissileLoading").GetComponent<MissileUIController> ().doorsOpen == false) {
                support.transform.Find("sixI/switchmissiletext").GetComponent<Text>().text = "";
                support.transform.Find ("oneI/missileloadtext").GetComponent<Text> ().text = "Click numbers 1-6 to match central column";
				support.transform.Find ("oneI/doorstext").GetComponent<Text> ().text = "Load a missile after pressing the correct numbers";
				state = "load2";
			}
			break;
		case "load2":
			if (support.transform.Find ("oneI/MissileLoading").GetComponent<missileMatch> ().getLoaded () > 0) {
				support.transform.Find ("oneI/missileloadtext").GetComponent<Text> ().text = "";
				support.transform.Find ("oneI/doorstext").GetComponent<Text> ().text = "Load another missile";
				state = "load3";
			}
			break;
		case "load3":
			if (support.transform.Find ("oneI/MissileLoading").GetComponent<missileMatch> ().getLoaded () > 1) {
				support.transform.Find ("oneI/missileloadtext").GetComponent<Text> ().text = "";
				support.transform.Find ("oneI/doorstext").GetComponent<Text> ().text = "Open missile doors ↓";
				state = "load4";
			}
			break;
		case "load4":
			if (support.transform.Find ("oneI/MissileLoading").GetComponent<MissileUIController> ().doorsOpen == true) {
				support.transform.Find ("oneI/missileloadtext").GetComponent<Text> ().text = "Wait until your pilot needs something.";
				support.transform.Find ("oneI/doorstext").GetComponent<Text> ().text = "";
                //obstruction.transform.Find ("destroytext").GetComponent<TextMesh> ().text = "Destroy\nObstruction";
                GameObject.Find("TutorialText").GetComponent<Text>().text = "Shoot the black wall to destroy it.";
				state = "shoot1";
			}
			break;
		case "shoot1":
			if (obstruction == null) {
				state = "powerup";
                support.transform.Find("oneI/missileloadtext").GetComponent<Text>().text = "Power up the missiles";
                support.transform.Find ("twoI/missilepowertext1").GetComponent<Text> ().text = "Power up the missiles";
				support.transform.Find ("twoI/missilepowertext2").GetComponent<Text> ().text = "<-- Power up the missiles past level 1";
			}
			break;
		case "powerup":
			if (support.transform.Find ("twoI/PowerSystem2.0/Missiles Component").GetComponent<PoweredComponent> ().user_power >= 2) {
				support.transform.Find ("twoI/missilepowertext1").GetComponent<Text> ().text = "";
				support.transform.Find ("twoI/missilepowertext2").GetComponent<Text> ().text = "";
				support.transform.Find ("oneI/missileloadtext").GetComponent<Text> ().text = "Load a missile while the pilot is pointing at an enemy to lock on";
				support.transform.Find ("oneI/doorstext").GetComponent<Text> ().text = "Timing is important!";
                //enemy.transform.Find ("lockontext").GetComponent<TextMesh> ().text = "Lock on";
                GameObject.Find("TutorialText").GetComponent<Text>().text = "Lock on that enemy";

                state = "shoot2";
			}
			break;
		case "shoot2":
			bool MISSILE_IS_LOCKED_ON = pod.getTargets () > 0;
			if (MISSILE_IS_LOCKED_ON) {
				support.transform.Find ("oneI/missileloadtext").GetComponent<Text> ().text = "";
				support.transform.Find ("oneI/doorstext").GetComponent<Text> ().text = "";
                //enemy.transform.Find ("lockontext").GetComponent<TextMesh> ().text = "Shoot";
                GameObject.Find("TutorialText").GetComponent<Text>().text = "Move onto the next room and find the enemy. Missiles can be locked on if you are pointing at an enemy when the support loads a missile. Try this now.";


            } else {
				support.transform.Find ("oneI/MissileLoading").GetComponent<MissileUIController> ().doorsOpen = false;
			}
			if (GameObject.Find ("Elite") == null) {
				state = "completed";
			}
			break;
		case "completed":
            SceneManager.LoadScene(0);
			break;
		}
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Tutorial1_Support : MonoBehaviour {

    public Text supportText;
    public Camera suppCam;
    public PoweredComponent legs;
    public GeneralShootingScript gss;
	public Slider boostBar;
	public Tutorial1_PilotLook pilotside;

    //private bool enteredPowerSystem;
    private bool hasSpeedBoosted;
    private bool hasReloaded;
    public bool supportComplete;
	private bool refueled;

    // Use this for initialization
    void Start () {
		boostBar.value = 0.0f;
        //enteredPowerSystem = false;
        hasSpeedBoosted = false;
        hasReloaded = false;
        supportComplete = false;
		refueled = false;
	}
	
	// Update is called once per frame
	void Update () {
        if(!hasSpeedBoosted && legs.user_power > 0)
        {
            hasSpeedBoosted = true;
            supportText.text = "Heat will passively transfer\nbetween systems.\nOverheating will cause problems.\nAlways keep an eye on your heat.\nHelp the Pilot reload.\nClick on the Machine Gun icon\non the right.";
        }
        else if(hasSpeedBoosted && !hasReloaded && gss.magazine > 0 && pilotside.killedTarget())
        {
            hasReloaded = true;
            supportText.text = "Now help the Pilot use their boost.\nClick on the sprinting icon below.\nClick and drag the white circle \nnear the blue circle to refuel.\n\nHaving fuel enables flight and dash.";
                //You have successfully reloaded the Pilot's weapon!\nCongratulations, Support!\n";
            
        }
		else if(hasSpeedBoosted && hasReloaded && !refueled && boostBar.value >= 500.0f){
			refueled = true;
			supportText.text = "Tutorial completed";
			supportComplete = true;
		}
	}

    public bool isSupportComplete()
    {
        return supportComplete;
    }
}

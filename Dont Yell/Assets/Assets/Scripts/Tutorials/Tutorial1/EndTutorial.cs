﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class EndTutorial : MonoBehaviour {

    public Tutorial1_PilotLook pilot;
    public Tutorial1_Support supp;
    public float endtime = 0.0f;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if(pilot.pCompStatus() && supp.isSupportComplete())
        {
            if (endtime == 0.0f)
            {
                endtime = Time.time + 5.0f;
            }
            else if (Time.time > endtime)
            {
                //end tutorial here
                SceneManager.LoadScene(0);
            }
        }
        if (Input.GetKey(KeyCode.Alpha0))
        {
            SceneManager.LoadScene(0);
        }
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Tutorial1_PilotLook : MonoBehaviour {

    public Text pilotText;
    public OVRInput.Controller left;
    public PoweredComponent legs;
    public GeneralShootingScript gss;
    public GameObject Enemy;
	public Slider boostBar;

    private bool hasMovedForward;
    private bool hasSpeedBoosted;
    private bool hasReloaded;
    private bool hasKilledTarget;
    public bool pilotComplete;

	// Use this for initialization
	void Start () {
        left = OVRInput.Controller.LTouch;
        hasMovedForward = false;
        hasSpeedBoosted = false;
        hasReloaded = false;
        hasKilledTarget = false;
        pilotComplete = false;
	}
	
	// Update is called once per frame
	void Update () {
		if(!hasMovedForward && OVRInput.Get(OVRInput.Axis2D.PrimaryThumbstick).y > 0.5f)
        {
            hasMovedForward = true;
            pilotText.text = "Various parts of your mech can be buffed. For now, ask your support to increase your movement speed.";//"Ask Support to increase speed";
        }
        else if(hasMovedForward && !hasSpeedBoosted && legs.user_power > 0)
        {
            hasSpeedBoosted = true;
            pilotText.text = "Notice the differing levels of speed. Now try asking your support to give you some ammo.";//"The Support has increased your movement speed.\nNow try asking Support to help reload your weapon.";
        }
        else if(hasMovedForward && hasSpeedBoosted && !hasReloaded && gss.magazine > 0)
        {
            hasReloaded = true;
            Enemy.SetActive(true);
            pilotText.text = "Use RIGHT trigger to shoot. There is an enemy nearby you. Shoot it";//"Press on the Right Trigger to fire \nYou can aim by moving the Right Controller \nDestroy the target";
        }
        else if(hasMovedForward && hasSpeedBoosted && hasReloaded && !hasKilledTarget && GameObject.Find("Elite") == null)
        {
            hasKilledTarget = true;
            pilotText.text = "Try to use boost and jump.\nPress A on the right controller to boost.\nPress the hand trigger on the left controller to jump.";
        }
		else if(hasMovedForward && hasSpeedBoosted && hasReloaded && hasKilledTarget && (OVRInput.Get(OVRInput.RawButton.A) || OVRInput.Get(OVRInput.RawButton.LHandTrigger)) && boostBar.value >= 500.0f){
			pilotComplete = true;
			pilotText.text = "Tutorial completed";
		}

        if (OVRInput.Get(OVRInput.RawButton.A) || OVRInput.Get(OVRInput.RawButton.LHandTrigger))
        {
            Debug.Log("button");
        }

    }

	public bool killedTarget(){
		return hasKilledTarget;
	}

    public bool pCompStatus()
    {
        return pilotComplete;
    }


}

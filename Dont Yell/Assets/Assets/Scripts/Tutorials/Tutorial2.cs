﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Tutorial2 : MonoBehaviour
{

	//public PilotTextRotater pilotText;
	public Text pilotText;
	public Text supportText;
	public SupportCamMovement supCam;
	public PoweredComponent shields;
	public ShieldGenScript frontShield;
	public handShield handShield;

	private int pilotTextPos = 0;
	private int shotsLeftToBlock = 5;
	private string[] supWords =     {"Add 3 power to the shield system.",
									"There are four sections of the mech that are all shielded separately. Press the right body side on the mech outline.",
									"Each section passively generates a small amount of shields while the component has power.",
									"In the center is the shield bank. It holds the shield energy in reserve until you need it.",
									"Pressing these buttons (arrows to the + buttons) takes power from the bank and allocates it to the corresponding section.",
									"It is important to only use the shields when you need them, because they degrade quickly over time.",
									"Overload your front shield.",
									"You have completed the basic shields tutorial. Your pilot should be finished shortly."};

	private string[] pilotWords =   {"Attached to your left arm is a physical shield.",
									"It can block 10 hits before it breaks.",
									"Every 5 seconds it hasn’t taken damage, it can take another hit.",
									"After breaking, it will come back in 5 seconds with one hit point.",
									"Block 5 shots!",
									"Great job! Your support should be finished with their tutorial shortly."};
	private int stateChanger = 0;

	private bool showedPilText, showedSupText;
	private bool hackIsReady;
	private bool clickedOnce;
	// Use this for initialization
	void Start()
	{
	}

	// Update is called once per frame
	void FixedUpdate()
	{
		tutorialMachine();
		/*if (Input.GetKey(KeyCode.Alpha0))
        {
            SceneManager.LoadScene(0);
        }*/
	}

	private void tutorialMachine()
	{

		switch (stateChanger)
		{
			case 0:
				if (showedPilText == false)
				{
					supportText.text = supWords[0];
					showedSupText = true;
					InvokeRepeating("pilotTimer", 0.0f, 7.0f);
				}

				if (shields.user_power == 3)
				{
					stateChanger++;
					showedSupText = false;
				}

				break;

			case 1:

				// This handles the support side text
				if (showedSupText == false)
				{
					supportText.text = supWords[1];
					showedSupText = true;

				}

				// When the support transitions to the shield panel, continue the state machine.
				if (supCam.shield)
				{
					stateChanger++;
					showedSupText = false;
				}
				break;

			case 2:
				if (showedSupText == false)
				{
					supportText.text = supWords[2];
					showedSupText = true;
					StartCoroutine(pause());

				}

				break;

			case 3:
				if (showedSupText == false)
				{
					supportText.text = supWords[3];
					showedSupText = true;
					StartCoroutine(pause());
				}
				break;

			case 4:
				if (showedSupText == false)
				{
					supportText.text = supWords[4];
					showedSupText = true;
					StartCoroutine(pause());
				}
				break;

			case 5:
				if (showedSupText == false)
				{
					supportText.text = supWords[5];
					showedSupText = true;
					StartCoroutine(pause());
				}
				break;

			case 6:
				if (showedSupText == false)
				{
					supportText.text = supWords[6];
					showedSupText = true;
				}

				if (frontShield.shields > frontShield.shieldMin)
				{
					stateChanger++;
					showedSupText = false;
				}

				break;

			case 7:
				if (showedSupText == false)
				{
					supportText.text = supWords[7];
					showedSupText = true;
				}

				break;

			default:
				Debug.LogError("tutorialMachine in Tutorial2 should not be reaching a default case " + stateChanger);
				break;
		}

		if (pilotTextPos == 5)
		{
			if (handShield.readHit())
				pilotText.text = "Block " + shotsLeftToBlock + " more shot" + ((shotsLeftToBlock == 1) ? "" : "s") + "!";
			shotsLeftToBlock--;

			if (shotsLeftToBlock == 0)
			{
				pilotTextPos = 6;
				pilotText.text = pilotWords[5];
			}
		}

		if (stateChanger == 7 && pilotTextPos == pilotWords.Length - 1)
			SceneManager.LoadSceneAsync("levelMenu");
		// finish the scene
	}

	// Point of this is to have a uniform changing of the scene while still giving the support and pilot some time to recover

	private IEnumerator pause()
	{

		yield return new WaitForSeconds(8f);
		showedSupText = false;
		stateChanger++;
	}

	private IEnumerator pilotTimer()
	{
		changePilotText(pilotTextPos);
		pilotTextPos++;
		// Stop this at the end; the last phrase just tells the pilot to wait for support
		if (pilotTextPos > pilotWords.Length - 1)
			CancelInvoke();
		yield return new WaitForSeconds(7f);
	}

	private void changePilotText(int newtext)
	{
		//pilotText.resetRotation();
		//pilotText.setText(pilotWords[newtext]);
		//StartCoroutine(pilotText.rotate(5f));
		pilotText.text = pilotWords[newtext];
		showedPilText = true;
	}
}
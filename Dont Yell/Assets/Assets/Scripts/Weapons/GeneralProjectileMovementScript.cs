﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GeneralProjectileMovementScript : MonoBehaviour {

    private float velocity;

	// Use this for initialization
	void Start () {
        velocity = 0.0f;
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        transform.Translate(transform.forward * velocity * Time.deltaTime, Space.World);
	}

    public void setVelocity(float v)
    {
        velocity = v;
    }

    public void setPosition(Vector3 pos)
    {
        transform.position = pos;
    }
}

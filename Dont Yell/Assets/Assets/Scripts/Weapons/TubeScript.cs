﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TubeScript : MonoBehaviour {

    public Transform t;
    public Rigidbody projectile;
    public float projectileForce;
    public GameObject target;
    public float loadProgress = 0.0f;
	public MissileStatus UITube;
	bool loadItUp;

	// Use this for initialization
	void Start () {
        t = transform;
        projectileForce = 500.0f;

		

	}
	
	// Update is called once per frame
	void FixedUpdate () {
		if (loadItUp)
			loadProgress += .01f;
	}

    //fire
    public void fire()
    {
        if (loadProgress >= 1.0f)
        {
            Rigidbody prClone = Instantiate(projectile, t.position, Quaternion.identity) as Rigidbody;
            prClone.AddForce(transform.forward * projectileForce);
            prClone.gameObject.GetComponent<MissileTrackingLogic>().setTarget(target);
            target = null;
            loadProgress = 0.0f;
			loadItUp = false;
        }
  
    }

	public void fuckTheKraken()
	{
		loadItUp = true;
	}

    public void setTarget(GameObject g)
    {
        target = g;
    }

    public float getProgress()
    {
        return loadProgress;
    }
}

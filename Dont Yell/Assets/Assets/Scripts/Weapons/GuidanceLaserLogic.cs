﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GuidanceLaserLogic : MonoBehaviour
{

    public GameObject gun;
    public float range = 100.0f;
    public GameObject lockOnPoint;
    public WaitForSeconds lockTime = new WaitForSeconds(1.0f);

    //Grace time before lock nullified
    public WaitForSeconds lockStickTime = new WaitForSeconds(1.0f);

    private LineRenderer guidanceLaser;
    private RaycastHit hit;

    // Use this for initialization
    void Start()
    {
        //gun = GameObject.FindGameObjectWithTag("RightController");
        guidanceLaser = GetComponent<LineRenderer>();
        //guidanceLaser.enabled = true;
    }

    // Update is called once per frame
	void FixedUpdate()
    {

        Vector3 rayOrigin = gun.transform.position;

        //Update position of lock on point
        guidanceLaser.SetPosition(0, rayOrigin);
        //will need additional statement to check if target can be locked
        if (Physics.Raycast(rayOrigin, gun.transform.forward, out hit, range) && hit.collider.gameObject.GetComponent<LockTargetScript>() != null)
        {
            Debug.Log(hit.collider.name);
            guidanceLaser.SetPosition(1, hit.point);
            lockOnPoint = hit.collider.gameObject;

        }
        else
        {
            guidanceLaser.SetPosition(1, rayOrigin + (gun.transform.forward * range));
            lockOnPoint = null;
            /*if (lockOnPoint != null)
            {

            }*/
        }
    }

    /*private IEnumerator Lock()
    {
        yield return lockTime;

        //Check if the currently hitscanned object is the same
        //If it is, lock
        //If not, reet lockonpoint

    }

    private IEnumerator UnLock()
    {
        yield return lockStickTime;

        //Check if there's still a lock
        //If not, reset lockonpoint
        //Otherwise don't do anything
        //if(hit.collider.GetComponent)
    }*/


    //Call from outside to pass to Support
    //Support should only add target to list if it is not null
    public GameObject getTarget()
    {

        return lockOnPoint;
    }
}
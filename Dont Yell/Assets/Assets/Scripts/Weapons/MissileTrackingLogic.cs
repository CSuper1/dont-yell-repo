﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MissileTrackingLogic : MonoBehaviour {

    public GameObject trackingTarget;
    public GameObject launcher;
    public Rigidbody missile;

    private Vector3 aimVector;
    private Vector3 launcherToMissileVector;
    private Vector3 CRE;
    private Vector3 beamCross;
    private Vector3 damping;
    private Vector3 guidanceFactorPN;
    private float navigationConstant = 5.0f;
    private float beamCorrectionConstant = 50.0f;
    private float dampingConstant = 150.0f;
    private Vector3 previousLOS;
    private Vector3 previousCRE;
    private Vector3 missileVector;
    private Vector3 prevFrameMissilePosition;
    private Vector3 missileToTargetVector;
    private bool prevFrame = false;
    private float currFrameDiff;
    private float prevFrameDiff;
    private float LOSDelta;
    private Vector3 guidanceDirection;
    private Vector3 targetFuture;
    private Vector3 trackingTargetLast;

    

    // Use this for initialization
    void Start() {
        missile = gameObject.GetComponent<Rigidbody>();
        launcher = GameObject.FindWithTag("Muzzle");
    }

    // Update is called once per frame
    void FixedUpdate() {

        //Use PN if lock target is set
        if (trackingTarget != null)
        {
            //get direction of missile as 3D vector
            //get direction of missile to target
            //compute angle between
            //remember this
            //grab in next frame
            //compare
            //change = amount to turn
            //multiply change by constant
            //apply as acceleration to turn missile

            computeMissileVector();
            computeDestination();
            computeMissileToTargetVector();
            computeLOSDeltaRate();


            //clamp LOSDelta so that really tiny values are increased to a minimum
            if (prevFrame)
            {
                missile.AddForce(guidanceDirection * navigationConstant);

                Debug.DrawRay(transform.position, transform.forward * 50.0f, Color.red);
                Debug.DrawRay(transform.position, missileVector * 50.0f, Color.blue);
                Debug.DrawRay(transform.position, guidanceDirection * 50.0f, Color.yellow);
                Debug.DrawRay(transform.position, missileToTargetVector * 50.0f, Color.cyan);
                Debug.DrawRay(transform.position, targetFuture - transform.position, Color.green);
            }
            /*Debug.DrawRay(transform.position, missileToTargetVector, Color.cyan);
            Debug.DrawRay(transform.position, targetFuture - transform.position, Color.green);
            Rigidbody missile = GetComponentInParent<Rigidbody>();
            missile.AddForce(transform.forward * 500.0f * Time.deltaTime);*/
            //Debug.DrawRay(transform.position, transform.forward * 50.0f, Color.red);
            
            
        }
        //Otherwise SACLOS
        else
        {
            //Compute guidance control
            computeAimVector();
            computeLauncherToMissileVector();
            computeCrossRangeError();
            computeBeamMotion();
            computeDamping();

            //Add force to missile for steering
            missile.AddForce((CRE * -navigationConstant) + (beamCross * -beamCorrectionConstant) + (damping * -dampingConstant));

            //Necessary for computeBeamMotion()
            previousLOS = aimVector;

            //Necessary for computeDamping()
            previousCRE = CRE;
        }

    }

    //computes the LoS of the launcher
    void computeAimVector()
    {
        aimVector = launcher.transform.forward;
        //aimVector.Normalize();
    }

    //computes the LoS from launcher to Missile
    void computeLauncherToMissileVector()
    {
        launcherToMissileVector = transform.position - launcher.transform.position;
        //launcherToMissileVector.Normalize();
    }

    //computes the amount of correction needed to steer missile back on course
    //tune navigationConstant to exaggerate the amount of correction
    void computeCrossRangeError()
    {
        Vector3 cross = Vector3.Cross(aimVector, launcherToMissileVector);
        CRE = Vector3.Cross(cross, aimVector);

        //CRE.Normalize();
    }

    //computes an additional force function for the guidance correction
    //tune beamCorrectionConstant to change how hard it corrects for changes in aimVector
    //The higher the constant, the more it tries to correct for aimVector's rate of change
    void computeBeamMotion()
    {
        beamCross = aimVector;
        if (previousLOS == null)
        {

        }
        else
        {
            beamCross = Vector3.Cross(previousLOS, aimVector);
            //beamCross.Normalize();
            beamCross /= launcherToMissileVector.magnitude;
        }

    }

    //computes damping for guidance correction
    //limits how far off a missile can overcorrect
    //The higher the value, the less overcorrection the misisle has
    void computeDamping()
    {
        damping = CRE;
        if (previousCRE == null)
        {

        }
        else
        {
            damping = CRE - previousCRE;
            //damping.Normalize();
        }
    }

    
    //computes direction of missile
    //going to have to adjust for the fact the missile doesn't rotate
    void computeMissileVector()
    {
        if (!prevFrame)
        {
            //Debug.Log("PrevFrameData empty");
            missileVector = transform.forward;
        }
        else
        {
            //Debug.Log("PrevFrameData filled");
            missileVector = transform.position - prevFrameMissilePosition;
            //Debug.Log("Missile curr frame: " + transform.position + "\nMissile prev frame: " + prevFrameMissilePosition);
        }
        prevFrameMissilePosition = transform.position;
        //missileVector = transform.forward;
        missileVector.Normalize();
        //Debug.Log("MV: " + missileVector.magnitude);
        //Debug.Log("Missile Vector: " + missileVector + "\nMissile Vector Norm: " + missileVector.normalized);
    }

    //computes vector from missile to target
    void computeMissileToTargetVector()
    {
        missileToTargetVector = trackingTarget.transform.position - transform.position;
        //missileToTargetVector = targetFuture - transform.position;
        missileToTargetVector.Normalize();
        //Debug.Log("MTT: " + missileToTargetVector.magnitude);
        //Debug.Log("MTT: " + missileToTargetVector.normalized);
    }

    //compute difference in angle between frames
    void computeLOSDeltaRate()
    {
        Vector3 cross = Vector3.Cross(missileVector.normalized, missileToTargetVector.normalized);
        currFrameDiff = cross.magnitude;
        Debug.Log("Curr Frame Magnitude: " + currFrameDiff);
        Debug.DrawRay(transform.position, cross.normalized * 50.0f, Color.magenta);
        if (!prevFrame)
        {
            LOSDelta = 0.0f;
        }
        else
        {
            Debug.Log("Prev Frame Magnitude: " + prevFrameDiff);
            Debug.Log("Actual Angle: " + Vector3.Angle(missileVector, missileToTargetVector));
            LOSDelta = currFrameDiff - prevFrameDiff;
        }
        guidanceDirection = Vector3.Cross(cross, missileVector);
       
        guidanceDirection.Normalize();
        prevFrameDiff = currFrameDiff;
        prevFrame = true;
        Debug.Log("Delta: " + LOSDelta + "\nDirection: " + guidanceDirection);
    }

    //Project target's future location
    void computeDestination()
    {
        if (trackingTargetLast == null)
        {
            targetFuture = trackingTarget.transform.position;
        }
        else
        {
            targetFuture = trackingTarget.transform.position + (navigationConstant * (trackingTarget.transform.position - trackingTargetLast));
            //Debug.Log("Target curr: " + trackingTarget.transform.position);
            //Debug.Log("Target future: " + targetFuture);
        }
        trackingTargetLast = trackingTarget.transform.position;
        //transform.LookAt(targetFuture);
    }

    public void setTarget(GameObject g)
    {
        trackingTarget = g;
    }
}

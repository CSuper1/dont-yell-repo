﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrosshairScript : MonoBehaviour {

    public GameObject gun;
    public Camera CameraFacing;
	public LineRenderer line;

    private Vector3 originalScale;

    void Start()
    {
		line = GetComponent<LineRenderer> ();
		line.enabled = false;
        originalScale = transform.localScale;
    }

	// Update is called once per frame
	void FixedUpdate () {

        Vector3 rayOrigin = gun.transform.position;
		line.SetPosition (0, rayOrigin);
        RaycastHit hit;
        float distance;

        //move crosshair to appropriate distance
        //if no hits detected, move crosshair to near the farclip plane
        if(Physics.Raycast(rayOrigin, gun.transform.forward, out hit, 50.0f))
        {
            BulletDamageScript ignoreBullet = hit.collider.gameObject.GetComponent<BulletDamageScript>();
            if (ignoreBullet == null)
            {
                //Debug.Log("No Bullet");
                distance = hit.distance * 0.95f;
            }
            else
            {
                //Debug.Log("Ignoring Bullet");
                distance = hit.distance * 0.95f;
                //distance = CameraFacing.farClipPlane * 0.95f;
            }
			line.SetPosition (1, hit.point);
        }
        else
        {
            distance = CameraFacing.farClipPlane * 0.95f;
			line.SetPosition(1, rayOrigin + (gun.transform.forward * distance));
        }

        //manipulate crosshair's transforms
        transform.position = rayOrigin + (gun.transform.forward * distance);
        transform.LookAt(CameraFacing.transform.position);
        transform.Rotate(0.0f, 180.0f, 0.0f);

        //scale with distance
		if (distance < 5.0f) {
			distance = 5.0f;
		}
        transform.localScale = originalScale * distance;
    }

	public void setLaser(bool b){
		line.enabled = b;
	}
}

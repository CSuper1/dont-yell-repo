﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReloadOverride : MonoBehaviour {

    public GameObject[] tubes;
    public bool[] load;

    private int currTube;

	// Use this for initialization
	void Start () {
        tubes = gameObject.GetComponent<MuzzleTransformOverride>().getOverrides();
        load = new bool[tubes.Length];
        for(int i = 0; i < load.Length; i++)
        {
            load[i] = true;
        }
        currTube = 0;
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		if(currTube > load.Length)
        {
            currTube = 0;
        }
	}

    void reload()
    {
        GeneralShootingScript gun = GetComponent<GeneralShootingScript>();
        gun.addRounds(1);
        gun.setIsLoaded(true);
        load[currTube] = true;
        currTube++;
    }

    public int firstLoaded()
    {
        int j = 0;
        bool found = false;
        while (!found && j < load.Length)
        {
            if(load[j] == true)
            {
                found = true;
            }
            else
            {
                j++;
            }
        }
        return j;
    }
}

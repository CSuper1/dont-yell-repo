﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Melee : MonoBehaviour {

    private Vector3 velocity;
    public float threshold = 1.0f;
    public OVRInput.Controller hi;
    public float pushForce = 500.0f;

    void OnTriggerEnter(Collider other)
    {
        //If the velocity of the controller is not high enough, don't do anything
        //otherwise test for a hit and push any struck enemies in the direction of the hit
        //Debug.Log("Melee Hit");
        if (velocity.magnitude < threshold)
        {

        }
        else
        {

            //We struck an enemy
            if (other.CompareTag("Enemy") || other.CompareTag("VIPEnemy"))
            {
                Debug.Log("Push!");
                Rigidbody body = other.gameObject.GetComponent<Rigidbody>();
                body.AddForce(velocity * pushForce);
            }
            else
            {
            }
        }
    }

    // Use this for initialization
    

	// Use this for initialization
	void Start () {
        hi = OVRInput.Controller.RTouch;
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        velocity = OVRInput.GetLocalControllerVelocity(hi);
        //Debug.Log(velocity.magnitude);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PodFiring : MonoBehaviour
{

    public GameObject[] tubes;
    public Rigidbody projectile;
    public float projectileForce = 500.0f;
    public float timeToLive = 7.0f;
    public bool canFire = false;

    private int currTube;
    public GameObject[] targets;
    public int maxTubes;
    public MissileUIController mc;

    // Use this for initialization
    void Start()
    {
        tubes = GameObject.FindGameObjectsWithTag("LauncherTube");
        targets = new GameObject[tubes.Length];
        currTube = tubes.Length;
        canFire = mc.doorsOpen;
    }

    // Update is called once per frame
	void FixedUpdate()
    {
        // Debug.Log(canFire);
        //Debug.Log(currTube);
        //Debug.Log(tubes.Length);
        //firing function should be placed in parent governing both pods
        //This one is for testing purposes
        //Debug.Log("Am I even on.");
        if ( /*(Input.GetMouseButton(0) ||*/ OVRInput.Get(OVRInput.RawButton.RIndexTrigger) && canFire)
        {
           // Debug.Log("I swear to god I will destroy.");
            mc.prepareTheKrakens();
            fire();
        }
        canFire = mc.doorsOpen;

    }

    //Call fire from outside
    public void fire()
    {
        //will fire entire payload in one click
        while (currTube < tubes.Length)
        {
            Rigidbody cloneRB = Instantiate(projectile, tubes[currTube].transform.position, Quaternion.identity) as Rigidbody;
            cloneRB.AddForce(tubes[currTube].transform.forward * projectileForce);
            cloneRB.transform.forward = tubes[currTube].transform.forward;
            cloneRB.GetComponent<BulletDamageScript>().setOwner("Player");

            //Set Tracking Target
            //If there are fewer targets than Missiles, untargeted missiles will fly out first
            //can be tweaked later
            //if targets is a list, we could remove it once we have a target locked
            //probably easier maintenance of the data structure too
            cloneRB.GetComponent<MissileTrackingLogic>().setTarget(targets[tubes.Length - currTube - 1]);
            Destroy(cloneRB.gameObject, timeToLive);

            //Move spawnpoint and choose new target
            currTube++;
        }
        //setFire(false);
        for (int i = 0; i < targets.Length; i++)
            targets[i] = null;
    }

    //Use this to allow pod to fire
    //Based on pod doors open/close
    //Call this when doors are opening/closing
    public void setFire(bool b)
    {
        canFire = b;
    }

    //Pass in array of targets to track
    //Cannot be larger than tube count
    //Call this when doors are opened
    public void setTargets(GameObject[] t)
    {
        for (int i = 0; i < t.Length; i++)
        {
            if (i == targets.Length)
                break;
            targets[i] = t[i];
        }
    }

    //Set the number of tubes loaded
    //The more tubes that are loaded, the closer to 0 currTube is
    //Do not load more missiels than there are tubes
    //Cannot fire if there is no payload
    //Call this when the doors are opened
    public void setLoaded(int i)
    {
        currTube = tubes.Length - i;
        if (currTube < 0)
            currTube = 0;
        Debug.Log("Pod: " + currTube);
    }

    public int getTargets()
    {
        return targets.Length;
    }

    public int getShots()
    {
        return tubes.Length - currTube;
    }
}

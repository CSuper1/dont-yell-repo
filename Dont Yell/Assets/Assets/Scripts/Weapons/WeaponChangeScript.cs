﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponChangeScript : MonoBehaviour {
    
    //Current weapon
    public int weaponIndex = 0;

    //List of all weapons
    public Transform[] weapons;

	// Use this for initialization
	void Start () {
        changeWeapon(1);
	}
	
	// Update is called once per frame
	void FixedUpdate () {

        //on keypress of NUM
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            changeWeapon(0); //melee
        }
        else if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            changeWeapon(1); //gun
        }
        else if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            changeWeapon(2); //laserpink
        }
        else if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            changeWeapon(3); //laseryellow
        }
        else if (Input.GetKeyDown(KeyCode.Alpha5))
        {
            changeWeapon(4); //lasercyan
        }
        else if (Input.GetKeyDown(KeyCode.Alpha6))
        {
            changeWeapon(5); //missiles 	
        }
	}

    public void changeWeapon(int num)
    {
        weaponIndex = num;
        for(int i = 0; i < weapons.Length; i++)
        {
            if(i == num)
            {
                weapons[i].gameObject.SetActive(true);
            }
            else
            {
                weapons[i].gameObject.SetActive(false);
            }
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosiveEntityData : MonoBehaviour {

    public GameObject boom;
	public float damage = 1.0f;
    public float innerRadius = 1.0f;
    public float outerRadius = 5.0f;
	public AudioSource boomSound;
	public float power = 0.0f;
	
	powerMissileLauncher mlScript;

	// Use this for initialization
	void Start () {
		if(GameObject.Find("Missiles Component") != null)
			mlScript = GameObject.Find("Missiles Component").GetComponent<powerMissileLauncher>();
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		if(mlScript != null && this.gameObject.CompareTag("Rocket")){
			overRideDamage(mlScript.dmg);
		}
	}

    //called when barrel is about to be destroyed
    void dealAOEDamage()
    {
	        
		//get all colliders within outerRadius
        Collider[] victims = Physics.OverlapSphere(transform.position, outerRadius);
        Debug.Log(victims.Length + " entities caught in blast");
        
        //Loop through and deal damage to each entity with a health script
        for(int i = 0; i < victims.Length; i++)
        {

            //If health is null, there is no health script
            HealthScript health = victims[i].gameObject.GetComponent<HealthScript>();
            if (health != null) 
            {

                //Find distance between entity and barrel
                //This is important for damage interpolation based on distance
                float distance = (transform.position - victims[i].gameObject.transform.position).magnitude;
                Debug.Log(distance);

                //This returns a float that is multiplied with damage to interpolate based on distance
                float damageDealt = Mathf.InverseLerp(outerRadius * Mathf.Pow (1.3f, power), innerRadius * Mathf.Pow (1.3f, power), distance);
                Debug.Log(damageDealt * damage * Mathf.Pow (1.3f, power));

                //deal damage
                if (victims[i].gameObject != null)
                {
                    health.takeDamage(damageDealt);
                }
            }
        }
    }

    private void OnDestroy()
    {
        dealAOEDamage();
    }

    public void onDeath()
    {
        dealAOEDamage();
    }

	public void playBoom(){
		GameObject go = Instantiate (boom, transform.position, Quaternion.identity);
		ParticleSystem ps = go.GetComponent<ParticleSystem> ();
		Destroy (go, 1.0f);
		boomSound.Play();
	}

	public void overRideDamage(float f){
		power = f;
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class reloadMovements : MonoBehaviour {

	// [Range(0f, 90f)]
	//public float face_threshold = 10f;
	private Transform right_transform;
	private GameObject pilot;
	private Vector3 originalPos;
	private Vector3 currentPos;
	private Quaternion originalRot;
	private Quaternion currentRot;
	private Quaternion targetRot;
	private bool initialReady;

	private Vector3 rot;
	private bool downReady;
	GameObject gun;
	GeneralShootingScript generalShootingScript;

	// public bool reloadDone = false;

		void Start () {
		right_transform = GameObject.Find("righthand_base").transform;
		originalPos.y = -100; 
		originalRot = right_transform.localRotation;
		downReady = false;
		GameObject gun = GameObject.Find("Gun");
		GeneralShootingScript generalShootingScript = GetComponent<GeneralShootingScript>();
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		generalShootingScript = GetComponent<GeneralShootingScript>();
        /*if(generalShootingScript.magazine == 0 && !initialReady){//to be replaced with something indicating reload is ready to occur
			// Debug.Log("yes");
			getInitialNums();
			initialReady = true;
		}

		currentPos = right_transform.localPosition;
		currentRot = right_transform.localRotation;

		if(targetRot.eulerAngles.y - currentRot.eulerAngles.y >= 95 && !downReady && initialReady){ //numbers may need fine tuning
			// Debug.Log("rotation works");
			targetRot = currentRot;
			downReady = true; //indicates ready to move down to reload
		}

		if((originalPos.y - currentPos.y) > .2 && downReady){ //numbers may need fine tuning
			// Debug.Log("down works"); //reload action is done
			generalShootingScript.reloadInput = true;
			// reloadDone = true;
			initialReady = false;
			originalPos.y = -100; //resets initial position in order to not have instant reloads
			downReady = false; 
		}*/
        if (Input.GetKey(KeyCode.Y))
        {
            generalShootingScript.reloadInput = true;
        }

	}
	
	public void getInitialNums(){
		originalPos = right_transform.localPosition;
		originalRot = right_transform.localRotation;
		rot = originalRot.eulerAngles;
		rot = new Vector3(rot.x,rot.y+90,rot.z);
		targetRot = Quaternion.Euler(rot);
		// Debug.Log(originalRot.eulerAngles.y);
		// Debug.Log(targetRot.eulerAngles.y);
	}

}



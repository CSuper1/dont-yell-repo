﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletDamageScript : MonoBehaviour {

    public GameObject sparks;
    public float power = 0.0f;
    public float damage = 17.0f;
    public bool hitF = false, hitB = false, hitL = false, hitR = false;
    public handShield hShield;
    public int timeIn = 150;

    public string owner = "enemy";

    void OnTriggerEnter(Collider other)
    {
        HealthScript dmgScript = other.gameObject.GetComponentInParent<HealthScript>();
        if (owner == "enemy" && (other.gameObject.tag == "Elite" || other.gameObject.tag == "VIPEnemy" || other.gameObject.tag == "Enemy" || other.gameObject.tag == "Hacker" || other.gameObject.tag == "COyellow" || other.gameObject.tag == "COcyan" || other.gameObject.tag == "COpink"))
        {

        }
		else if (dmgScript != null) {
			//The first 4 statements will check for different colliders. Used for player 
			if (other.gameObject.name == "hitFront") {
				dmgScript.hitFront (damage);
			} else if (other.gameObject.name == "hitBack") {
				dmgScript.hitBack (damage);
			} else if (other.gameObject.name == "hitLeft") {
				dmgScript.hitLeft (damage);
			} else if (other.gameObject.name == "hitRight") {
				dmgScript.hitRight (damage);
			} else {
				// place particle effect here asdfasdfasdfasdf
				dmgScript.takeDamage (damage * Mathf.Pow (1.3f, power)); //last statement for use when attached to an enemy
                
                /*
				GameObject go = Instantiate (sparks, transform.position, Quaternion.identity);
				ParticleSystem ps = go.GetComponent<ParticleSystem> ();
				Destroy (go, ps.main.duration);
                */
                
			}

		} else {
			if (other.gameObject.tag == "handShield") {
				hShield.hitpts -= 1;
				hShield.startRegen = false;
				hShield.takeHit = true;
			}
		}


        if (!other.gameObject.CompareTag("Player") && !other.gameObject.CompareTag(gameObject.tag) && other.gameObject.name != "hitRight" && other.gameObject.name != "hitLeft" && other.gameObject.name != "hitFront")
        {
            Destroy(this.gameObject);
        }


    }


	// Use this for initialization
	void Start () {
        hShield = GameObject.Find ("Shield Model").GetComponent<handShield> ();
  	}
	
	// Update is called once per frame
	void Update () {

	}

    //override damage
    public void overRideDamage(float f)
    {
        power = f;
    }

    public void setOwner(string s)
    {
        owner = s;
    }
}

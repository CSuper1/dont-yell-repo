﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MuzzleTransformOverride : MonoBehaviour {

    public GameObject[] muzzles;

	// Use this for initialization
	void Start () {
        muzzles = GameObject.FindGameObjectsWithTag("LauncherTube");
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public GameObject[] getOverrides()
    {
        return muzzles;
    }
}

﻿// Shan Lin
// 1/17/2017
// This script handles shooting mechanics and hit detection
// Currently only lasers (hitscan) but will eventually handle projectiles

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GeneralShootingScript : MonoBehaviour {

	public static float rateOfFire = 450.0f;
	public float range = 50.0f;
	public GameObject gun;
	public Rigidbody bullet;
	public float damage = 17.0f;
	public float ProjectileForce = 500.0f;
	public int magazine = 30;
	public float reloadTime = 2.4f;
	public float timeToLive = 1.5f;
	public float dispersion;
	public float power = 0.0f;

	public LineRenderer lineRenderer;
	public AudioSource gunShot;
	public AudioSource loop;
	public AudioSource shotEnd;
	private WaitForSeconds shotLength = new WaitForSeconds(0.5f);//new WaitForSeconds(0.07f);
	private float nextFireTime;
	private float shotInterval = 60 / rateOfFire;
	private bool isLoaded = true;
	private int magazineCapacity;
	private float reloadedTimeGate;
	private bool isReloading = false;
	public bool reloadInput = false;
	private Vector3 initialDirection;
	private Vector3 rayOrigin;
	private float spread;
	private float rot;
	private MuzzleTransformOverride muzzlescript;
	private GameObject[] muzzles = new GameObject[1];
	private ReloadOverride reloader;
	private int currMuzzle;
	public CrosshairScript cross;
	public Control cScript;
    private bool lastFired = false;

	public powerMachineGun mgScript;
	public powerLaserGun lsScript;

	void Start()
	{
		if(GameObject.Find("Machine Gun Component") != null)
			mgScript = GameObject.Find("Machine Gun Component").GetComponent<powerMachineGun>();
		if(GameObject.Find("Laser Component") != null)
			lsScript = GameObject.Find("Laser Component").GetComponent<powerLaserGun>();
		lineRenderer = GetComponent<LineRenderer>();
		gunShot = GetComponent<AudioSource>();
		gun = GameObject.FindGameObjectWithTag("Muzzle");
		lineRenderer.enabled = false;
		//gunShot.mute = true;
		magazineCapacity = 50;
		initialDirection = gun.transform.forward;
		dispersion = 1.0f;
		muzzlescript = gameObject.GetComponent<MuzzleTransformOverride>();
		reloader = gameObject.GetComponent<ReloadOverride>();
		cross = gameObject.GetComponentInChildren<CrosshairScript> ();
		if(muzzlescript == null)
		{
			muzzles[0] = gun;
		}
		else
		{
			muzzles = muzzlescript.getOverrides();
		}
		currMuzzle = 0;
		//gun = GetComponentInParent<righthand_base.gameObject>();
	}

	// Update is called once per frame
	void FixedUpdate ()
	{
		//Prepare Raycast and projectile spawn origin
		//Prepare this frame's dispersion
		rayOrigin = gun.transform.position;
        lineRenderer.SetPosition(0, rayOrigin);
        //Debug.Log(muzzles.Length);
        if (muzzles.Length > 1)
		{
			//currMuzzle = reloader.firstLoaded();
			rayOrigin = muzzles[currMuzzle].transform.position;
		}
		spread = dispersion * Mathf.Deg2Rad;
		//Debug.Log(dispersion + " " + spread);
		//initialDirection[0] = Mathf.Sin(spread);
		//initialDirection[2] = Mathf.Cos(spread);
		rot = Random.Range(0.0f, 360.0f) * Mathf.Deg2Rad;
		//Debug.Log(rot);
		//initialDirection[1] = Mathf.Sin(rot) * initialDirection[0];
		//initialDirection[0] *= Mathf.Cos(rot);
		//rot = Random.Range(0.0f, 360.0f);
		initialDirection.Set(Mathf.Sin(spread) * Mathf.Cos(rot), Mathf.Sin(spread) * Mathf.Sin(rot), Mathf.Cos(spread));

		Vector3.Normalize(initialDirection);
		/*RaycastHit hit;
		//StartCoroutine(LaserShotEffect());
		lineRenderer.SetPosition(0, rayOrigin);

		//Raycast
		if (Physics.Raycast(rayOrigin, gun.transform.forward, out hit, range))
		{

			
			//Set up destination as impact point
			lineRenderer.SetPosition(1, hit.point);

		}
		else
		{
			//Set up destination to be 50m forward of barrel
			lineRenderer.SetPosition(1, rayOrigin + (gun.transform.forward * range));
		}*/

		//Debug.Log(rayOrigin);

		//Can the player fire?

		//Check remaining bullets and only happens if weapon uses bullets
		if (bullet != null && magazine <= 0 && isLoaded == true)
		{
			//Debug.Log("No bullets");
			setIsLoaded(false);
		}

		//Check if reloading is done only happens if the weapon isReloading and if the weapon uses bullets
		//If reload time has elapsed, refresh magazine and reset booleans
		if (bullet != null && isReloading && Time.time > reloadedTimeGate)
		{
			//Debug.Log("Reloaded");
			magazine += magazineCapacity;
			setIsLoaded(true);
			isReloading = false;
			setInput(false);
		}

		//Check for input, fire interval, and if the weapon is loaded
		if (OVRInput.Get (OVRInput.RawButton.RIndexTrigger) && Time.time > nextFireTime && isLoaded) {
			fire ();


		}
		if(OVRInput.GetDown(OVRInput.RawButton.RIndexTrigger) && magazine > 0){
			if(bullet == null){
				lineRenderer.enabled = true;
				lineRenderer.startWidth = 0.1f;
                lineRenderer.endWidth = 0.5f * Mathf.Pow(1.3f, lsScript.dmg);
				//lineRenderer.SetPosition(0, rayOrigin);
                cScript.speed = 0.0f;
            }
            if (shotEnd.isPlaying)
            {
                shotEnd.Stop();
            }
			gunShot.Play();
		}
		else if(OVRInput.Get(OVRInput.RawButton.RIndexTrigger) && gunShot.isPlaying == false && magazine > 0)
		{
			if(bullet == null){
				lineRenderer.enabled = true;
                lineRenderer.startWidth = 0.1f;
                lineRenderer.endWidth = 0.5f * Mathf.Pow(1.3f, lsScript.dmg);
                //lineRenderer.SetPosition(0, rayOrigin);
                cScript.speed = 0.0f;
            }
			if(loop.isPlaying == false){
				loop.Play();
			}

		}
		else if ((OVRInput.GetUp(OVRInput.RawButton.RIndexTrigger) || (OVRInput.Get(OVRInput.RawButton.RIndexTrigger) && magazine == 0)) && !lastFired)
		{
			lineRenderer.enabled = false;
            cScript.speed = 50.0f;
            gunShot.Stop();
			loop.Stop();
			shotEnd.Play();
            if (magazine == 0)
            {
                lastFired = true;
            }
		}


		 /*if (Input.GetKeyDown(KeyCode.R))
		 {
			 setInput(true);
		 }*/
		 //Debug.Log("Hello?");
		//If the weapon is unloaded, the reload input has been performed, and the weapon is not already in the process of reloading
		//This prevents repeated reload inputs from continuously shifting the time gate
		if(isLoaded == false && reloadInput == true && !isReloading)
		{
			//Debug.Log("Begin Reloading");
			isReloading = true;
			reloadedTimeGate = Time.time + reloadTime;
			setInput(false);
		}
	}

	private IEnumerator LaserShotEffect()
	{
		//gunShot.mute = false;
		if (!gunShot.isPlaying)
		{
			gunShot.Play();
		}

		//lineRenderer.enabled = true;
		yield return shotLength;
		//lineRenderer.enabled = false;
		//gunShot.mute = true;
	}

	private IEnumerator BallisticShotEffect()
	{
		//gunShot.mute = false;
		if (!gunShot.isPlaying)
		{
			gunShot.Play();
		}

		yield return shotLength;
		//gunShot.mute = true;
	}

	//Use this function to set isLoaded from outside
	public void setIsLoaded(bool loaded)
	{
		isLoaded = loaded;
	}

	//called from outside by support functionalities
	//will enable weapon to reload by setting flag to true
	public void setInput(bool inputCompleted)
	{
		reloadInput = inputCompleted;
        lastFired = false;
	}


	//Use this to add bullets from outside
	//primarily for rockets
	public void addRounds(int i)
	{
		magazine += i;
	}

	public void fire()
	{
		//Debug.Log(spread);
		//Debug.Log(initialDirection);
		//Debug.Log(initialDirection.x + " " + initialDirection.y + " " + initialDirection.z);
		//Set next time weapon can fire
		nextFireTime = Time.time + shotInterval;

		//If the projectile has a null ID, that means it's a hitscan
		//Otherwise, instantiate the projectile into the world
		if (bullet != null)
		{
            //StartCoroutine(BallisticShotEffect());

            //Projectile spawn, -1 magazine, pushed forward, TimeToLive set to 1.5s
            Rigidbody bulletCopy = Instantiate(bullet, rayOrigin, transform.rotation) as Rigidbody;//Rigidbody bulletCopy = Instantiate(bullet, rayOrigin, Quaternion.identity) as Rigidbody;
            magazine--;
			bulletCopy.AddForce(gun.transform.forward * ProjectileForce);
			BulletDamageScript bscript = bulletCopy.gameObject.GetComponent<BulletDamageScript>();
			bscript.setOwner("Player");
			if (GameObject.Find("Machine Gun Component") != null)
			{
				Debug.Log(bscript.ToString());
				Debug.Log(mgScript.dmg);
				bscript.overRideDamage(mgScript.dmg);
			}
			Destroy(bulletCopy.gameObject, timeToLive);
		}
		else
		{
			
			//Prepare Raycast destination and effects
			RaycastHit hit;
			//StartCoroutine(LaserShotEffect());

			lineRenderer.enabled = true;
			//lineRenderer.SetPosition(0, rayOrigin);

			if(GameObject.Find("Laser Component") != null){
			
				overRideDamage(lsScript.dmg);
			}

			//Raycast
			if (Physics.Raycast(rayOrigin, gun.transform.forward, out hit, range))
			{

				//If an enemy was struck, dmgScript will not be null
				//This means damage can be dealt

				

				HealthScript dmgScript = hit.collider.gameObject.GetComponent<HealthScript>();
				if (dmgScript != null)
				{
					Debug.Log ("Laser hit");
					if (hit.collider.gameObject.tag == this.tag)
					{
						dmgScript.takeDamage(damage * Mathf.Pow(1.3f, lsScript.dmg) * 2.0f);
					}
					else
					{
						dmgScript.takeDamage(damage * Mathf.Pow(1.3f, lsScript.dmg) * 0.0f);
					}
				}

				//Set up destination as impact point
				lineRenderer.SetPosition(1, hit.point);

			}
			else
			{
				//Set up destination to be 50m forward of barrel
				lineRenderer.SetPosition(1, rayOrigin + (gun.transform.forward * range));
			}

		}
		currMuzzle++;
		if(currMuzzle > muzzles.Length - 1)
		{
			currMuzzle = 0;
		}
	}

	public void overRideDamage(float f){
		power = f;	
	}

}

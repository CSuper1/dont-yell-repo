﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpdateAmmo : MonoBehaviour {

	private Text my_txt;
	private int gun;

	// Use this for initialization
	void Start () {
		my_txt = GetComponent<Text>();
		gun = GameObject.Find ("Gun").gameObject.GetComponent<GeneralShootingScript> ().magazine;
	}

	// Update is called once per frame
	void FixedUpdate () {
		gun = GameObject.Find ("Gun").gameObject.GetComponent<GeneralShootingScript> ().magazine;
		my_txt.text = "AMMO: " + gun.ToString();
	}
}

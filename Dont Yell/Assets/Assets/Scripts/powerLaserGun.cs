﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class powerLaserGun : MonoBehaviour
{

    public PoweredComponent poweredcomponent;
    public float dmg;

    // Use this for initialization
    void Start()
    {
        dmg = 0;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
		if (poweredcomponent.power == 0) {
			dmg = 0;
			GameObject.Find ("gun").gameObject.GetComponent<Renderer> ().material.color = Color.white;
		} else if (poweredcomponent.power == 1) {
			dmg = 1;
			GameObject.Find ("gun").gameObject.GetComponent<Renderer> ().material.color = Color.magenta;
		} else if (poweredcomponent.power == 2) {
			dmg = 2;
			GameObject.Find ("gun").gameObject.GetComponent<Renderer> ().material.color = Color.yellow;
		} else if (poweredcomponent.power == 3) {
			dmg = 3;
			GameObject.Find ("gun").gameObject.GetComponent<Renderer> ().material.color = Color.red;
		}

		if (poweredcomponent.heat >= 95) {
			dmg = -1;
			GameObject.Find ("gun").gameObject.GetComponent<Renderer> ().material.color = Color.black;
		}
    }
}

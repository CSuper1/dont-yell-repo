﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MissileController : MonoBehaviour {

	List<Transform> weapons = new List<Transform>();
	List<MissileButtonSet> missileButtons = new List<MissileButtonSet>();

	private void Update()
	{
		showCount();
	}

	// Use this for initialization
	void Start () {
		foreach(GameObject i in GameObject.FindGameObjectsWithTag("MissileButtonSet"))
		{
			missileButtons.Add(i.GetComponent<MissileButtonSet>());
		}
	}
	
	public void showCount()
	{
		int count = 0;
		foreach(MissileButtonSet i in missileButtons)
		{
			count += i.getCount();
		}
		Debug.Log(count);
	}

	public int getCount()
	{
		int count = 0;
		foreach (MissileButtonSet i in missileButtons)
		{
			count += i.getCount();
		}
		return count;
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MissileButton : MonoBehaviour {

	private bool myState;
	public Material on, off;
	private Renderer myColor;

	private void Start()
	{
		Debug.Log("wtf");
		myColor = GetComponent<Renderer>();
		myColor.material = off;
	}

	public bool getState()
	{
		return myState;
	}

	public void changeState()
	{
		myState = !myState;
		if (myState)
		{
			myColor.material = on;
			transform.parent.GetComponent<MissileButtonSet>().increaseCount(transform);
		}

		else
		{
			myColor.material = off;
			gameObject.transform.parent.GetComponent<MissileButtonSet>().decreaseCount();
		}
	}
}

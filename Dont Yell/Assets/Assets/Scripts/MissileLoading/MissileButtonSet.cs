﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MissileButtonSet : MonoBehaviour {

	private bool allSet;
	private MissileButton buttonScript;
	private int CurrentCount = 0;
	public int missileCount = 10;

	public bool statusCheck()
	{
		allSet = true;
		foreach (Transform child in transform)
		{
			buttonScript = child.GetComponent<MissileButton>();
			allSet = buttonScript.getState() && allSet;
		}
		return allSet;
	}

	public int getCount()
	{
		return CurrentCount;
	}

	public void increaseCount(Transform button)
	{
		CurrentCount++;

		if (transform.parent.GetComponent<MissileController>().getCount() > missileCount)
		{
			button.GetComponent<MissileButton>().changeState();
		}
		Debug.Assert(CurrentCount <= missileCount, "increaseCount is greater than " + missileCount + ".");
	}

	public void decreaseCount()
	{
		CurrentCount--;
		Debug.Assert(CurrentCount >= 0, "decreaseCount is less than 0.");
	}
}

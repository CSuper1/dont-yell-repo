﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Dragging : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    private Vector3 screenPoint;
    private Vector3 offset;
    public float Max_x;
    public float Max_y;
    public float Min_x;
    public float Min_y;
    private LineHandler l;
    private MiniGameScript m;

    void Start()
    {
        l = GameObject.Find("Lines").GetComponent<LineHandler>();
        m = GameObject.Find("MiniGame").GetComponent<MiniGameScript>();
    }

    public void OnBeginDrag (PointerEventData eventData)
    {
        screenPoint = Camera.main.WorldToScreenPoint(gameObject.transform.position);
        offset = gameObject.transform.position - Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z));
    }

    public void OnDrag(PointerEventData eventData)
    {
        Vector3 curScreenPoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z);
        Vector3 curPosition = Camera.main.ScreenToWorldPoint(curScreenPoint) + offset;
        transform.position = curPosition;
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        if (l.no_line_intersect)
        {
            m.level++;
            m.initial = true;
            l.no_line_intersect = false;
            l.timer = 0;
        }
    }
}
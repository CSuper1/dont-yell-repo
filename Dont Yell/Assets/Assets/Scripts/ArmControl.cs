﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArmControl : MonoBehaviour
{

    public OVRInput.Controller which;
    private Vector3 v;
    private Quaternion q;
    private bool armHacked = false;
    public bool sw = false;



    // Use this for initialization
    void Start()
    {
        if (this.gameObject == GameObject.Find("Right_Arm"))
        {
            which = OVRInput.Controller.RTouch;
            v = new Vector3(0.3f, -0.1f, 0.5f);
        }
        else
        {
            which = OVRInput.Controller.LTouch;
            v = new Vector3(-0.3f, -0.1f, 0.5f);
        }
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetKeyUp("space"))
        {
            sw = true;
        }

        if (sw)
        {
            sw = false;
            //if hacked then switch
            if (!armHacked)
            {
                if (this.gameObject == GameObject.Find("Right_Arm"))
                {
                    v = transform.localPosition - GameObject.Find("lefthand_base").transform.localPosition;
                    q = transform.localRotation * Quaternion.Inverse(GameObject.Find("lefthand_base").transform.localRotation);
                    which = OVRInput.Controller.LTouch;
                }
                else
                {
                    v = transform.localPosition - GameObject.Find("righthand_base").transform.localPosition;
                    q = transform.localRotation * Quaternion.Inverse(GameObject.Find("righthand_base").transform.localRotation);
                    which = OVRInput.Controller.RTouch;
                }
            }
            else
            {
                if (this.gameObject == GameObject.Find("Right_Arm"))
                {
                    which = OVRInput.Controller.RTouch;
                    v = new Vector3(0.3f, -0.1f, 0.5f);
                }
                else
                {
                    which = OVRInput.Controller.LTouch;
                    v = new Vector3(-0.3f, -0.1f, 0.5f);
                }
            }
            armHacked = !armHacked;
        }

        transform.localPosition = OVRInput.GetLocalControllerPosition(which) + v;
        if (armHacked)
            transform.localRotation = OVRInput.GetLocalControllerRotation(which) * q;
        else                               
            transform.localRotation = OVRInput.GetLocalControllerRotation(which);
    }
}

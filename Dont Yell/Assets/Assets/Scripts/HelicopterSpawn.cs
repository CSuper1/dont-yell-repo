﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HelicopterSpawn : MonoBehaviour {

    public Transform helicopterPrefab;
    private int random = 0;
    private int timer = 0;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        timer++;
        GameObject[] elites = GameObject.FindGameObjectsWithTag("Helicopter");
        if (elites.Length < 6 && timer % 100 == 0)
        {
            random = Random.Range(0, 3);
            Instantiate(helicopterPrefab, transform.GetChild(random).transform.position, transform.rotation);
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class enemySpawn : MonoBehaviour {
	public int maxSpawn; //limit of enemies from particular spawner
	public static int currentSpawn;
	public int enemyTypes;
	public float waitTime;//allows time between spawns to be controlled
    public NavMeshAgent agent;
    public bool isVIPEnabled;

	//allows prefabs to be added to spawner
	[SerializeField] private GameObject gruntPrefab;
	[SerializeField] private GameObject elitePrefab;
    [SerializeField] private GameObject pinkPrefab;
    [SerializeField] private GameObject yellowPrefab;
    [SerializeField] private GameObject cyanPrefab;

    private GameObject _enemy;

	private int count;
    public int groupNum;
    private Vector3 pos;

	// Use this for initialization
	void Start () {
		currentSpawn = 0; //initial spawn number

		count = 0;

        //switch for VIP Mode
        //Use an Empty Object titled VIPManager to enable/disable
        if (GameObject.Find("VIPManager") != null)
        {
            isVIPEnabled = true;
        }
        else
        {
            isVIPEnabled = false;
        }
		StartCoroutine(spawnWait()); //starts spawner

    }
	
	// Update is called once per frame
	void FixedUpdate () {
        if (Input.GetKeyUp("right"))
            maxSpawn++;
        else if (Input.GetKeyUp("left"))
            maxSpawn--;
	}

	void spawn(){

        //groupNum = Random.Range(1, 5);//(5, 10);

		if(currentSpawn < maxSpawn) {

            //int a = Random.Range(0, enemyTypes); //picks a random integer that decides which enemy spawns

            /*
			switch(a){
				case 0:
				    _enemy = Instantiate(enemyPrefab) as GameObject;
                    if (count == 10 && isVIPEnabled)
                    {
                        _enemy.transform.localScale = new Vector3(2.2f, 2.2f, 2.2f);
                        _enemy.tag = "VIPEnemy";
                        isVIPEnabled = false;
                    }
                    agent = _enemy.GetComponent<NavMeshAgent>();
                    agent.enabled = false;
                    break;
				case 1:
				_enemy = Instantiate(enemyPrefab2) as GameObject;
				break;	
			}
            */

            count++;
            pos = transform.GetChild(Random.Range(0, 3)).transform.position;

            if(currentSpawn == 1)
            {
                int a = Random.Range(0, 3);
                if(a == 0)
                    _enemy = Instantiate(pinkPrefab) as GameObject;
                else if(a == 1)
                    _enemy = Instantiate(yellowPrefab) as GameObject;
                else if(a == 2)
                    _enemy = Instantiate(cyanPrefab) as GameObject;
                _enemy.transform.position = pos;
            }

            for (int i = 0; i < groupNum; i++)
            {
                if (i == 0)
                    _enemy = Instantiate(elitePrefab) as GameObject;
                else
                    _enemy = Instantiate(gruntPrefab) as GameObject;
                /*
                if (count == 10 && isVIPEnabled)
                {
                    _enemy.transform.localScale = new Vector3(2.2f, 2.2f, 2.2f);
                    _enemy.tag = "VIPEnemy";
                    isVIPEnabled = false;
                }
                */
                _enemy.transform.position = pos;
                _enemy.GetComponent<BehaviorTreeClass>().TeamNum = currentSpawn;
            }
            currentSpawn += 1;

        }
		StartCoroutine(spawnWait());
	}

	private IEnumerator spawnWait() {
		yield return new WaitForSeconds(waitTime);
		spawn();

	}
}

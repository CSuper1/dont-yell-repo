﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class TankAI : MonoBehaviour {

    public Transform bullet;
    public Transform explosion;

    private float enemyReloadTime = 3.6f;
    private bool allowfire = true;
    private int magCap = 20;
    private int enemyMag = 20;

    private NavMeshAgent agent;
    private GameObject player;
    private bool desFound = false;
    private Vector3 targetPostition;
    private Vector3 tempV;

    // Use this for initialization
    void Start () {
        agent = GetComponent<NavMeshAgent>();
        player = GameObject.Find("Pilot");
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        if (GameObject.Find("TimeShield(Clone)") && Vector3.Distance(GameObject.Find("TimeShield(Clone)").transform.position, transform.position) < 130f)
        {
            agent.speed = 0.1f;
        }
        else
        {
            agent.speed = 20f;
        }

        if (!Physics.Linecast(transform.position, player.transform.position, 3))
        {
            targetPostition = new Vector3(player.transform.position.x, this.transform.GetChild(1).position.y, player.transform.position.z);
            this.transform.GetChild(1).LookAt(targetPostition);
        }

        if (Vector3.Distance(player.transform.position, transform.position) < 2000f)
        {
            //can't see player
            if (Physics.Linecast(transform.position + transform.up * 10f, player.transform.position, 3))
            {
                //find a new destination
                if (!desFound)
                {
                    for (int i = 0; i < 100; i++)
                    {
                        tempV = new Vector3(Random.Range(-700, 320), 401, Random.Range(-1040, -200));

                        if (!Physics.Linecast(tempV, player.transform.position, 3))
                        {
                            agent.SetDestination(tempV);
                            if (agent.hasPath)
                            {
                                desFound = true;
                                break;
                            }
                        }
                    }
                    if (Vector3.Distance(transform.position, agent.destination) < 30f)
                    {
                        desFound = false;
                    }
                }
            }
            //see player
            else
            {
                desFound = false;
                //stop
                agent.SetDestination(transform.position);

                //look at player
                targetPostition = new Vector3(player.transform.position.x, this.transform.GetChild(1).position.y, player.transform.position.z);
                this.transform.GetChild(1).LookAt(targetPostition);

                //fire
                if (allowfire)
                {
                    StartCoroutine(Fire());
                }
                if (enemyMag <= 0)
                {
                    StartCoroutine(Reload());
                    allowfire = false;
                }
            }

        }

        GameObject[] bullets = GameObject.FindGameObjectsWithTag("Bullet");
        foreach (GameObject b in bullets)
        {
            if (Vector3.Distance(transform.position, b.transform.position) < 20f && b.name == "Bu(Clone)")
            {
                Instantiate(explosion, transform.position, transform.rotation);
                Destroy(this.gameObject);
            }
        }
    }

    IEnumerator Fire()
    {
        allowfire = false;
        Instantiate(bullet, transform.GetChild(1).transform.position + transform.GetChild(1).transform.forward * 10f, transform.GetChild(1).transform.rotation);
        enemyMag--;
        float shootTime = 1;
        if (tag == "Tank") shootTime = 2;
        yield return new WaitForSeconds(shootTime);
        allowfire = true;
    }

    //Reload for the enemy
    IEnumerator Reload()
    {
        yield return new WaitForSeconds(enemyReloadTime);
        enemyMag = magCap;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponVisibility : MonoBehaviour {
	public GameObject pilot;
	private WeaponChangeScript weaponsystem = null;
	public int weaponid = -1;
	private MeshRenderer renderer;

	// Use this for initialization
	void Start () {
		weaponsystem = pilot.GetComponent<WeaponChangeScript> ();
		renderer = GetComponent<MeshRenderer> ();
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		renderer.enabled = weaponsystem.weaponIndex == weaponid;
	}
}

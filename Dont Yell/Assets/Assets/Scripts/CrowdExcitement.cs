﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrowdExcitement : MonoBehaviour {

	public double excitement = 0.0;

	public void addKill() {
		excitement += 1.0;
	}
	public void addOverkill(double amt) {
		excitement += amt / 10.0;
	}

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SupportCamMovement : MonoBehaviour {

    public Camera cam;
    public bool radar;
    public bool miniGame;
    public bool weaponChanging;
    public bool main;
    public bool missile;
    public bool reloading;
    public bool unhacking;
    public bool boost;
    public bool camera;
    public bool shield;
    private Vector3 mainPos;
    private Vector3 mainPos_;
    private Vector3 temp;
    private Vector3 temp_;
    private Vector3 radarPos;
    private Vector3 radarPos_;
    private Vector3 miniGamePos;
    private Vector3 miniGamePos_;
    private Vector3 weaponChangingPos;
    private Vector3 weaponChangingPos_;
    private Vector3 missilePos;
    private Vector3 missilePos_;
    private Vector3 reloadingPos;
    private Vector3 reloadingPos_;
    private Vector3 unhackingPos;
    private Vector3 unhackingPos_;
    private Vector3 boostPos;
    private Vector3 boostPos_;
    private Vector3 cameraPos;
    private Vector3 cameraPos_;
    private Vector3 shieldPos;
    private Vector3 shieldPos_;

    // Use this for initialization
    void Start () {
        radar = false;
        miniGame = false;
        weaponChanging = false;
        reloading = false;
        missile = false;
        boost = false;
        camera = false;
        shield = false;
        mainPos = new Vector3(0, 0, 0);
        radarPos = new Vector3(0f, 75f, 0);
        radarPos_ = new Vector3(0f, 7.5f, 0);
        miniGamePos = new Vector3(-88f, 75f, 0);
        miniGamePos_ = new Vector3(-8.8f, 7.5f, 0);
        weaponChangingPos = new Vector3(-88f, -75f, 0);
        weaponChangingPos_ = new Vector3(-8.8f, -7.5f, 0);
        missilePos = new Vector3(88f, 75f, 0);
        missilePos_ = new Vector3(8.8f, 7.5f, 0);
        reloadingPos = new Vector3(0, -75f, 0);
        reloadingPos_ = new Vector3(0, -7.5f, 0);
        unhackingPos = new Vector3(88f, -75f, 0);
        unhackingPos_ = new Vector3(8.8f, -7.5f, 0);
        boostPos = new Vector3(88f, 0, 0);
        boostPos_ = new Vector3(8.8f, 0, 0);
        cameraPos = new Vector3(-88f, -30f, 0);
        cameraPos_ = new Vector3(-8.8f, -3f, 0);
        shieldPos = new Vector3(-88f, 30f, 0);
        shieldPos_ = new Vector3(-8.8f, 3f, 0);
    }
	
	// Update is called once per frame
	void FixedUpdate () {
        if (radar)
        {
            if (cam.transform.position != radarPos)
                cam.transform.position += radarPos_;
            else
            {
                radar = false;
                temp = radarPos_;
            }
        }
        if (miniGame)
        {
            if (cam.transform.position != miniGamePos)
                cam.transform.position += miniGamePos_;
            else
            {
                miniGame = false;
                temp = miniGamePos_;
            }
        }
        if (weaponChanging)
        {
            if (cam.transform.position != weaponChangingPos)
                cam.transform.position += weaponChangingPos_;
            else
            {
                weaponChanging = false;
                temp = weaponChangingPos_;
            }
        }
        if (missile)
        {
            if (cam.transform.position != missilePos)
                cam.transform.position += missilePos_;
            else
            {
                missile = false;
                temp = missilePos_;
            }
        }
        if (reloading)
        {
            if (cam.transform.position != reloadingPos)
                cam.transform.position += reloadingPos_;
            else
            {
                reloading = false;
                temp = reloadingPos_;
            }
        }
        if (unhacking)
        {
            if (cam.transform.position != unhackingPos)
                cam.transform.position += unhackingPos_;
            else
            {
                unhacking = false;
                temp = unhackingPos_;
            }
        }
        if (boost)
        {
            if (cam.transform.position != boostPos)
                cam.transform.position += boostPos_;
            else
            {
                boost = false;
                temp = boostPos_;
            }
        }
        if (camera)
        {
            if (cam.transform.position != cameraPos)
                cam.transform.position += cameraPos_;
            else
            {
                camera = false;
                temp = cameraPos_;
            }
        }
        if (shield)
        {
            if (cam.transform.position != shieldPos)
                cam.transform.position += shieldPos_;
            else
            {
                shield = false;
                temp = shieldPos_;
            }
        }
        if (main){
            if(Vector3.Distance(mainPos, cam.transform.position) > 0.1f)
            {
                cam.transform.position -= temp;
            }
            else
            {
                main = false;
            }
        }
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class NewMirrorHandScript : MonoBehaviour
{
    [Range(0.0f, 0.1f)]
    public float transpeed = 0.004f;
    [Range(0.0f, 10f)]
    public float rotspeed = 2f;
    public GameObject handbase;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
	void FixedUpdate()
    {
        if (handbase)
        {
            Vector3 pos_difference = handbase.transform.localPosition - transform.localPosition + Vector3.right;
            float distance = Mathf.Min(pos_difference.magnitude, transpeed);
            transform.localPosition += pos_difference.normalized * distance;

            float angle_difference = Quaternion.Angle(handbase.transform.localRotation, transform.localRotation);
            float angle_dist = Mathf.Min(angle_difference, rotspeed);
            transform.localRotation = Quaternion.RotateTowards(transform.localRotation, handbase.transform.localRotation, angle_dist);
        }
    }
}

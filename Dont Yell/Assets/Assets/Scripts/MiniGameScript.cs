﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MiniGameScript : MonoBehaviour {

    public int level;
    public bool initial = true;
    private DrawLine[] m;
    private GameObject miniGame;
    private NewDrawLine level1;
    private NewDrawLine level2;
    private NewDrawLine level3;
    private NewDrawLine level4;
    private NewDrawLine level5;
    private NewDrawLine level6;
    private TextMesh textM;

    // Use this for initialization
    void Start () {
        level = 1;
        miniGame = GameObject.Find("MiniGame");
        level1 = GameObject.Find("Level_1").GetComponent<NewDrawLine>();
        level2 = GameObject.Find("Level_2").GetComponent<NewDrawLine>();
        level3 = GameObject.Find("Level_3").GetComponent<NewDrawLine>();
        level4 = GameObject.Find("Level_4").GetComponent<NewDrawLine>();
        level5 = GameObject.Find("Level_5").GetComponent<NewDrawLine>();
        level6 = GameObject.Find("Level_6").GetComponent<NewDrawLine>();
        textM = GameObject.Find("oneIStatus").GetComponent<TextMesh>();
    }
	
	// Update is called once per frame
	void FixedUpdate () {
        if(level == 1 && initial)
        {
            initial = false;
            level1.enabled = true;
        }
        else if(level == 2 && initial)
        {
            initial = false;
            clear_level_1();
            level2.enabled = true;
            textM.text = "Status:\nDecrypt 1 / 5";
        }
        else if (level == 3 && initial)
        {
            initial = false;
            clear_level_2();
            level3.enabled = true;
            textM.text = "Status:\nDecrypt 2 / 5";
        }
        else if(level == 4 && initial)
        {
            initial = false;
            clear_level_3();
            level4.enabled = true;
            textM.text = "Status:\nDecrypt 3 / 5";
        }
        else if (level == 5 && initial)
        {
            initial = false;
            clear_level_4();
            level5.enabled = true;
            textM.text = "Status:\nDecrypt 4 / 5";
        }
        else if(level == 6 && initial)
        {
            initial = false;
            clear_level_5();
            level6.enabled = true;
            textM.text = "Status:\nDecrypt 5 / 5\nObstacle\nDestroy";
            if (GameObject.Find("Obstacle_1"))
                Destroy(GameObject.Find("Obstacle_1"));
        }
        else if(level == 7 && initial)
        {
            initial = false;
            clear_level_6();
            textM.text = "Status:\nDecrypt 6 / 6\nObstacle\nDestroy";
            if (GameObject.Find("Obstacle_2"))
                Destroy(GameObject.Find("Obstacle_2"));
        }
    }

    void clear_level_1()
    {
        level1.enabled = false;
        foreach (Transform child in GameObject.Find("Level_1").transform)
        {
            Destroy(child.gameObject);
        }
        GameObject.Find("Cir1").transform.localPosition = new Vector3(-1,-31,0);
        GameObject.Find("Cir2").transform.localPosition = new Vector3(31, 11, 0);
        GameObject.Find("Cir3").transform.localPosition = new Vector3(-3, 17, 0);
        GameObject.Find("Cir4").transform.localPosition = new Vector3(-31, 2, 0);
        GameObject.Find("Cir5").transform.localPosition = new Vector3(-23, -25, 0);
        miniGame.transform.GetChild(6).gameObject.SetActive(true);
        GameObject.Find("Cir6").transform.localPosition = new Vector3(41, -25, 0);
    }

    void clear_level_2()
    {
        level2.enabled = false;
        foreach (Transform child in GameObject.Find("Level_2").transform)
        {
            Destroy(child.gameObject);
        }
        GameObject.Find("Cir1").transform.localPosition = new Vector3(20, 12, 0);
        GameObject.Find("Cir2").transform.localPosition = new Vector3(36, 27, 0);
        GameObject.Find("Cir3").transform.localPosition = new Vector3(-13, 27, 0);
        GameObject.Find("Cir4").transform.localPosition = new Vector3(-29, 12, 0);
        GameObject.Find("Cir5").transform.localPosition = new Vector3(-13, -12, 0);
        GameObject.Find("Cir6").transform.localPosition = new Vector3(36, -12, 0);
        miniGame.transform.GetChild(7).gameObject.SetActive(true);
        GameObject.Find("Cir7").transform.localPosition = new Vector3(20, -27, 0);
        miniGame.transform.GetChild(8).gameObject.SetActive(true);
        GameObject.Find("Cir8").transform.localPosition = new Vector3(-29, -29, 0);
    }

    void clear_level_3()
    {
        level3.enabled = false;
        foreach (Transform child in GameObject.Find("Level_3").transform)
        {
            Destroy(child.gameObject);
        }
        GameObject.Find("Cir1").transform.localPosition = new Vector3(20, 12, 0);
        GameObject.Find("Cir2").transform.localPosition = new Vector3(20, -12, 0);
        GameObject.Find("Cir3").transform.localPosition = new Vector3(0, 27, 0);
        GameObject.Find("Cir4").transform.localPosition = new Vector3(-20, 12, 0);
        GameObject.Find("Cir5").transform.localPosition = new Vector3(0, 0, 0);
        GameObject.Find("Cir6").transform.localPosition = new Vector3(-20, -12, 0);
        GameObject.Find("Cir7").transform.localPosition = new Vector3(0, -27, 0);
        GameObject.Find("Cir8").SetActive(false);
    }

    void clear_level_4()
    {
        level4.enabled = false;
        foreach (Transform child in GameObject.Find("Level_4").transform)
        {
            Destroy(child.gameObject);
        }
        GameObject.Find("Cir1").transform.localPosition = new Vector3(20, 26, 0);
        GameObject.Find("Cir2").transform.localPosition = new Vector3(20, -26, 0);
        GameObject.Find("Cir3").transform.localPosition = new Vector3(0, 30, 0);
        GameObject.Find("Cir4").transform.localPosition = new Vector3(-20, 26, 0);
        GameObject.Find("Cir5").transform.localPosition = new Vector3(-20, -26, 0);
        GameObject.Find("Cir6").transform.localPosition = new Vector3(-36, -10, 0);
        GameObject.Find("Cir7").transform.localPosition = new Vector3(0, -30, 0);
        miniGame.transform.GetChild(8).gameObject.SetActive(true);
        GameObject.Find("Cir8").transform.localPosition = new Vector3(-36, 10, 0);
        miniGame.transform.GetChild(9).gameObject.SetActive(true);
        GameObject.Find("Cir9").transform.localPosition = new Vector3(36, -10, 0);
        miniGame.transform.GetChild(10).gameObject.SetActive(true);
        GameObject.Find("Cir10").transform.localPosition = new Vector3(36, 10, 0);
    }

    void clear_level_5()
    {
        level5.enabled = false;
        foreach (Transform child in GameObject.Find("Level_5").transform)
        {
            Destroy(child.gameObject);
        }
        /*
        GameObject.Find("Cir1").SetActive(false);
        GameObject.Find("Cir2").SetActive(false);
        GameObject.Find("Cir3").SetActive(false);
        GameObject.Find("Cir4").SetActive(false);
        GameObject.Find("Cir5").SetActive(false);
        GameObject.Find("Cir6").SetActive(false);
        GameObject.Find("Cir7").SetActive(false);
        GameObject.Find("Cir8").SetActive(false);
        GameObject.Find("Cir9").SetActive(false);
        GameObject.Find("Cir10").SetActive(false);
        */
        GameObject.Find("Cir1").transform.localPosition = new Vector3(-5, 30, 0);
        GameObject.Find("Cir2").transform.localPosition = new Vector3(5, 30, 0);
        GameObject.Find("Cir3").transform.localPosition = new Vector3(-5, -30, 0);
        GameObject.Find("Cir4").transform.localPosition = new Vector3(5, -30, 0);
        GameObject.Find("Cir5").transform.localPosition = new Vector3(-30, 5, 0);
        GameObject.Find("Cir6").transform.localPosition = new Vector3(-30, -5, 0);
        GameObject.Find("Cir7").transform.localPosition = new Vector3(30, 5, 0);
        GameObject.Find("Cir8").transform.localPosition = new Vector3(30, -5, 0);
        GameObject.Find("Cir9").SetActive(false);
        GameObject.Find("Cir10").SetActive(false);

        GameObject.Find("oneI").transform.GetChild(0).gameObject.SetActive(true);
    }

    void clear_level_6()
    {
        level6.enabled = false;
        foreach (Transform child in GameObject.Find("Level_6").transform)
        {
            Destroy(child.gameObject);
        }
        GameObject.Find("Cir1").SetActive(false);
        GameObject.Find("Cir2").SetActive(false);
        GameObject.Find("Cir3").SetActive(false);
        GameObject.Find("Cir4").SetActive(false);
        GameObject.Find("Cir5").SetActive(false);
        GameObject.Find("Cir6").SetActive(false);
        GameObject.Find("Cir7").SetActive(false);
        GameObject.Find("Cir8").SetActive(false);
    }
}

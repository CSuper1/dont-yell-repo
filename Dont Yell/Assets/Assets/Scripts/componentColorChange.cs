﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
public class componentColorChange : MonoBehaviour {
	public PoweredComponent my_pc;
	private GameObject ht;
	private GameObject pt;
	private GameObject nt;
	private Image i;
	Color orange;

	// Use this for initialization
	void Start () {
		//my_pc = transform.parent.gameObject.GetComponent<PoweredComponent>();
		ht = my_pc.transform.Find ("HeatText").gameObject;
		pt = my_pc.transform.Find ("PowerText").gameObject;
		nt = my_pc.transform.Find ("NameText").gameObject;
		orange = new Color (255, 153, 0);
		i = my_pc.transform.FindChild ("UI Background").GetComponent<Image>();
	}
	
	// Update is called once per frame
	void Update () {
		if (my_pc == null)
			Debug.Log (name);
		if (my_pc.power > 0) {
			if (my_pc.heat > 75) {
				i.color = Color.red;
				ht.GetComponent<Text> ().color = Color.black;
				pt.GetComponent<Text> ().color = Color.cyan;
				nt.GetComponent<Text> ().color = orange;
			} else if (my_pc.heat > 50) {
				i.color = Color.yellow;
				nt.GetComponent<Text> ().color = Color.black;
				pt.GetComponent<Text> ().color = Color.cyan;
				ht.GetComponent<Text> ().color = Color.black;
			}
			else {
				i.color = Color.cyan;
				ht.GetComponent<Text> ().color = Color.red;
				pt.GetComponent<Text> ().color = Color.black;
				nt.GetComponent<Text> ().color = orange;
			}
		} else {
			if (my_pc.heat > 75) {
				i.color = Color.red;
				ht.GetComponent<Text> ().color = Color.black;
				pt.GetComponent<Text> ().color = Color.cyan;
				nt.GetComponent<Text> ().color = orange;
			} else if (my_pc.heat > 50) {
				i.color = Color.yellow;
				nt.GetComponent<Text> ().color = Color.black;
				pt.GetComponent<Text> ().color = Color.cyan;
				ht.GetComponent<Text> ().color = Color.black;
			} else {
				i.color = Color.black;
				ht.GetComponent<Text> ().color = Color.red;
				pt.GetComponent<Text> ().color = Color.cyan;
				nt.GetComponent<Text> ().color = orange;
			}
		}
	}
}

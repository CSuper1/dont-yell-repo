﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner2 : MonoBehaviour {

    public Transform elitePrefab;
    //public Transform helicopterPrefab;
    //public Transform hackerPrefab;
    private int random = 0;
    private int timer = 0;
    private int two = 0;
    private int three = 0;
    private int four = 0;
    private int five = 0;
    private int six = 0;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        timer++;
        GameObject[] elites = GameObject.FindGameObjectsWithTag("Elite");
        /*
        if (elites.Length < 5 && timer % 300 == 0)
        {
            random = Random.Range(0, 3);
            Transform temp = Instantiate(elitePrefab, transform.GetChild(random).transform.position, transform.rotation);
            temp.GetComponent<AI>().area = 2;
        }
        */
        foreach(GameObject e in elites)
        {
            switch (e.GetComponent<AI>().area)
            {
                case 2:
                    two++;
                    break;
                case 3:
                    three++;
                    break;
                case 4:
                    four++;
                    break;
                case 5:
                    five++;
                    break;
                case 6:
                    six++;
                    break;
            }
        }

        if(two < 1 && timer % 500 == 0)
        {
            Transform temp = Instantiate(elitePrefab, transform.GetChild(0).transform.position, transform.rotation);
            temp.GetComponent<AI>().area = 2;
        }
        if (three < 1 && timer % 500 == 0)
        {
            Transform temp = Instantiate(elitePrefab, transform.GetChild(Random.Range(1, 3)).transform.position, transform.rotation);
            temp.GetComponent<AI>().area = 3;
        }
        if (four < 1 && timer % 500 == 0)
        {
            Transform temp = Instantiate(elitePrefab, transform.GetChild(3).transform.position, transform.rotation);
            temp.GetComponent<AI>().area = 4;
        }
        if (five < 1 && timer % 500 == 0)
        {
            Transform temp = Instantiate(elitePrefab, transform.GetChild(Random.Range(4, 6)).transform.position, transform.rotation);
            temp.GetComponent<AI>().area = 5;
        }
        if (six < 1 && timer % 300 == 0)
        {
            Transform temp = Instantiate(elitePrefab, transform.GetChild(Random.Range(6, 11)).transform.position, transform.rotation);
            temp.GetComponent<AI>().area = 6;
        }
    }
}

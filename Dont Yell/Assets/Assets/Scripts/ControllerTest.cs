﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[RequireComponent(typeof(CharacterController))]
public class ControllerTest : MonoBehaviour
{
    public float speed = 3.0F;
    public float rotateSpeed = 3.0F;
    public Transform shield;
    public Transform time;
    public PoweredComponent poweredcomponent;
    private CharacterController controller;

    void Start()
    {
        controller = GetComponent<CharacterController>();
    }

    void Update()
    {

        if (Input.GetKey("space") && GameObject.Find("BoostBar").GetComponent<Slider>().value > 0)
        {
            //GameObject.Find("BoostBar").GetComponent<Image>().fillAmount -= 0.01f;
            controller.Move(transform.up * 5f);//transform.Translate(transform.up);
        }
        if (Input.GetKeyUp("f"))
        {
            Instantiate(time, GameObject.Find("Gun").transform.position, new Quaternion(-0.7f, 0, 0, 0.7f));
        }
        controller.Move(transform.up * -1f);
        transform.Rotate(0, Input.GetAxis("Horizontal") * rotateSpeed, 0);
        GameObject.Find("PlayerDot").transform.Rotate(0, 0, -Input.GetAxis("Horizontal") * rotateSpeed);
        Vector3 forward = transform.TransformDirection(Vector3.forward);
        float curSpeed = speed * Input.GetAxis("Vertical");
        //controller.SimpleMove(forward * curSpeed);
        controller.Move(forward * curSpeed * Time.deltaTime);
    }
}
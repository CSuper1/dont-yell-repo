﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CockpitGlow : MonoBehaviour {
	[Range(0f, 1f)]
	public float glow_amount = 0;

	public CockpitHeat cockpit_component;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		if (cockpit_component != null) {
			glow_amount = (float)cockpit_component.heat_visual;
		}
		foreach (Transform child in transform) {
			child.gameObject.GetComponent<Renderer> ().sharedMaterial.SetColor ("_EmissionColor", new Color (glow_amount, 0, 0));
		}
	}
}

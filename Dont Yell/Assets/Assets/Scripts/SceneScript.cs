﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SceneScript : MonoBehaviour {

    private bool level1_clear = false;
    public bool level2 = false;
    public bool level3 = false;
    public bool level4 = false;
    private bool spawn = false;
    private int timer = 0;
    private GameObject support;

    public Material g;
    public Material b;
    public Material r;

    private GameObject obstacle1;
    private GameObject obstacle2;
    private GameObject player;
    private GameObject spawner1;
    private GameObject spawner2;
    private GameObject spawner3;
    private TextMesh spawner1Text;
    private TextMesh spawner2Text;
    private TextMesh spawner3Text;
    private GameObject check1;
    private GameObject check2;
    private GameObject area2;
    private GameObject glass;

    // Use this for initialization
    void Start () {
        support = GameObject.Find("support");
        obstacle1 = GameObject.Find("Obstacle_1");
        obstacle2 = GameObject.Find("Obstacle_2");
        player = GameObject.Find("Pilot");
        spawner1 = GameObject.Find("Spawner1");
        spawner2 = GameObject.Find("Spawner2");
        spawner3 = GameObject.Find("Spawner3");
        spawner1Text = GameObject.Find("Spawner1Text").GetComponent<TextMesh>();
        spawner2Text = GameObject.Find("Spawner2Text").GetComponent<TextMesh>();
        spawner3Text = GameObject.Find("Spawner3Text").GetComponent<TextMesh>();
        check1 = GameObject.Find("CheckPoint1");
        check2 = GameObject.Find("CheckPoint2");
        area2 = GameObject.Find("Area2");
        glass = GameObject.Find("glass");
    }
	
	// Update is called once per frame
	void FixedUpdate () {
        if (obstacle1 == null && !level1_clear)
        {
            GameObject[] eiltes = GameObject.FindGameObjectsWithTag("Elite");
            foreach(GameObject g in eiltes)
            {
                Destroy(g);
            }
            GameObject[] tanks = GameObject.FindGameObjectsWithTag("Tank");
            foreach(GameObject g in tanks)
            {
                Destroy(g);
            }
            GameObject.Find("MiniGameI").GetComponent<Image>().color = Color.white;
            GetComponent<NewEnemySpawner>().enabled = false;
            level1_clear = true;
        }

        if(Vector3.Distance(player.transform.position, check1.transform.position) < 1500f)
        {
            level2 = true;
        }

        if (check2 != null && Vector3.Distance(player.transform.position, check2.transform.position) < 100f)
        {
            Destroy(check2.gameObject);
            level2 = false;
            level3 = true;
            GameObject.Find("Obstacles").transform.GetChild(0).gameObject.SetActive(true);
            GameObject.Find("glass").GetComponent<MeshRenderer>().material = b;
            for(int i = 0; i < 16; i++)
            {
                GameObject.Find("Level_6").transform.GetChild(i).gameObject.GetComponent<LineRenderer>().startWidth = 0.1f;
                GameObject.Find("Level_6").transform.GetChild(i).gameObject.GetComponent<LineRenderer>().endWidth = 0.1f;
            }
        }

        if(level3 && obstacle2 != null)
        {
            if(glass.GetComponent<MeshRenderer>().material != g)
                timer++;
            else
            {
                timer = 0;
            }
            if(timer == 50)
                glass.GetComponent<MeshRenderer>().material = g;
        }

        if(level3 && obstacle2 == null)
        {
            level3 = false;
            level4 = true;

            timer++;
            if (timer == 0)
            {
                support.transform.GetChild(12).gameObject.SetActive(true);
            }
            else if (timer == 50)
            {
                support.transform.GetChild(12).gameObject.SetActive(false);
            }
            else if (timer == 100)
            {
                support.transform.GetChild(12).gameObject.SetActive(true);
            }
            else if (timer == 150)
            {
                support.transform.GetChild(12).gameObject.SetActive(false);
            }
            else if (timer == 200)
            {
                support.transform.GetChild(12).gameObject.SetActive(true);
            }
            else if (timer == 250)
            {
                support.transform.GetChild(12).gameObject.SetActive(false);
            }
            if (!spawn) {
                spawn = true;
                GameObject.Find("PilotCamImage").GetComponent<MeshRenderer>().material = r;
                for (int i = 0; i < area2.transform.childCount; i++)
                {
                    area2.transform.GetChild(i).gameObject.SetActive(true);
                }
            }
        }
        

        if(spawner1 == null && spawner2 == null && spawner3 == null)
        {
            Application.LoadLevel("Ending");
        }

        if(spawner1 != null)
        {
            spawner1Text.text = "Spawner1 Health: " + spawner1.GetComponent<EnemyHealth>().hitPoints.ToString();
        }
        else
        {
            spawner1Text.text = "Spawner1: Destroy";
        }
        if (spawner2 != null)
        {
            spawner1Text.text = "Spawner2 Health: " + spawner2.GetComponent<EnemyHealth>().hitPoints.ToString();
        }
        else
        {
            spawner1Text.text = "Spawner2: Destroy";
        }
        if (spawner3 != null)
        {
            spawner1Text.text = "Spawner3 Health: " + spawner3.GetComponent<EnemyHealth>().hitPoints.ToString();
        }
        else
        {
            spawner1Text.text = "Spawner3: Destroy";
        }
    }
}

﻿//Fernando Tapia

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

//class for radar objects and how the appear
public class RadarObject{
	public Image icon { get; set; }
	public GameObject owner { get; set; }
	public string type { get; set;}
	public bool vip { get; set;}
}

public class RadarDetection : MonoBehaviour {

    public PoweredComponent poweredcomponent;

	//origin
	public Transform playerPos;
	float mapScale = 1.0f;
	public int pwrLvl = 1;

	//list to hold radar objects
	public static List<RadarObject> radObjects = new List<RadarObject> ();

	//how the object images get added to list
	public static void RegisterRadarObject(GameObject o, Image i, string t, bool b){
		Image image = Instantiate(i);
		radObjects.Add(new RadarObject(){owner = o, icon = image, type = t, vip = b});
	}

	// to handle when the enemy dies and removes it from radar list
	public static void RemoveRadarObject(GameObject o){
		List<RadarObject> newList = new List<RadarObject> ();
		for (int i = 0; i < radObjects.Count; i++) {
			if (radObjects [i].owner == o) {
				Destroy (radObjects [i].icon);
				continue;
			} else
				newList.Add (radObjects [i]);
		}
		radObjects.RemoveRange (0, radObjects.Count);
		radObjects.AddRange (newList);
	}

	//math to handle drawing the objects on the radar in a circular fashion 
	void DrawRadarDots(){
		foreach (RadarObject ro in radObjects) {
			Vector3 radarPos = (ro.owner.transform.position - playerPos.position);
			float distToObject = Vector3.Distance (playerPos.position, ro.owner.transform.position) * mapScale;
			float deltay = Mathf.Atan2 (radarPos.x, radarPos.z) * Mathf.Rad2Deg - 270 - playerPos.eulerAngles.y;
			radarPos.x = distToObject * Mathf.Cos (deltay * Mathf.Deg2Rad) * -1;
			radarPos.z = distToObject * Mathf.Sin (deltay * Mathf.Deg2Rad);

			ro.icon.transform.SetParent (this.transform);
			ro.icon.transform.position = new Vector3 (radarPos.x/75, radarPos.z/75, 0) + this.transform.position;

			//changes color at each power level
			if (poweredcomponent.power == 0) {
				ro.icon.color = Color.clear;
			}
			else if(poweredcomponent.heat >=95){
				ro.icon.color = Color.red;
			}
			else if (poweredcomponent.power != 0 && String.Equals (ro.type, "grunt"))
				ro.icon.color = Color.white;
			else if ((poweredcomponent.power == 2 || poweredcomponent.power == 3) && String.Equals (ro.type, "COmagenta"))
				ro.icon.color = Color.white;
			else if ((poweredcomponent.power == 2 || poweredcomponent.power == 3) && String.Equals (ro.type, "COcyan"))
				ro.icon.color = Color.white;
			else if ((poweredcomponent.power == 2 || poweredcomponent.power == 3) && String.Equals (ro.type, "COyellow"))
				ro.icon.color = Color.white;
			else if ((poweredcomponent.power == 2 || poweredcomponent.power == 3) && String.Equals (ro.type, "elite"))
				ro.icon.color = Color.white;
			else if (poweredcomponent.power == 1 && (String.Equals (ro.type, "hacker") || String.Equals (ro.type, "elite") || String.Equals (ro.type, "COyellow") || String.Equals (ro.type, "COcyan") || String.Equals (ro.type, "COmagenta")))
				ro.icon.color = Color.clear;
			else if (poweredcomponent.power == 2 && String.Equals (ro.type, "hacker"))
				ro.icon.color = Color.clear;
			else if (poweredcomponent.power == 3 && String.Equals (ro.type, "hacker"))
				ro.icon.color = Color.white;

			
			
			

		}
	}

	void FixedUpdate(){
		DrawRadarDots ();


	}
}

﻿//Fernando Tapia

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//attach this script to enemies in order for them to appear on radar
//types for enemy are: hacker, grunt, COyellow, COcyan, COpink, freeze, blade

public class MakeRadarObject : MonoBehaviour {

	public Image image;
	public string type;
	public bool vip;

	// Use this for initialization
	void Start () {
		if (this.gameObject.tag == "VIPEnemy")
			vip = true;
		else
			vip = false;
		
		RadarDetection.RegisterRadarObject (this.gameObject, image, type, vip);
	}
	
	// Update is called once per frame
	void Update () {
	}

	void OnDestroy(){
		RadarDetection.RemoveRadarObject (this.gameObject);
	}
}

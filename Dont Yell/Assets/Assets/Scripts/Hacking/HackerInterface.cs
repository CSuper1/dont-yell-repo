﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
 
public class HackerInterface : MonoBehaviour {

	private int[] solution = new int[3];
	private int[] badSet = new int[3];

	private int[] leftStick = { 1, 2, 3, 4, 5, 6, 7, 8 };
	private int[] rightStick = { 10, 11, 12, 13, 14, 15, 16, 17 };
	private int[] controlPositions = { 19, 20 };
	private int[] controlRotations = { 18, 21, 22, 23, 24 };

	private HackerFunctionLib library;

	
	private Transform left_transform;
	private Transform right_transform;

	private void Start()
	{
		library = GameObject.Find("HackerPanel").GetComponent<HackerFunctionLib>();
		left_transform = GameObject.Find("lefthand_base").transform;
		right_transform = GameObject.Find("righthand_base").transform;
		Debug.Assert(left_transform != null);
		Debug.Assert(right_transform != null);
	}
 
	// This is what runs if we are currently being hacked and the button to solve our state is pushed.
	public bool checkInState(int[] set)
	{
		if (set.Length != 3)
			throw new System.Exception("checkInState will only take int[] of length 3");

		bool[] nailedIt = { false, false, false };
		
		//Quaternion LRot = GetCtrlRot(OVRInput.Controller.LTouch);
		//Quaternion RRot = GetCtrlRot(OVRInput.Controller.RTouch);

		//Vector3 LPos = GetCtrlPos(OVRInput.Controller.LTouch);
		//Vector3 RPos = GetCtrlPos(OVRInput.Controller.RTouch);

		Vector2 LStick = GetLStickPos();
		Vector2 RStick = GetRStickPos();

		int counter = 0;
		foreach(int i in set){
			switch (i)
			{
				
				case 0:
					nailedIt[counter] = library.LSC();
					break;
				case 1:
					nailedIt[counter] = library.LSL(LStick);
					break;
				case 2:
					nailedIt[counter] = library.LSU(LStick);
					break;
				case 3:
					nailedIt[counter] = library.LSR(LStick);
					break;
				case 4:
					nailedIt[counter] = library.LSD(LStick);
					break;
				case 5:
					nailedIt[counter] = library.LSDL(LStick);
					break;
				case 6:
					nailedIt[counter] = library.LSDR(LStick);
					break;
				case 7:
					nailedIt[counter] = library.LSUR(LStick);
					break;
				case 8:
					nailedIt[counter] = library.LSUL(LStick);
					break;
				case 9:
					nailedIt[counter] = library.RSC();
					break;
				case 10:
					nailedIt[counter] = library.RSL(RStick);
					break;
				case 11:
					nailedIt[counter] = library.RSU(RStick);
					break;
				case 12:
					nailedIt[counter] = library.RSR(RStick);
					break;
				case 13:
					nailedIt[counter] = library.RSD(RStick);
					break;
				case 14:
					nailedIt[counter] = library.RSDL(RStick);
					break;
				case 15:
					nailedIt[counter] = library.RSDR(RStick);
					break;
				case 16:
					nailedIt[counter] = library.RSUR(RStick);
					break;
				case 17:
					nailedIt[counter] = library.RSUL(RStick);
					break;
				case 18:
					nailedIt[counter] = library.CC(left_transform, right_transform);
					break;
				case 19:
					nailedIt[counter] = library.RCOLC(left_transform, right_transform);
					break;
				case 20:
					nailedIt[counter] = library.LCORC(left_transform, right_transform);
					break;
				case 21:
					nailedIt[counter] = library.CFT(left_transform, right_transform);
					break;
				case 22:
					nailedIt[counter] = library.CFA(left_transform, right_transform);
					break;
				case 23:
					nailedIt[counter] = library.TRDLU(left_transform, right_transform);
					break;
				case 24:
					nailedIt[counter] = library.TLDRU(left_transform, right_transform);
					break;
				default:
					throw new System.Exception("HackerInterface did not resolve checkinState(int set[]) with a switch(i) of '" + i + "'");
				
			}
			counter++;
		}
		//Debug.Log(nailedIt[0] + " " + nailedIt[1] + " " + nailedIt[2]);
		return nailedIt[0] && nailedIt[1] && nailedIt[2];
	}

	public void showBadLights()
	{
		library.turnOnWrongLights(badSet);
	}

	public void showRightLights()
	{
		library.turnOnCorrectLights(solution);
	}

	public void clearLights()
	{
		library.turnOffLights();
	}

	public void generateSolution()
	{
		solution = generateSet();
	}

	public void generateBadSet()
	{
		badSet = generateSet();
	}

	public int[] getBadSet()
	{
		return badSet;
	}

	public int[] getSolution()
	{
		return solution;
	}

	// Reset all of the objects to their 'off' state
    public void resetBools()
    {
        library.resetBooleans();
    }

	// Generate a set for internal use
	private int[] generateSet()
	{
		List<int> builder = new List<int>();
		HashSet<int> exclude = new HashSet<int>();


		for (int i = 0; i < 3; i++)
		{
			// Set our exclusion range
			var range = Enumerable.Range(0, 24).Where(k => !exclude.Contains(k));

			// Rng this shit up
			var index = Random.Range(0, 24 - exclude.Count);
			int number = range.ElementAt(index);

			builder.Add(number);

			// Update our exclude list.
			if (number >= 1 && number <= 8)
				exclude.UnionWith(leftStick);
			else if (number >= 10 && number <= 17)
				exclude.UnionWith(rightStick);
			else if (number == 19 || number == 20)
				exclude.UnionWith(controlPositions);
			// Note: 18 is bundled in here to remove confusion for tilting controllers while they are crossed.
			else if (number == 18 || (number >= 21 && number <= 24))
				exclude.UnionWith(controlRotations);					
			else
				exclude.Add(number);
		}

        return builder.ToArray();
        //int[] temp = { 18, 9, 0 };
       // return temp;

	}

	private Vector2 GetRStickPos()
	{
		return OVRInput.Get(OVRInput.Axis2D.SecondaryThumbstick);
	}

	private Vector2 GetLStickPos()
	{
		return OVRInput.Get(OVRInput.Axis2D.PrimaryThumbstick);
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StickySet : MonoBehaviour {

	private bool allSet;
	private StickyNoteButton buttonScript;
	private StickyGroup myParent;

	private void Start()
	{
		myParent = GetComponentInParent<StickyGroup>();
	}
	public bool statusCheck()
	{
		allSet = true;
		foreach( Transform child in transform)
		{
			buttonScript = child.GetComponent<StickyNoteButton>();
			allSet = buttonScript.getState() && allSet;
		}
		return allSet;
	}

	public void turnButtonsOff()
	{
		foreach(Transform child in transform)
		{
			StickyNoteButton temp = child.GetComponent<StickyNoteButton>();
			if (temp.getState() == true)
				temp.changeState();
		}
	}

	public int getTrueCount()
	{
		int count = 0;
		foreach (Transform child in transform)
		{
			if (child.GetComponent<StickyNoteButton>().getState())
				count++;
		}

		return count;
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WarningLight : MonoBehaviour {

	private Color off = Color.white;
	private Color red = Color.red;
	Image myImage;
	bool switcheroo;

	private void Start()
	{
		
		myImage = gameObject.GetComponent<Image>();
		myImage.color = off;
	}

	public void warningOn()
	{
		InvokeRepeating("changeColor", 0.0f, 0.5f);
	}

	public void warningOff()
	{
		CancelInvoke("changeColor");
		myImage.color = off;
	}

	private void changeColor()
	{
		switcheroo = !switcheroo;
		if (switcheroo)
			myImage.color = red;
		else
			myImage.color = off;
	}
}

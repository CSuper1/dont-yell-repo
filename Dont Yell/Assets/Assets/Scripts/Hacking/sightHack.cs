﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class sightHack : MonoBehaviour {
	public Image UIblock;
	public Image UIblock2;
	public bool hack = true;
	public bool LR = false;
	Image block;
	Image block2;

	// Use this for initialization
	void Start () {
		block = Instantiate (UIblock);
		block.transform.SetParent (this.transform, false);
		block2 = Instantiate (UIblock2);
		block2.transform.SetParent (this.transform, false);

		block.color = Color.clear;
		block2.color = Color.clear;
	}

	// Update is called once per frame
	void FixedUpdate () {
		//if being hacked color in one of the images
		if (hack) {
			if(LR)
				block.color = Color.black;
			else
				block2.color = Color.black;
		}
		//clear after the hack
		else{
			block.color = Color.clear;
			block2.color = Color.clear;
		}
	}
}

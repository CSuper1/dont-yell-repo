﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckStateButton : MonoBehaviour {

	HackerController hc;
		
	private void Start()
	{
		hc = GameObject.Find("HackerPanel").GetComponent<HackerController>();
	}

	public void unhack()
	{
		hc.attemptUnhack();
	}
}

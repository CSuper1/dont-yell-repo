﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StickyNoteButton : MonoBehaviour
{
	private bool myState;
	public Sprite on, off;
	private SpriteRenderer mySprite;
	private StickyGroup allButtons;

	private void Start()
	{
		mySprite = GetComponent<SpriteRenderer>();
		mySprite.sprite = off;
		allButtons = GetComponentInParent<StickyGroup>();
	}

	public bool getState()
	{
		return myState;
	}

    public void changeState()
    {
		if (!myState && allButtons.areThreeTrue())
			return;

        myState = !myState;
		if (myState)
			mySprite.sprite = on;
		else
			mySprite.sprite = off;
	}


}

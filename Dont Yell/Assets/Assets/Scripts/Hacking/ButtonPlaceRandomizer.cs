﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class ButtonPlaceRandomizer : MonoBehaviour {

	public Transform[] hackerButtons;

	private List<Vector3> buttonLocations = new List<Vector3>();
	// Use this for initialization
	
	public void randomizeLocations()
	{
		foreach (Transform i in hackerButtons)
			buttonLocations.Add(new Vector3(i.position.x, i.position.y, i.position.z));

		HashSet<int> exclude = new HashSet<int>();

		for(int i = 0; i < hackerButtons.Length; i++)
		{
			
			var range = Enumerable.Range(0, hackerButtons.Length).Where(k => !exclude.Contains(k));
			int myButton = range.ElementAt(Random.Range(0, hackerButtons.Length - exclude.Count));
			exclude.Add(myButton);
			hackerButtons[myButton].position = buttonLocations.ElementAt(i);

		}

		buttonLocations.Clear();
	}

}

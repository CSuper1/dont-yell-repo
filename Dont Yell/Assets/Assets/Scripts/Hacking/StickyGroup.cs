﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StickyGroup : MonoBehaviour
{

	private StickySet stickyset;
	public bool isOneTrue()
	{

		foreach (Transform child in transform)
		{
			stickyset = child.GetComponent<StickySet>();
			if (stickyset.statusCheck())
				return true;
		}
		return false;
	}

	public bool areMultipleTrue()
	{
		int count = 0;
		foreach (Transform child in transform)
		{
			if (child.GetComponent<StickySet>().statusCheck())
				count++;
		}

		if (count >= 2)
			return true;

		return false;
	}

	public bool areAllTrue()
	{
		foreach (Transform child in transform)
		{
			stickyset = child.GetComponent<StickySet>();
			if (!stickyset.statusCheck())
				return false;
		}

		return true;
	}

	public void turnButtonsOff()
	{
		foreach(Transform child in transform)
		{
			child.GetComponent<StickySet>().turnButtonsOff();
		}
	}

	public bool areThreeTrue()
	{
		int count = 0;
		foreach(Transform child in transform)
		{
			count += child.GetComponent<StickySet>().getTrueCount();
		}

		return (count < 3) ? false: true;
	}
}

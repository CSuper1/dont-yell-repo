﻿// Cory Super
// 1/16/2017
// This is the main controller for the hacker Script.


using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HackerController : MonoBehaviour
{
	public GameObject player;
	public sightHack sightHack;
	public PoweredComponent[] hackableComponents;

	public StickyGroup allStickyNotes;
	private float gracePeriod = 0;
	public GameObject warningLight;

	private bool hacked, endEarly;
	private bool isSightHacked = false;
	private bool lowHeatHack, highHeathHack, isHeatHack, isMoveHacked;

	private HackerInterface hackInterface;
	private ButtonPlaceRandomizer randomizeButtons;
	private Control thumbstickHack;
	private StickySet properStickyNote;

	private int heatHackVal;
	private PoweredComponent hackedTarget;

	private bool isTutorial = false;
	private int tutorialProgress = 0;
	private bool correctLightPicked = false;

	private List<StickySet> stickyNotes = new List<StickySet>();


	private const int SIGHTHACK = 0;
	private const int MOVEHACK = 1;
	private const int HEATHACK = 2;

	void Start()
	{
		hackInterface = GameObject.Find("HackerPanel").GetComponent<HackerInterface>();
		randomizeButtons = GameObject.Find("HackerPanel").GetComponent<ButtonPlaceRandomizer>();
		foreach (Transform child in GameObject.Find("StickyNotes").transform)
		{
			StickySet temp = child.GetComponent<StickySet>();
			if (temp != null)
				stickyNotes.Add(child.GetComponent<StickySet>());
		}

		warningLight = GameObject.Find("UnhackingI");
		thumbstickHack = player.GetComponent<Control>();

		if (allStickyNotes == null)
			Debug.Log("wtf mang");

		//StartHack();
	}

	void FixedUpdate()
	{
		/*if (!hacked && Input.GetKeyDown(KeyCode.Q))
        {
            StartHack();
            hacked = true;
        }*/

		// If we're not hacked or our enemy gets killed before our grace period ends, none of the other 
		// shit matters.

		if (!hacked || endEarly)
		{
			hacked = false;
			endEarly = false;
			return;
		}


		if (isHeatHack)
			hackedTarget.user_power = heatHackVal;

		if (properStickyNote.statusCheck() && !allStickyNotes.areMultipleTrue())
		{
			hackInterface.clearLights();
			hackInterface.showRightLights();
			correctLightPicked = true;
			hackInterface.checkInState(hackInterface.getSolution());
		}
		else if (allStickyNotes.isOneTrue())
		{
			hackInterface.clearLights();
			hackInterface.showBadLights();
			correctLightPicked = false;
		}
		else
		{
			hackInterface.clearLights();
			correctLightPicked = false;
		}

		//Debug.Log("The proper set to click is " + properStickyNote.name);
	}

	// Call this to start the hack 
	public void StartHack()
	{
		waitPeriod();
	}

	// Call this to end a hack early. Cases for this would generally only be if the hacker enemy is 
	// killed prior to finishing the hack in the alloted time given.
	public void EndHackEarly()
	{
		endEarly = true;
	}

	// Call this only if you're in the tutorial
	public void thisIsTheTutorial()
	{
		isTutorial = true;
	}

	// Use this to check if we are hacked. Will return true if we are in any state of being hacked.
	public bool areHacked()
	{

		return isHeatHack || isMoveHacked || isSightHacked;
	}

	// For the tutorial to check when we should trigger the next phase of text
	public bool correctLightsShown()
	{
		return correctLightPicked;
	}

	// Call this when you want to unhack the mech.
	// It talks to backend of the hacker setup to check controller rotations and positions
	public void attemptUnhack()
	{
		if (properStickyNote.statusCheck())
		{
			if (hackInterface.checkInState(hackInterface.getSolution()))
			{
				endHack();
				hackInterface.clearLights();
				endWarning();
				correctLightPicked = false;
				// end my specific hack here
				if (isSightHacked)
				{
					sightHack.hack = false;
					isSightHacked = false;

				}
				else if (isMoveHacked)
				{
					thumbstickHack.hacked = false;
					isMoveHacked = false;
				}
				else
				{
					isHeatHack = false;
					hackedTarget = null;
				}
			}
		}
		else
			hackInterface.resetBools();
	}

	private void chooseHack()
	{

		// Sloppy way of adding in a tutorial
		if (isTutorial)
		{
			switch (tutorialProgress)
			{
				case 0:
					isSightHacked = true;
					sightHack.hack = true;
					break;

				case 1:
					isMoveHacked = true;
					thumbstickHack.hacked = true;
					break;

				case 2:
					isHeatHack = true;
					heatHack();
					break;

				default:
					Debug.LogError("Default state should not be achieved in Tutorial of HackerController");
					break;
			}

			tutorialProgress++;

			// We don't need to execute any of the further code in our tutorial, so we return
			return;
		}

		int rnd = Random.Range(0, 100);

		// Do our Sighthack at 50% chance
		if (rnd <= 50)
		{
			isSightHacked = true;
			sightHack.hack = true;
		}

		// Do our component hack at 33% chance
		else
		{
			heatHack();
			isHeatHack = true;
		}

		// Do our movement hack at 33% chance

		//NOTE: This is commented out since the thumbstick hack breaks literally everything else

		/*else
		{
			isMoveHacked = true;
			thumbstickHack.hacked = true;
		}*/
	}

	private void heatHack()
	{
		int rnd = Random.Range(0, 6);
		hackedTarget = hackableComponents[rnd];
		if (hackedTarget.user_power >= 2)
		{
			heatHackVal = 0;
			hackedTarget.user_power = heatHackVal;
		}
		else
		{
			heatHackVal = 3;
			hackedTarget.user_power = heatHackVal;
		}
	}

	private void waitPeriod()
	{
		startWarning();
		hacked = true;

		chooseHack();

		PickStickyNote();
		hackInterface.generateSolution();
		hackInterface.generateBadSet();
		// hack starts here

		// Omit button randization for now
		/*if (!isTutorial)
			randomizeButtons.randomizeLocations();
		*/
	}

	private void endHack()
	{
		hacked = false;
		endEarly = false;
		hackInterface.clearLights();
		hackInterface.resetBools();
		allStickyNotes.turnButtonsOff();
		// reset lights here
	}

	private void PickStickyNote()
	{

		// Lights are associated with specific types of hacks
		if (isSightHacked)
			properStickyNote = stickyNotes.ElementAt(SIGHTHACK);
		else if (isMoveHacked)
			properStickyNote = stickyNotes.ElementAt(MOVEHACK);
		else
			properStickyNote = stickyNotes.ElementAt(HEATHACK);
	}

	private void startWarning()
	{
		warningLight.GetComponent<WarningLight>().warningOn();
	}

	private void endWarning()
	{
		warningLight.GetComponent<WarningLight>().warningOff();
	}
}

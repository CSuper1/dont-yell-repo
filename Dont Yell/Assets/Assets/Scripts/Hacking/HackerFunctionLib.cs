﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HackerFunctionLib : MonoBehaviour {

	private GameObject[] lightList = new GameObject[25];
	public Material right, wrong, hitme, plain;
	private Color green = new Color(57/255f, 255/255f, 20/255f);
	private Color orange = new Color(255/255f, 165/255f, 1/255f);
	[Range(0f, 90f)]
	public float face_threshhold;

	void Start()
	{
		lightList[0] = GameObject.Find("LStickClick");
		
		lightList[1] = GameObject.Find("LStickLeft");
		lightList[2] = GameObject.Find("LStickUp");
		lightList[3] = GameObject.Find("LStickRight");
		lightList[4] = GameObject.Find("LStickDown");
		lightList[5] = GameObject.Find("LStickDownLeft");
		lightList[6] = GameObject.Find("LStickDownRight");
		lightList[7] = GameObject.Find("LStickUpRight");
		lightList[8] = GameObject.Find("LStickUpLeft");

		lightList[9] = GameObject.Find("RStickClick");

		lightList[10] = GameObject.Find("RStickLeft");
		lightList[11] = GameObject.Find("RStickUp");
		lightList[12] = GameObject.Find("RStickRight");
		lightList[13] = GameObject.Find("RStickDown");
		lightList[14] = GameObject.Find("RStickDownLeft");
		lightList[15] = GameObject.Find("RStickDownRight");
		lightList[16] = GameObject.Find("RStickUpRight");
		lightList[17] = GameObject.Find("RStickUpLeft");

		lightList[18] = GameObject.Find("CrossControllers");

		lightList[19] = GameObject.Find("RightOverLeftController");
		lightList[20] = GameObject.Find("LeftOverRightController");


		lightList[21] = GameObject.Find("FaceTowardEachOther");
		lightList[22] = GameObject.Find("FaceAwayEachOther");
		lightList[23] = GameObject.Find("TiltRightDownLeftUp");
		lightList[24] = GameObject.Find("TiltLeftDownRightUp");

		/* These are spaced out for mental grouping.
		* When assigning controller configurations, if one in the group is selected
		* the rest in the block can NOT be selected because it would be impossible to do physically.
		*/
	}

	//															*** Controller Rotation Checks (THIS_IS_AN_ERRORs) ***

	//A, B, must be normalized
	private bool are_vectors_aligned(Vector3 A, Vector3 B)
	{
		float diff = Vector3.Magnitude(A - B);
		float theta = Mathf.Asin(diff / 2);
		theta *= 180 / Mathf.PI;
		return theta < face_threshhold;
	}

	// Tilt Left Down Right Up (Left controller tilted so L thumbstick faces the ground and R faces ceiling)
	public bool TLDRU(Transform L, Transform R)
	{
		Vector3 global_up = new Vector3(0, 1, 0);
		bool check = are_vectors_aligned(-L.up, global_up) && are_vectors_aligned(R.up, global_up);
		if (check)
		{
			lightList[24].GetComponent<SpriteRenderer>().color = green;
			return check;
		}
		lightList[24].GetComponent<SpriteRenderer>().color = orange;
		return check;
	}
	// Tilt Right Down Left Up (Right controller tilted so R thumbstick faces the ground and L faces ceiling)
	public bool TRDLU(Transform L, Transform R)
	{
		Vector3 global_up = new Vector3(0, 1, 0);
		bool check = are_vectors_aligned(L.up, global_up) && are_vectors_aligned(-R.up, global_up);
		if (check)
		{
			lightList[23].GetComponent<SpriteRenderer>().color = green;
			return check;
		}
		lightList[23].GetComponent<SpriteRenderer>().color = orange;
		return check;
	}

	// Controllers Face Away (Controllers turned so their front triggers face away)
	public bool CFA(Transform L, Transform R)
	{
		Vector3 leftTowardsRight = Vector3.Normalize(R.position - L.position);

		bool check = are_vectors_aligned(-L.up, leftTowardsRight) && 
			   are_vectors_aligned(R.up, leftTowardsRight);
		if (check)
		{
			lightList[22].GetComponent<SpriteRenderer>().color = green;
			return check;
		}
		lightList[22].GetComponent<SpriteRenderer>().color = orange;
		return check;
	}

	// Controllers Face Toward (Controllers turned so their front triggers face each other)
	public bool CFT(Transform L, Transform R)
	{
		Vector3 leftTowardsRight = Vector3.Normalize(R.position - L.position);

		bool check = are_vectors_aligned(L.forward, leftTowardsRight) && 
			   are_vectors_aligned(-R.forward, leftTowardsRight);
		if (check)
		{
			lightList[21].GetComponent<SpriteRenderer>().color = green;
			return check;
		}
		lightList[21].GetComponent<SpriteRenderer>().color = orange;
		return check;
	}

	//															*** Controller Position Checks (Vector3) ***

	// Cross Controllers (Controllers are an opposite sides)
	public bool CC(Transform L, Transform R)
	{
		Vector3 leftTowardsRight = Vector3.Normalize(R.position - L.position);
		bool check = leftTowardsRight.x >= -.2;
        Debug.Log(leftTowardsRight.x);
		if (check)
		{
			lightList[18].GetComponent<SpriteRenderer>().color = green;
			return check;
		}
		
		lightList[18].GetComponent<SpriteRenderer>().color = orange;
		return check;
	}

	// Right Controller Over Left Controller
	public bool RCOLC(Transform L, Transform R)
	{

		Vector3 leftTowardsRight = Vector3.Normalize(R.position - L.position);
		bool check = leftTowardsRight.y >= .2;
		if (check)
		{
			lightList[19].GetComponent<SpriteRenderer>().color = green;
			return check;
		}

		lightList[19].GetComponent<SpriteRenderer>().color = orange;
		return check;
	}

	// Left Controller Over Right Controller
	public bool LCORC(Transform L, Transform R)
	{
		Vector3 leftTowardsRight = Vector3.Normalize(R.position - L.position);
		bool check = leftTowardsRight.y <= -.2;
		if (check)
		{
			lightList[20].GetComponent<SpriteRenderer>().color = green;
			return check;
		}

		lightList[20].GetComponent<SpriteRenderer>().color = orange;
		return check;
	}

	//															*** Right Controller Thumbstick Controls ***

	// Right Thumbstick Click
	public bool RSC()
	{
		if (OVRInput.Get(OVRInput.Button.SecondaryThumbstick))
		{
			lightList[9].GetComponent<SpriteRenderer>().color = green;
			return true;
		}

		lightList[9].GetComponent<SpriteRenderer>().color = orange;
		return false;
	}

	// Right Thumbstick Right
	public bool RSR(Vector2 position)
	{
		if (position.x > .8)
		{
			lightList[12].GetComponent<SpriteRenderer>().color = green;
			return true;
		}

		lightList[12].GetComponent<SpriteRenderer>().color = orange;
		return false;
	}

	// Right Thumbstick Up
	public bool RSU(Vector2 position)
	{
		if (position.y > .8)
		{
			lightList[11].GetComponent<SpriteRenderer>().color = green;
			return true;
		}

		lightList[11].GetComponent<SpriteRenderer>().color = orange;
		return false;
	}

	// Right Thumbstick Down
	public bool RSD(Vector2 position)
	{
		if (position.y < -.8)
		{
			lightList[13].GetComponent<SpriteRenderer>().color = green;
			return true;
		}

		lightList[13].GetComponent<SpriteRenderer>().color = orange;
		return false;
	}

	// Right Thumbstick Left
	public bool RSL(Vector2 position)
	{
		if (position.x < -.8)
		{
			lightList[10].GetComponent<SpriteRenderer>().color = green;
			return true;
		}

		lightList[10].GetComponent<SpriteRenderer>().color = orange;
		return false;
	}

	// Right Thumbstick Up Right
	public bool RSUR(Vector2 position)
	{
		if ((position.x > .5 && position.x < .9) &&
			(position.y > .5 && position.y < .9))
		{
			lightList[16].GetComponent<SpriteRenderer>().color = green;
			return true;
		}

		lightList[16].GetComponent<SpriteRenderer>().color = orange;
		return false;
	}

	// Right Thumbstick Up Left
	public bool RSUL(Vector2 position)
	{
		if ((position.x > -.9 && position.x < -.5) &&
			(position.y > .5 && position.y < .9))
		{
			lightList[17].GetComponent<SpriteRenderer>().color = green;
			return true;
		}

		lightList[17].GetComponent<SpriteRenderer>().color = orange;
		return false;
	}

	// Right Thumbstick Down Right
	public bool RSDR(Vector2 position)
	{
		if ((position.x > .5 && position.x < .9) &&
			(position.y > -.9 && position.y < -.5))
		{
			lightList[15].GetComponent<SpriteRenderer>().color = green;
			return true;
		}

		lightList[15].GetComponent<SpriteRenderer>().color = orange;
		return false;
	}

	// Right Thumbstick Down Left
	public bool RSDL(Vector2 position)
	{
		if ((position.x > -.9 && position.x < -.5) &&
			(position.y > -.9 && position.y < -.5))
		{
			lightList[14].GetComponent<SpriteRenderer>().color = green;
			return true;
		}

		lightList[14].GetComponent<SpriteRenderer>().color = orange;
		return false;
	}

	//															*** Left Controller Thumbstick Controls ***

	// Left Thumbstick Click
	public bool LSC()
	{
		if (OVRInput.Get(OVRInput.Button.PrimaryThumbstick))
		{
			lightList[0].GetComponent<SpriteRenderer>().color = green;
			return true;
		}

		lightList[0].GetComponent<SpriteRenderer>().color = orange;
		return false;
	}

	// Left Thumbstick Right
	public bool LSR(Vector2 position)
	{
		if (position.x > .8)
		{
			lightList[3].GetComponent<SpriteRenderer>().color = green;
			return true;
		}

		lightList[3].GetComponent<SpriteRenderer>().color = orange;
		return false;
	}

	// Left Thumbstick Up
	public bool LSU(Vector2 position)
	{
		if (position.y > .8)
		{
			lightList[2].GetComponent<SpriteRenderer>().color = green;
			return true;
		}

		lightList[2].GetComponent<SpriteRenderer>().color = orange;
		return false;
	}

	// Left Thumbstick Down
	public bool LSD(Vector2 position)
	{
		if (position.y < -.8)
		{
			lightList[4].GetComponent<SpriteRenderer>().color = green;
			return true;
		}

		lightList[4].GetComponent<SpriteRenderer>().color = orange;
		return false;
	}

	// Left Thumbstick Left
	public bool LSL(Vector2 position)
	{
		if (position.x < -.8)
		{
			lightList[1].GetComponent<SpriteRenderer>().color = green;
			return true;
		}

		lightList[1].GetComponent<SpriteRenderer>().color = orange;
		return false;
	}

	// Left Thumbstick Up Right
	public bool LSUR(Vector2 position)
	{
		if ((position.x > .5 && position.x < .9) &&
			 (position.y > .5 && position.y < .9))
		{
			lightList[7].GetComponent<SpriteRenderer>().color = green;
			return true;
		}

		lightList[7].GetComponent<SpriteRenderer>().color = orange;
		return false;
	}

	// Left Thumbstick Up Left
	public bool LSUL(Vector2 position)
	{
		if ((position.x > -.9 && position.x < -.5) &&
			 (position.y > .5 && position.y < .9))
		{
			lightList[8].GetComponent<SpriteRenderer>().color = green;
			return true;
		}

		lightList[8].GetComponent<SpriteRenderer>().color = orange;
		return false;
	}

	// Left Thumbstick Down Right
	public bool LSDR(Vector2 position)
	{
		if ((position.x > .5 && position.x < .9) &&
			 (position.y > -.9 && position.y < -.5))
		{
			lightList[6].GetComponent<SpriteRenderer>().color = green;
			return true;
		}

		lightList[6].GetComponent<SpriteRenderer>().color = orange;
		return false;
	}
	// Left Thumbstick Down Left
	public bool LSDL(Vector2 position)
	{
		if ((position.x > -.9 && position.x < -.5) &&
			(position.y > -.9 && position.y < -.5))
		{
			lightList[5].GetComponent<SpriteRenderer>().color = green;
			return true;
		}

		lightList[5].GetComponent<SpriteRenderer>().color = orange;
		return false;
	}

	public void turnOnWrongLights(int[] badSet)
	{
		foreach (int i in badSet)
		{
			lightList[i].GetComponent<SpriteRenderer>().color = orange;
			AnimationSpeed temp = lightList[i].GetComponent<AnimationSpeed>();
			if (temp != null)
				temp.setSpeed(.1f);
		}
	}

	public void turnOnCorrectLights(int[] solution)
	{
		foreach (int i in solution)
		{
			lightList[i].GetComponent<SpriteRenderer>().color = orange;
			AnimationSpeed temp = lightList[i].GetComponent<AnimationSpeed>();
			if (temp != null)
				temp.setSpeed(.1f);
		}
	}

	public void turnOffLights()
	{
		foreach (GameObject i in lightList)
		{
			i.GetComponent<SpriteRenderer>().color = Color.white;
			AnimationSpeed temp = i.GetComponent<AnimationSpeed>();
			if (temp != null)
				temp.setSpeed(0f);
					
		}
	}

    public void resetBooleans()
    {
        foreach (GameObject i in lightList)
            i.GetComponent<SpriteRenderer>().color = Color.white;
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlamePitTrap : MonoBehaviour {

	public float damage, interval, lastDamageTime;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		if(lastDamageTime > interval)
		{
			lastDamageTime = 0;
		}
		lastDamageTime += Time.fixedDeltaTime;
	}

	public void OnCollisionStay(Collision collision)
	{
        if (lastDamageTime > interval)
		{
            if (collision.gameObject.tag == "Enemy")
			{
				collision.gameObject.GetComponent<HealthScript>().takeDamage(10);
			}

			if(collision.gameObject.tag == "Player")
			{
                // Add heat to the player
				collision.gameObject.GetComponent<HealthScript>().takeDamage(10);
			}
		}
	}

    public void OnCollisionEnter(Collision collision)
    {
        Debug.Log(collision.gameObject.name);
    }

}

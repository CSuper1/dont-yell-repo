﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerMovement : MonoBehaviour {

    private float speed = 1f;
    private bool up = true;
    private int timer = 0;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        timer++;
        if (timer < 30)
            transform.Translate(new Vector3(0, speed, 0));
        else if (timer < 60)
            transform.Translate(new Vector3(0, -speed, 0));
        else if (timer == 60)
            timer = 0;
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyHealthBarScript : MonoBehaviour {

    private Image healthBar;
    private Transform player;
    private Transform health;

	// Use this for initialization
	void Start () {
        healthBar = gameObject.transform.GetChild(1).GetChild(1).GetComponent<Image>();
        player = GameObject.Find("Pilot").transform;
        health = gameObject.transform.GetChild(1);
    }
	
	// Update is called once per frame
	void FixedUpdate () {
        healthBar.fillAmount = GetComponent<HealthScript>().hitPoints / 300;
        health.transform.LookAt(player.position);
	}
}

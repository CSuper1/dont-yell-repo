﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class MouseOver : MonoBehaviour
    , IPointerClickHandler
    , IPointerEnterHandler
    , IPointerExitHandler{

    public Camera cam;
    private int timer;
    private bool on = false;
    private GameObject obstacle_1;
    private GameObject enemySpawner;
    private GameObject radar;
    public int type;
    private SupportCamMovement supCam;

    // Use this for initialization
    void Start () {
        obstacle_1 = GameObject.Find("Obstacle_1");
        enemySpawner = GameObject.Find("EnemySpawner");
        radar = GameObject.Find("Radar UI");
        supCam = GameObject.Find("support camera").GetComponent<SupportCamMovement>();
    }
	
	// Update is called once per frame
	void FixedUpdate () {
        timer++;
        if (timer == 80)
            timer = 0;
        if (on)
        {
            GetComponent<Image>().color = new Color32(88, 178, 220, 255);
            timer = 0;
        }

        //level1
        if (obstacle_1 != null && !on && this.name == "MiniGameI")
        {
            if (timer < 40)
            {
                GetComponent<Image>().color = Color.white;
            }
            else
            {
                GetComponent<Image>().color = Color.red;
            }
        }
        //level2
        if (enemySpawner != null && enemySpawner.GetComponent<SceneScript>().level2 && this.name == "headI")
        {
            if (timer < 40)
            {
                GetComponent<Image>().color = Color.white;
            }
            else
            {
                GetComponent<Image>().color = Color.red;
            }
        }
        //level3
        if(enemySpawner != null && enemySpawner.GetComponent<SceneScript>().level3 && this.name == "MiniGameI")
        {
            if (timer < 40)
            {
                GetComponent<Image>().color = Color.white;
            }
            else
            {
                GetComponent<Image>().color = Color.red;
            }
        }
        if (enemySpawner != null && enemySpawner.GetComponent<SceneScript>().level4 && this.name == "CameraI")
        {
            if (timer < 40)
            {
                GetComponent<Image>().color = Color.white;
            }
            else
            {
                GetComponent<Image>().color = Color.red;
            }
        }
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        switch (type)
        {
            case 1:
                supCam.radar = true;
                break;
            case 2:
                supCam.missile = true;
                break;
            case 3:
                supCam.miniGame = true;
                break;
            case 4:
                supCam.weaponChanging = true;
                break;
            case 5:
                supCam.reloading = true;
                break;
            case 6:
                supCam.unhacking = true;
                break;
            case 7:
                supCam.boost = true;
                break;
            case 8:
                supCam.camera = true;
                break;
            case 9:
                supCam.shield = true;
                break;
            case 10:
                radar.transform.GetChild(0).gameObject.SetActive(true);
                radar.transform.GetChild(1).gameObject.SetActive(false);
                radar.transform.GetChild(2).gameObject.SetActive(false);
                break;
            case 11:
                radar.transform.GetChild(0).gameObject.SetActive(false);
                radar.transform.GetChild(1).gameObject.SetActive(true);
                radar.transform.GetChild(2).gameObject.SetActive(true);
                break;
            case 12:
                GameObject.Find("Gun").GetComponent<GeneralShootingScript>().setInput(true);
                break;
            case 13:
                supCam.main = true;
                break;
        }
        /*
        if(type == 1)//if(this.name == "headI")
        {
            //cam.transform.position = new Vector3(0, 75f, 0);
            supCam.radar = true;
        }
        else if(type == 2)//else if(this.name == "MissileI")
        {
            //cam.transform.position = new Vector3(-88f, 75f, 0);
            supCam.missile = true;
        }
        else if(type == 3)//else if(this.name == "MiniGameI")
        {
            //cam.transform.position = new Vector3(88f, 75f, 0);
            supCam.miniGame = true;
        }
        else if(type == 4)//else if (this.name == "WeaponChangingI")
        {
            //cam.transform.position = new Vector3(-88f, -75f, 0);
            supCam.weaponChanging = true;
        }
        else if (type == 5)//else if (this.name == "ReloadingI")
        {
            //cam.transform.position = new Vector3(0, -75f, 0);
            supCam.reloading = true;
        }
        else if (type == 6)//else if (this.name == "UnhackingI")
        {
            //cam.transform.position = new Vector3(88f, -75f, 0);
            supCam.unhacking = true;
        }
        else if (type == 7)//else if (this.name == "BoostI")
        {
            //cam.transform.position = new Vector3(88f, 0, 0);
            supCam.boost = true;
        }
        else if (type == 8)//else if(this.name == "CameraI")
        {
            supCam.camera = true;
        }
        else if (type == 9)//else if(this.name == "chestLI")
        {
            supCam.shield = true;
        }
        else if (type == 10)//else if(this.name == "EnemyButton")
        {
            radar.transform.GetChild(0).gameObject.SetActive(true);
            radar.transform.GetChild(1).gameObject.SetActive(false);
            radar.transform.GetChild(2).gameObject.SetActive(false);
        }
        else if (type == 11)//else if(this.name == "PlatformButton")
        {
            radar.transform.GetChild(0).gameObject.SetActive(false);
            radar.transform.GetChild(1).gameObject.SetActive(true);
            radar.transform.GetChild(2).gameObject.SetActive(true);
        }
        else if (type == 12)//else if(this.name == "ReloadButton")
        {
            GameObject.Find("Gun").GetComponent<GeneralShootingScript>().setInput(true);
        }
        else if (type == 13)//else if(this.name == "ReturnButton")
        {
            //cam.transform.position = new Vector3(0, 0, 0);
            supCam.main = true;
        }
        */
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (type == 3)//if (this.name == "MiniGameI")
            on = true;
        GetComponent<Image>().color = new Color32(88, 178, 220, 255);//Color.blue;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        GetComponent<Image>().color = Color.white;
        if (type == 3)//if (this.name == "MiniGameI")
            on = false;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CalcArmor : MonoBehaviour {

	public Slider armorBar;
	HealthScript core;
	float armor, armorMax;

	// Use this for initialization
	void Start () {
		core = GameObject.Find("Pilot").GetComponent<HealthScript>();
		armorMax = core.armorF;
		armorBar.value = calc();
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		if(this.name == "ArmorFrontUI")
			armor = core.armorF;
		else if(this.name == "ArmorBackUI")
			armor = core.armorB;
		else if(this.name == "ArmorLeftUI")
			armor = core.armorL;
		else if(this.name == "ArmorRightUI")
			armor = core.armorR;
		
		armorBar.value = calc ();
	}

	float calc(){
		return armor / armorMax;
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ReloadText : MonoBehaviour {

    // Use this for initialization
    void Start () {
        
    }
	
	// Update is called once per frame
	void FixedUpdate () {
        /*
        GetComponent<Text>().text = Mathf.Floor(GameObject.Find("ReloadSlider").GetComponent<Slider>().value * 100).ToString() + "%";

        if (GameObject.Find("Gun").GetComponent<GeneralShootingScript>().magazine == 0 && GameObject.Find("ReloadSlider").GetComponent<Slider>().value == 1 && Input.GetMouseButtonUp(0))
        {
            GameObject.Find("ReloadSlider").GetComponent<Slider>().value = 0;
            GameObject.Find("Gun").GetComponent<GeneralShootingScript>().reloadInput = true;
        }
        */

        GameObject.Find("ReloadButton").GetComponent<Button>().onClick.AddListener(reload);
	}

    void reload()
    {
        GameObject.Find("Gun").GetComponent<GeneralShootingScript>().setInput(true);
    }
}

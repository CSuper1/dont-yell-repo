﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponButton : MonoBehaviour
{
	private bool myState;
	public Material on, off;
	private Renderer myColor;

	private void Start()
	{
		myColor = GetComponent<Renderer>();
		myColor.material = off;
	}

	public bool getState()
	{
		return myState;
	}

	public void changeState()
	{
		myState = !myState;
		if (myState)
		{
			myColor.material = on;
			transform.parent.GetComponent<WeaponButtonSet>().increaseCount(transform);
		}

		else
		{
			myColor.material = off;
			gameObject.transform.parent.GetComponent<WeaponButtonSet>().decreaseCount();
		}
	}

}

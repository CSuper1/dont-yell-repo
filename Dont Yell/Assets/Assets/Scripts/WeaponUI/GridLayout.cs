﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.UI;

public class GridLayout : MonoBehaviour
{

	public Camera supportCam;
	public GameObject[] panelLights;
	public GameObject[] orderLights;
	public WeaponChangeScript weaponChanger;
	public Transform changeWeaponButton;
	public string laserColor = "";

	private Transform mySelectedButton;
	public string weaponSelect = "";
	private HashSet<GameObject> playerInput = new HashSet<GameObject>();
	private List<Color> colors = new List<Color>();
	private List<GameObject> currentLights = new List<GameObject>();
	private LineRenderer line;
	private Transform right_transform;
	[Range(0f, 90f)]
	public float face_threshhold = 10f;

    private Color green = new Color(79 / 255f, 204 / 255f, 85 / 255f);
    private Color teal = new Color(30 / 255f, 195 / 255f, 238 / 255f);
    private Color orange = new Color(234 / 255f, 173 / 255f, 49 / 255f);

    // Use this for initialization
    void Start()
	{
		//makeOrder(3);
		line = GetComponent<LineRenderer>();
		changeWeaponButton.GetComponent<Button>().interactable = false;
		right_transform = GameObject.Find("righthand_base").transform;
		foreach(GameObject i in orderLights)
		{
			i.SetActive(false);
		}
	}

	// Update is called once per frame
	void FixedUpdate()
	{

		// If we're using the left click
		if (Input.GetMouseButton(0))
		{
			Ray ray = supportCam.ScreenPointToRay(Input.mousePosition);
			RaycastHit hit;

			if (Physics.Raycast(ray, out hit, 100.0f))
			{
				if (hit.transform.tag == "WeaponChangeUICircle")
				{

					playerInput.Add(hit.transform.gameObject);
					line.numPositions = playerInput.Count;
					GameObject[] objects = playerInput.ToArray<GameObject>();
					List<Vector3> positions = new List<Vector3>();

					foreach (GameObject i in objects)
					{
						positions.Add(i.transform.position);
					}

					line.SetPositions(positions.ToArray<Vector3>());

					// Check if we've got all our lights in order
					if (playerInput.Count == currentLights.Count)
					{
						bool isRight = true;
						for (int i = 0; i < playerInput.Count; i++)
						{
							isRight = (playerInput.ElementAt(i).GetComponent<SpriteRenderer>().color ==
									   orderLights[i].GetComponent<SpriteRenderer>().color);
							if (!isRight)
								break;
						}
						if (isRight)
						{
							//Debug.Log("yay we did it");
							turnOff();
							// Reset the colors to be white for the order of things.
							foreach (GameObject i in orderLights)
								i.GetComponent<SpriteRenderer>().color = Color.white;

							PUSHMEFGT();
							mySelectedButton.GetComponent<Image>().color = Color.white;
						}
					}
				}
			}
		}

		else
		{
			playerInput.Clear();
			line.numPositions = playerInput.Count;
		}

	}

	private void makeOrder(int numberOfSteps)
	{
		if (numberOfSteps <= 0 || numberOfSteps > 6)
		{
			Debug.LogError("NumberOfSteps in GridLayout.makeOrder(int) must be 1 <= numberOfSteps <=6");
			return;
		}
		// placed here just in case the user clicks the same button a bunch, or swaps through weapons
		turnOff();

		HashSet<int> exclude = new HashSet<int>();
		Color[] input = { Color.black, Color.red, green, Color.magenta, Color.yellow, orange };
		colors.AddRange(input);

		for (int i = 0; i < numberOfSteps; i++)
		{
			// for choosing which panel we're getting without hitting it again.
			var range = Enumerable.Range(0, 24).Where(k => !exclude.Contains(k));
			int number = range.ElementAt(Random.Range(0, 24 - exclude.Count));

			exclude.Add(number);

			currentLights.Add(panelLights[number]);
			int random = Random.Range(0, colors.Count);
			currentLights.ElementAt(i).GetComponent<SpriteRenderer>().color = colors.ElementAt(random);
			currentLights.ElementAt(i).SetActive(true);
			colors.RemoveAt(random);
		}

		for (int i = 0; i < numberOfSteps; i++)
		{
			orderLights[i].SetActive(true);
			orderLights[i].GetComponent<SpriteRenderer>().color = currentLights.ElementAt(i).GetComponent<SpriteRenderer>().color;
		}
	}

	private void turnOff()
	{
		foreach (GameObject i in currentLights)
		{
			i.SetActive(false);
		}
		foreach (GameObject i in orderLights)
		{
			i.SetActive(false);
		}
		currentLights.Clear();
	}

	public void chooseWeapon(Transform button)
	{

		if (mySelectedButton != null)
			mySelectedButton.GetComponent<Image>().color = teal;

		weaponSelect = button.name;
		mySelectedButton = button;
		mySelectedButton.GetComponent<Image>().color = Color.green;
		if (button.name == "Laser")
		{
			GameObject.Find("LaserBG").transform.GetChild(1).gameObject.SetActive(true);
			GameObject.Find("LaserBG").transform.GetChild(2).gameObject.SetActive(true);
			GameObject.Find("LaserBG").transform.GetChild(3).gameObject.SetActive(true);
			turnOff();
		}
		else
		{
			makeOrder(6);
		}
	}

	public void chooseLaser(Transform button)
	{
		laserColor = button.name;
		makeOrder(6);
	}

	public void changeWeapon()
	{
		Vector3 globalUp = new Vector3(0, 1, 0);

		if (areVectorsAligned(right_transform.forward, globalUp))
		{
			switch (weaponSelect)
			{
				case "Laser":
					if (laserColor == "Pink")
						weaponChanger.changeWeapon(2);
					else if (laserColor == "Yellow")
						weaponChanger.changeWeapon(3);
					else if (laserColor == "Cyan")
						weaponChanger.changeWeapon(4);

					changeWeaponButton.GetComponent<Button>().interactable = false;
					//Debug.Log(weaponSelect);
					// This is where I call to change the real weapon.
					break;
				case "Missiles":
					weaponChanger.changeWeapon(5);
					changeWeaponButton.GetComponent<Button>().interactable = false;
					//Debug.Log(weaponSelect);
					break;
				case "Bullets":
					weaponChanger.changeWeapon(1);
					changeWeaponButton.GetComponent<Button>().interactable = false;
					//Debug.Log(weaponSelect);
					break;
				case "Sword":
					weaponChanger.changeWeapon(0);
					changeWeaponButton.GetComponent<Button>().interactable = false;
					//Debug.Log(weaponSelect);
					break;
				default:
					Debug.LogError("WeaponCheck switch statement fell through. \"" + weaponSelect + "\"");
					break;
			}
			changeWeaponButton.GetComponent<Image>().color = Color.white;
		}

	}

	// Make the "Change weapon" button flash on and off.
	private void PUSHMEFGT()
	{
		if (changeWeaponButton.GetComponent<Button>().IsInteractable() == false)
			changeWeaponButton.GetComponent<Button>().interactable = true;

		/*switcheroo = !switcheroo;
		if (switcheroo)
			changeWeaponButton.GetComponent<Image>().color = Color.white;
		else
			changeWeaponButton.GetComponent<Image>().color = Color.red;*/
	}

	//A, B, must be normalized
	private bool areVectorsAligned(Vector3 A, Vector3 B)
	{
		float diff = Vector3.Magnitude(A - B);
		float theta = Mathf.Asin(diff / 2);
		theta *= 180 / Mathf.PI;
		return theta < face_threshhold;
	}
}


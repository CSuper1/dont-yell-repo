﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponButtonSet : MonoBehaviour
{
	private bool allSet;
	private WeaponButton buttonScript;
	private int CurrentCount = 0;

	public bool statusCheck()
	{
		allSet = true;
		foreach (Transform child in transform)
		{
			buttonScript = child.GetComponent<WeaponButton>();
			allSet = buttonScript.getState() && allSet;
		}
		return allSet;
	}

	public int getCount()
	{
		return CurrentCount;
	}

	public void increaseCount(Transform button)
	{
		CurrentCount++;
		if(transform.parent.GetComponent<WeaponController>().getCount() > 6)
		{
			button.GetComponent<WeaponButton>().changeState();
		}
		Debug.Assert(CurrentCount <= 6, "increaseCount is greater than 6.");
	}

	public void decreaseCount()
	{
		CurrentCount--;
		Debug.Assert(CurrentCount >= 0, "decreaseCount is less than 0.");
	}
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponController : MonoBehaviour
{

	List<Transform> weapons = new List<Transform>();
	List<WeaponButtonSet> weaponButtons = new List<WeaponButtonSet>();
	WeaponChangeScript weaponChanger;

	private Transform right_transform;
	[Range(0f, 90f)]
	public float face_threshhold = 10f;

	// Use this for initialization
	void Start()
	{
		weaponChanger = GameObject.FindWithTag("Player").GetComponent<WeaponChangeScript>();
		foreach (Transform child in transform)
		{
			weapons.Add(child);
		}

		right_transform = GameObject.Find("righthand_base").transform;

		Debug.Assert(right_transform != null);
		GameObject[] holder = GameObject.FindGameObjectsWithTag("WeaponButtonSet");
		foreach (GameObject i in holder)
		{
			weaponButtons.Add(i.GetComponent<WeaponButtonSet>());
		}
	}

	private void Update()
	{
		//Debug.Log(right_transform.forward);
	}

	public void setWeapon()
	{
		Vector3 globalUp = new Vector3(0, 1, 0);
		if (are_vectors_aligned(right_transform.forward, globalUp))
		{
			foreach (WeaponButtonSet i in weaponButtons)
			{
				if (i.statusCheck())
				{
					weaponCheck(i.transform);
					break;
				}
			}
		}
	}

	private void weaponCheck(Transform weapon)
	{
		switch (weapon.name)
		{
			case "Laser":
				weaponChanger.changeWeapon(2);
				Debug.Log(weapon.name);
				// This is where I call to change the real weapon.
				break;
			case "Missiles":
				weaponChanger.changeWeapon(5);
				Debug.Log(weapon.name);
				break;
			case "Bullets":
				weaponChanger.changeWeapon(1);
				Debug.Log(weapon.name);
				break;
			case "Sword":
				weaponChanger.changeWeapon(0);
				Debug.Log(weapon.name);
				break;
			default:
				Debug.LogError("WeaponCheck switch statement fell through.");
				break;
		}
	}

	public void showCount()
	{
		int count = 0;
		foreach (WeaponButtonSet i in weaponButtons)
		{
			count += i.getCount();
		}
		Debug.Log(count);
	}

	public int getCount()
	{
		int count = 0;
		foreach (WeaponButtonSet i in weaponButtons)
		{
			count += i.getCount();
		}
		return count;
	}

	//A, B, must be normalized
	private bool are_vectors_aligned(Vector3 A, Vector3 B)
	{
		float diff = Vector3.Magnitude(A - B);
		float theta = Mathf.Asin(diff / 2);
		theta *= 180 / Mathf.PI;
		return theta < face_threshhold;
	}

}
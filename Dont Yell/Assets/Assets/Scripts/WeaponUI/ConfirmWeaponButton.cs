﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConfirmWeaponButton : MonoBehaviour {

	WeaponController wc;

	private void Start()
	{
		wc = GameObject.Find("WeaponPanel").GetComponent<WeaponController>();
	}


	public void weaponStateCheck()
	{
		wc.setWeapon();
	}
}

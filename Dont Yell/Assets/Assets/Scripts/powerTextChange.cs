﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class powerTextChange : MonoBehaviour {

	private Text t;
	public PoweredComponent pc;
	// Use this for initialization
	void Start () {
		t = this.gameObject.GetComponent<Text> ();
		t.color = Color.gray;
	}
	
	// Update is called once per frame
	void Update () {
		if(pc.GetComponent<onOffComponent>().onOff == false)
			t.color = Color.gray;
		else
			t.color = Color.green;
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PoweredComponent : MonoBehaviour {
	public int user_power;
	public int power {
		get {
			if (hack_disabled) {
				return 0;
			} else if (hack_powered) {
				return user_power+(int)Mathf.Floor((float)(max_hack_power*(1.0-((float)power_hack_progress)/power_hack_time)));
			} else {
				return user_power;
			}
		}
	}
	public double heat;
	public List<PoweredComponent> adjacent = new List<PoweredComponent>();
	//public List<PoweredComponent> pumptargets = new List<PoweredComponent>(); //no longer necessary

	//hack variables
	public bool hack_disabled = false;
	public bool hack_powered = false;
	public int max_hack_power = 0;
	private int power_hack_progress;
	public int power_hack_time = 1;
	//use this to start a power hack (it needs additional data to initialize)
	public void start_power_hack(int additional_power, int duration_ticks) {
		power_hack_time = duration_ticks;
		power_hack_progress = power_hack_time;
		max_hack_power = additional_power;
		hack_powered = true;
	}


	void Start () {
		//component_name = transform.Find("NameText").gameObject.GetComponent<Text>().text;

		foreach (PoweredComponent pc in adjacent) {
			if (!heatedBy.Contains(pc)) {
				pc.heatedBy.Add(this);
				//GameObject passiveline = new GameObject ("Passive heat transfer line");
				//passiveline.layer = 9;
				//passiveline.transform.parent = transform;
				//LineRenderer line = passiveline.AddComponent<LineRenderer>();
				//line.startColor = line.endColor = Color.red;
				//line.startWidth = line.endWidth = 0.5f;
				//line.numPositions = 2;
				//Vector3[] positions = new Vector3[2];
				//positions [0] = transform.position+Vector3.forward * 0.5f;
				//positions [1] = pc.transform.position+Vector3.forward * 0.5f;
				//line.SetPositions(positions);
			}
		}
	}

	void FixedUpdate() {
		if (hack_powered) {
			if (power_hack_progress > 0)
				power_hack_progress -= 1;
		}
		if(user_power == 1){
			heat += 0.2f;
		}
		else if(user_power == 2){
			heat += 0.5f;
		}
		else if(user_power == 3){
			if (heat < 65)
				heat += 0.8f;
			else
				heat += 1.5f;
		}
	}

	//internal (should only be used by PowerSystem)
	private List<PoweredComponent> heatedBy = new List<PoweredComponent>();
	public void resetHeatedBy() {
		heatedBy.Clear ();
	}
	public void heatNeighbors(float factor) {
		foreach (PoweredComponent pc in adjacent) {
			if (!heatedBy.Contains (pc)) {
				pc.heatedBy.Add (this);
				double difference = this.heat - pc.heat;
				this.heat -= factor * difference;
				pc.heat += factor * difference;
			}
		}
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpdateHeatUI : MonoBehaviour {
	private PoweredComponent my_pc;
	private Text my_txt;

	// Use this for initialization
	void Start () {
		my_pc = transform.parent.gameObject.GetComponent<PoweredComponent>();
		my_txt = GetComponent<Text>();
		Debug.Assert(my_pc != null);
		Debug.Assert(my_txt != null);
	}

	// Update is called once per frame
	void Update () {
		my_txt.text = "HEAT: " + my_pc.heat.ToString("F0");
	}
}

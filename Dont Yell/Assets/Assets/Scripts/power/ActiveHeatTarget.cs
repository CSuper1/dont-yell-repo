﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActiveHeatTarget : MonoBehaviour {
	public PoweredComponent source;
	public PoweredComponent dest;
	public double heat_per_tick = 1f;

	void FixedUpdate() {
		double heat_taken = heat_per_tick;
		source.heat -= heat_taken;
		if (source.heat < 0) {
			heat_taken += source.heat;
			source.heat = 0;
		}
		dest.heat += heat_taken;
	}
}

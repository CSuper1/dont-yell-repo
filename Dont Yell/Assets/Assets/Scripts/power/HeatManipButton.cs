using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class HeatManipButton: Button {
	private GameObject progressline;
	private Vector3 progresstarget;
	private bool inprogress = false;

	private Button my_button;
	private PoweredComponent my_pc;
	private PowerSystem my_ps;

	private Camera support_cam;

	private HeatManipButton target_button = null;

	// Use this for initialization
	override protected void Start () {
		base.Start();
		my_pc = transform.parent.gameObject.GetComponent<PoweredComponent>();
		my_ps = my_pc.transform.parent.gameObject.GetComponent<PowerSystem>();
		my_button = GetComponent<Button>();
		Debug.Assert(my_pc != null);
		Debug.Assert(my_ps != null);
		Debug.Assert(my_button != null);

		support_cam = my_ps.support_cam.GetComponent<Camera>();
	}

	void Update() {
		if (inprogress) {
			if (target_button != null) {
				progresstarget = target_button.transform.position;
			} else {
				progresstarget = getMouseWorld();
			}
			Vector3[] positions = new Vector3[2];
			positions [0] = getRootLinePosition();
			positions [1] = progresstarget+getOffsetTowardsCamera();
			progressline.GetComponent<LineRenderer>().SetPositions(positions);

			if (Input.GetMouseButtonUp(0)) {
				if (target_button != null) {
					ActiveHeatTarget heat = progressline.AddComponent<ActiveHeatTarget>();
					heat.source = my_pc;
					heat.dest = target_button.my_pc;
					progressline = null;
					my_ps.current_heatlines += 1;
				} else {
					Destroy(progressline);
				}
				my_ps.active_button = null;
				target_button = null;
				inprogress = false;
			}
		}
	}

	override public void OnPointerDown(PointerEventData data) {
		if (!inprogress && my_ps.current_heatlines < my_ps.max_heatlines) {
			if (my_ps.active_button != null) {
				Debug.Log("Something fucky happened");
				return;
			}

			inprogress = true;
			my_ps.active_button = this;

			progresstarget = getMouseWorld();

			progressline = new GameObject ("active heat transfer line");
			progressline.transform.parent = transform.parent;

			LineRenderer line = progressline.AddComponent<LineRenderer>();
			line.sortingLayerName = "Lines";
			line.startColor = Color.blue;
			line.endColor = Color.red;
			line.startWidth = 0.04f;
			line.endWidth = line.startWidth;
			line.numPositions = 2;
			Vector3[] positions = new Vector3[2];
			positions [0] = getRootLinePosition();
			positions [1] = progresstarget+getOffsetTowardsCamera();
			line.SetPositions(positions);
			Material whiteDiffuseMat = new Material(Shader.Find("Unlit/Crosshair")); //dunno why crosshair works but color doesnt
			line.material = whiteDiffuseMat;

			progressline.layer = 9;
		}
	}

	override public void OnPointerEnter(PointerEventData data) {
		Debug.Log ("IN!");
		if (my_ps.active_button != null && my_ps.active_button != this) {
			foreach (Transform child in my_ps.active_button.transform) {
				ActiveHeatTarget target = child.gameObject.GetComponent<ActiveHeatTarget>();
				if (target != null) {
					//Debug.Log(target.dest);
					if (target.dest == this.my_pc)
						return;
				}
			}
			foreach (Transform child in transform) {
				ActiveHeatTarget target = child.gameObject.GetComponent<ActiveHeatTarget>();
				if (target != null) {
					//Debug.Log(target.dest);
					if (target.dest == this.my_ps.active_button.my_pc)
						return;
				}
			}
			my_ps.active_button.target_button = this;
		}
	}
	override public void OnPointerExit(PointerEventData data) {
		if (my_ps.active_button != null && my_ps.active_button != this && my_ps.active_button.target_button == this) {
			my_ps.active_button.target_button = null;
		}
	}

	private Vector3 getOffsetTowardsCamera(float distance = 0f) {
		return  distance * Vector3.Normalize(support_cam.transform.position - transform.position);
	}

	private Vector3 getRootLinePosition() {
		return transform.position + getOffsetTowardsCamera();
	}

	private Vector3 getMouseWorld(float distance = 100f) {
		Ray ray = support_cam.ScreenPointToRay (Input.mousePosition);
		return ray.GetPoint(distance);
	}
}

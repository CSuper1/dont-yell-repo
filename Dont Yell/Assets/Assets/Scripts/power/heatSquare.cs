﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class heatSquare : MonoBehaviour {

	public bool h1,h2,h3,h4,h5,h6;
	public Sprite heat_off;
	public Sprite heat_on;
	private Image i;
	public PoweredComponent pc;
	// Use this for initialization
	void Start () {
		pc = transform.parent.parent.gameObject.GetComponent<PoweredComponent>();
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		if (h1 && pc.heat > 15)
			GetComponent<SpriteRenderer>().sprite = heat_on;
		else if(h2 && pc.heat > 34)
			GetComponent<SpriteRenderer>().sprite = heat_on;
		else if(h3 && pc.heat > 51)
			GetComponent<SpriteRenderer>().sprite = heat_on;
		else if(h4 && pc.heat > 69)
			GetComponent<SpriteRenderer>().sprite = heat_on;
		else if(h5 && pc.heat > 85)
			GetComponent<SpriteRenderer>().sprite = heat_on;
		else if(h6 && pc.heat >= 95)
			GetComponent<SpriteRenderer>().sprite = heat_on;
		else
			GetComponent<SpriteRenderer>().sprite = heat_off;
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class PowerSystem : MonoBehaviour {
	public float adjacency_equalization_factor = 0.01f;
	public float natural_heat_loss_factor = 0.01f;

	public GameObject support_cam;
	private Camera sup_cam;

	public double getLastFrameTotalPower() {
		return lastFrameTotalPower;
	}
	//For internal use by HeatManipButton
	public HeatManipButton active_button = null;

	//For internal use by ReactorScript
	public int lastFrameTotalPower = 0;

	//For internal use bu HeatPumpScript
	public int max_heatlines = 2;
	public int current_heatlines = 0;

	private LineRenderer destroyline = null;
	private Vector2 destroy_start;
	public float destroy_camera_distance = 100;

	// Use this for initialization
	void Start () {
		EventSystem[] foo = FindObjectsOfType (typeof(EventSystem)) as EventSystem[];
		if (foo.Length > 1) {
			foreach (EventSystem bar in foo) {
				string path = "";
				Transform baz = bar.transform;
				path = "/" + baz.name + path;
				while (baz.parent != null) {
					baz = baz.parent;
					path = "/" + baz.name + path;
				}
				Debug.Log ("!!!!!!!!!!!!!!");
				Debug.Log (path);
			}
		}
		sup_cam = support_cam.GetComponent<Camera> ();
	}

	//once per frame
	void Update() {
		if (Input.GetMouseButtonDown(1) && !Input.GetMouseButton(0) && destroyline == null) {
			destroy_start = Input.mousePosition;

			destroyline = gameObject.AddComponent<LineRenderer>();
			destroyline.sortingLayerName = "Lines";
			destroyline.startColor = Color.yellow;
			destroyline.endColor = Color.yellow;
			destroyline.startWidth = 0.2f;
			destroyline.endWidth = 0.2f;
			destroyline.numPositions = 2;
			Material whiteDiffuseMat = new Material(Shader.Find("Unlit/Crosshair")); //dunno why crosshair works but color doesnt
			destroyline.material = whiteDiffuseMat;
		}
		if (destroyline != null) {
			Vector3[] positions = new Vector3[2];
			Ray start_ray = sup_cam.ScreenPointToRay (destroy_start);
			Ray ray = sup_cam.ScreenPointToRay (Input.mousePosition);
			positions [0] = start_ray.GetPoint(destroy_camera_distance);
			positions [1] = ray.GetPoint(destroy_camera_distance);
			destroyline.SetPositions(positions);
		}
		if (Input.GetMouseButtonUp(1) && destroyline != null) {
			foreach (Transform childPC in transform) {
				foreach (Transform childLine in childPC) {
					ActiveHeatTarget target = childLine.GetComponent<ActiveHeatTarget>();
					if (target != null) {
						LineRenderer line = target.GetComponent<LineRenderer>();
						Vector3[] positions = new Vector3[line.numPositions];
						line.GetPositions(positions);
						if (AreLineSegmentsCrossing(destroy_start, Input.mousePosition,
							sup_cam.WorldToScreenPoint(positions[0]), sup_cam.WorldToScreenPoint(positions[1]))) {
							Destroy(target.gameObject);
							current_heatlines -= 1;
						}
					}
				}
			}

			Destroy(destroyline);
			destroyline = null;
		}
	}

	// FixedUpdate is called once per tick
	void FixedUpdate () {
		lastFrameTotalPower = 0;

		foreach (Transform childTransform in transform) {
			PoweredComponent component = childTransform.gameObject.GetComponent<PoweredComponent> ();
			Debug.Assert (component != null);
			component.resetHeatedBy ();
		}
		foreach (Transform childTransform in transform) {
			PoweredComponent component = childTransform.gameObject.GetComponent<PoweredComponent> ();
			Debug.Assert (component != null);
			component.heatNeighbors (adjacency_equalization_factor);
		}


		foreach (Transform childTransform in transform) {
			PoweredComponent component = childTransform.gameObject.GetComponent<PoweredComponent> ();
			Debug.Assert (component != null);

			lastFrameTotalPower += component.power;
		}
		foreach (Transform childTransform in transform) {
			PoweredComponent component = childTransform.gameObject.GetComponent<PoweredComponent> ();
			Debug.Assert (component != null);
			component.heat *= 1 - natural_heat_loss_factor;
		}
	}


	//all below from http://wiki.unity3d.com/index.php/3d_Math_functions
	public static bool AreLineSegmentsCrossing(Vector3 pointA1, Vector3 pointA2, Vector3 pointB1, Vector3 pointB2){

		Vector3 closestPointA;
		Vector3 closestPointB;
		int sideA;
		int sideB;

		Vector3 lineVecA = pointA2 - pointA1;
		Vector3 lineVecB = pointB2 - pointB1;

		bool valid = ClosestPointsOnTwoLines(out closestPointA, out closestPointB, pointA1, lineVecA.normalized, pointB1, lineVecB.normalized); 

		//lines are not parallel
		if(valid){

			sideA = PointOnWhichSideOfLineSegment(pointA1, pointA2, closestPointA);
			sideB = PointOnWhichSideOfLineSegment(pointB1, pointB2, closestPointB);

			if((sideA == 0) && (sideB == 0)){

				return true;
			}

			else{

				return false;
			}
		}

		//lines are parallel
		else{

			return false;
		}
	}

	public static bool ClosestPointsOnTwoLines(out Vector3 closestPointLine1, out Vector3 closestPointLine2, Vector3 linePoint1, Vector3 lineVec1, Vector3 linePoint2, Vector3 lineVec2){

		closestPointLine1 = Vector3.zero;
		closestPointLine2 = Vector3.zero;

		float a = Vector3.Dot(lineVec1, lineVec1);
		float b = Vector3.Dot(lineVec1, lineVec2);
		float e = Vector3.Dot(lineVec2, lineVec2);

		float d = a*e - b*b;

		//lines are not parallel
		if(d != 0.0f){

			Vector3 r = linePoint1 - linePoint2;
			float c = Vector3.Dot(lineVec1, r);
			float f = Vector3.Dot(lineVec2, r);

			float s = (b*f - c*e) / d;
			float t = (a*f - c*b) / d;

			closestPointLine1 = linePoint1 + lineVec1 * s;
			closestPointLine2 = linePoint2 + lineVec2 * t;

			return true;
		}

		else{
			return false;
		}
	}	

	public static int PointOnWhichSideOfLineSegment(Vector3 linePoint1, Vector3 linePoint2, Vector3 point){

		Vector3 lineVec = linePoint2 - linePoint1;
		Vector3 pointVec = point - linePoint1;

		float dot = Vector3.Dot(pointVec, lineVec);

		//point is on side of linePoint2, compared to linePoint1
		if (dot > 0) {

			//point is on the line segment
			if (pointVec.magnitude <= lineVec.magnitude) {

				return 0;
			}

			//point is not on the line segment and it is on the side of linePoint2
			else {

				return 2;
			}
		}

		//Point is not on side of linePoint2, compared to linePoint1.
		//Point is not on the line segment and it is on the side of linePoint1.
		else {

			return 1;
		}
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReactorScript : MonoBehaviour {

	private PoweredComponent my_pc;
	private PowerSystem my_ps;

	// Use this for initialization
	void Start () {
		my_pc = gameObject.GetComponent<PoweredComponent>();
		my_ps = transform.parent.GetComponent<PowerSystem> ();
		Debug.Assert (my_pc != null);
		Debug.Assert (my_ps != null);
	}

	void FixedUpdate() {
		my_pc.heat += heatPerPower (my_ps.lastFrameTotalPower);
		if(my_pc.heat >= 100){
			gameOver ();
		}
	}

	private double heatPerPower(int power) {
		//TODO: decide non-linear power-heat ratio
		return power*power*0.1;
	}

	void gameOver(){
		
	}
}

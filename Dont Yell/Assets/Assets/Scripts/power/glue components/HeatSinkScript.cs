﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeatSinkScript : MonoBehaviour {
	private PoweredComponent my_pc;

	public double heat_per_second = 2;

	// Use this for initialization
	void Start () {
		my_pc = gameObject.GetComponent<PoweredComponent>();
		Debug.Assert (my_pc != null);
	}

	// Update is called once per frame
	void Update () {
		my_pc.user_power = 0;
	}

	void FixedUpdate() {
		my_pc.heat -= heat_per_second;
		if (my_pc.heat < 0)
			my_pc.heat = 0;
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CockpitHeat : MonoBehaviour {
	private PoweredComponent my_pc;
	public double heat_begin_threshhold = 10;
	public double heat_lethal_threshhold = 20;
	public double heat_visual = 0;

	// Use this for initialization
	void Start () {
		my_pc = gameObject.GetComponent<PoweredComponent>();
		Debug.Assert (my_pc != null);
	}
	
	// Update is called once per frame
	void Update () {
		my_pc.user_power = 0;
	}

	void FixedUpdate() {
		if (my_pc.heat > heat_lethal_threshhold) {
			//TODO: rip cockpit
			//Debug.Log("u ded");
		} else {
			heat_visual = Mathf.Clamp((float)((my_pc.heat - heat_begin_threshhold) / (heat_lethal_threshhold - heat_begin_threshhold)), 0f, 1f);
		}
	}
}

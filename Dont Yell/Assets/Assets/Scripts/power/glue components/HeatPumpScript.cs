﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeatPumpScript : MonoBehaviour {

	private PoweredComponent my_pc;
	private PowerSystem my_ps;

	// Use this for initialization
	void Start () {
		my_pc = gameObject.GetComponent<PoweredComponent>();
		my_ps = transform.parent.GetComponent<PowerSystem> ();
		Debug.Assert (my_pc != null);
		Debug.Assert (my_ps != null);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void FixedUpdate() {
		my_ps.max_heatlines = 2 + my_pc.power * 2;
	}
}

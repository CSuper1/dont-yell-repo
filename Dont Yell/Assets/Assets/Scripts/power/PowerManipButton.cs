﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class PowerManipButton : MonoBehaviour,
    IPointerClickHandler
{
    private Button my_button;
    private PoweredComponent my_pc;
    private GameObject pwrButton;
    private Text t;
    private Color32 og;
    public int powerAmount = 0;
    public Sprite p0_off;
    public Sprite p0_on;
    public Sprite p1_off;
    public Sprite p1_on;
    public Sprite p2_off;
    public Sprite p2_on;
    public Sprite p3_off;
    public Sprite p3_on;
    private bool init;
    Color32 ol;
    Color32 db;

    // Use this for initialization
    void Start()
    {
        my_pc = transform.parent.gameObject.GetComponent<PoweredComponent>();
        //my_button = GetComponent<Button>();
        pwrButton = this.gameObject;
        //t = my_button.GetComponentInChildren<Text>();
        //og = t.color;
        //ol = new Color32(40, 234, 230, 255);
        //db = new Color32(5, 5, 35, 255);
    }

    void Update()
    {

        if (my_pc.user_power == 0 && powerAmount == 0)
        {
            //pwrButton.GetComponent<Image> ().color = Color.grey;
            //t.color = Color.white;
            GetComponent<SpriteRenderer>().sprite = p0_on;
        }
        else if (my_pc.user_power == 1 && powerAmount == 1)
        {
            //pwrButton.GetComponent<Image>().color = Color.yellow;
            //t.color = Color.white;
			GetComponent<SpriteRenderer>().sprite = p1_on;
        }
        else if (my_pc.user_power == 2 && powerAmount == 2)
        {
            //pwrButton.GetComponent<Image>().color = new Color32(255, 125, 0, 255);
            //t.color = Color.white;
			GetComponent<SpriteRenderer>().sprite = p2_on;
        }
        else if (my_pc.user_power == 3 && powerAmount == 3)
        {
            //pwrButton.GetComponent<Image>().color = Color.red;
            //t.color = Color.white;
			GetComponent<SpriteRenderer>().sprite = p3_on;
        }
        else
        {
			if (powerAmount == 0)
			{
				GetComponent<SpriteRenderer>().sprite = p0_off;
			}
			if (powerAmount == 1)
			{
				GetComponent<SpriteRenderer>().sprite = p1_off;
			}
			if (powerAmount == 2)
			{
				GetComponent<SpriteRenderer>().sprite = p2_off;
			}
			if (powerAmount == 3)
			{
				GetComponent<SpriteRenderer>().sprite = p3_off;
			}
        }

    }

    public void powered()
    {
        my_pc.user_power = powerAmount;
    }

    public void OnPointerClick(PointerEventData eventData)
    {

    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.UI;

public class MissileUIController : MonoBehaviour {

	List<MissileStatus> UItubes = new List<MissileStatus>();
	public bool doorsOpen;
	public float doorTimer;
	public bool doorPressed = false;
    public int reserveMissiles = 64;
	private List<PodFiring> pods = new List<PodFiring>();
	private List<GameObject> targets = new List<GameObject>();

	float wGrow = 0;
	bool growStart = false;

	// Use this for initialization
	void Start () {

		foreach(MissileStatus tube in gameObject.GetComponentsInChildren<MissileStatus>())
		{
			UItubes.Add(tube);
		}

		foreach(GameObject pod in GameObject.FindGameObjectsWithTag("Pod"))
		{
            pods.Add(pod.GetComponent<PodFiring>());

		}
    }

	public void prepareTheKrakens()
	{
		// Kind of redundant but better safe than sorry.
		if (!doorsOpen)
		{
			//Debug.Log("doorsOpen in MissileUIController is " + doorsOpen);
			return;
		}

		//Debug.Log ("I'm in prepare the krakens");

        int missleReadyCount = 0;
		foreach(MissileStatus littleKraken in UItubes)
		{
            if (littleKraken.missileReady)
                missleReadyCount++;
			if (littleKraken.myTarget != null)
				targets.Add(littleKraken.myTarget);
		}
        


		foreach(PodFiring pod in pods)
		{
            pod.setTargets(targets.ToArray());
            pod.setLoaded(missleReadyCount);
		}

		foreach (MissileStatus littleKraken in UItubes)
			littleKraken.releaseTheLittleKraken();


        reserveMissiles -= targets.Count;
        targets.Clear();

		Debug.Log("I fired all the missiles.");
        
	}

	public IEnumerator closeDoors()
	{
		yield return new WaitForSeconds(doorTimer);
		GameObject.Find ("buttonFill").GetComponent<Image> ().fillAmount = 0 / 75;
		wGrow = 0;
		growStart = false;
		doorsOpen = false;
		foreach (PodFiring pod in pods)
			pod.setFire(false);
		//Debug.Log("Doors closed");
	}

	public IEnumerator openDoors()
	{
		yield return new WaitForSeconds(doorTimer);
		GameObject.Find ("buttonFill").GetComponent<Image> ().fillAmount = 0 / 75;
		wGrow = 0;
		growStart = false;
		doorsOpen = true;
		foreach (PodFiring pod in pods)
			pod.setFire(true);
		//Debug.Log("Doors open");
	}

	// Update is called once per frame
	void FixedUpdate () {
        if(pods.Count == 0)
        {
            foreach (GameObject pod in GameObject.FindGameObjectsWithTag("Pod"))
            {

                pods.Add(pod.GetComponent<PodFiring>());

            }
        }


		if(growStart){
			GameObject.Find ("buttonFill").GetComponent<Image> ().fillAmount = wGrow / 75;
			if(wGrow <= 75) {
				wGrow += 1f; 
			}
		}
	}

	public void changeDoors(){
		doorPressed = !doorPressed;
		if (doorPressed) {
			growStart = true;
			StartCoroutine ("closeDoors");
		} else {
			growStart = true;
			StartCoroutine ("openDoors");
		}
	}
}

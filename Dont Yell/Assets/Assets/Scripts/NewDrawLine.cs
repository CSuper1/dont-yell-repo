﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NewDrawLine : MonoBehaviour
{

    //private GameObject line_object;
    //private LineRenderer lr;
    //private Vector3 a;
    //private Vector3 b;
    private Transform canvas;
    //public int start;
    //public int end;
    public int[] starts;
    public int[] ends;
    private LineHandler.line[] p;
    private GameObject[] line_objects;
    private LineRenderer[] lrs;

    // Use this for initialization
    void Start()
    {
        canvas = GameObject.Find("MiniGame").transform;
        line_objects = new GameObject[starts.Length];
        lrs = new LineRenderer[starts.Length];
        p = new LineHandler.line[starts.Length];
        for(int i = 0; i < p.Length; i++)
        {
            line_objects[i] = new GameObject();
            line_objects[i].transform.parent = transform;
            line_objects[i].layer = 5;
            line_objects[i].transform.position = new Vector3(0, 0, 0);
            lrs[i] = line_objects[i].AddComponent<LineRenderer>();
            lrs[i].material = new Material(Shader.Find("Particles/Alpha Blended Premultiply"));
            lrs[i].startColor = Color.blue;
            lrs[i].endColor = Color.blue;
            if (gameObject.name == "Level_6")
            {
                lrs[i].startWidth = 0f;
                lrs[i].endWidth = 0f;
            }
            else
            {
                lrs[i].startWidth = 0.1f;
                lrs[i].endWidth = 0.1f;
            }
            p[i].start_point = new Vector3(canvas.GetChild(starts[i]).transform.position.x, canvas.GetChild(starts[i]).transform.position.y, canvas.GetChild(starts[i]).transform.position.z - 1f);
            p[i].end_point = new Vector3(canvas.GetChild(ends[i]).transform.position.x, canvas.GetChild(ends[i]).transform.position.y, canvas.GetChild(ends[i]).transform.position.z - 1f);
            GameObject.Find("Lines").GetComponent<LineHandler>().line_array.Add(p[i]);
        }
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        for(int i = 0; i < starts.Length; i++)
        {
            lrs[i].SetPosition(0, new Vector3(canvas.GetChild(starts[i]).transform.position.x, canvas.GetChild(starts[i]).transform.position.y, canvas.GetChild(starts[i]).transform.position.z - 1f));
            lrs[i].SetPosition(1, new Vector3(canvas.GetChild(ends[i]).transform.position.x, canvas.GetChild(ends[i]).transform.position.y, canvas.GetChild(ends[i]).transform.position.z - 1f));
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyMove : MonoBehaviour {

    float speed = 25.0f;
    //float rotateSpeed = 3.0f;
    float reachDistance = 1f;
    float vision = 75f; //how wide can enemy see
    float playerDis; //distance between enemy and player
    float dis; //distance between enemy and waypoint
    float randomDis;
    Vector3 waypoint;
    List<Vector3> waypointList = new List<Vector3>();
    NavMeshAgent agent;
    Transform player;
    public bool closeEnough; //whether should shoot
    bool finding; //whether find player

	private PoweredComponent ecm;
	private float basespeed;

    // Use this for initialization
    void Start () {
        agent = GetComponent<NavMeshAgent>();
        waypoint = new Vector3(Random.Range(-25f, 25f), 0, Random.Range(-25f, 25f));
        finding = true;
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
		ecm = player.FindChild("ECM Component").GetComponent<PoweredComponent>();
		basespeed = agent.speed;
        closeEnough = false;
        randomDis = Random.Range(0, 2f);
    }
	
	// use fixed update guys
	void FixedUpdate () {
        //if can see player
        if(!Physics.Linecast(transform.position, player.position, 2) && Vector3.Angle(player.position - transform.position, transform.forward) <= vision)
        {
            waypoint = player.transform.position;
            agent.SetDestination(waypoint);
            playerDis = Vector3.Distance(transform.position, player.position);
            if (playerDis <= 10f - randomDis)
            {
                agent.velocity = Vector3.zero;
                agent.Stop();
                closeEnough = true;
            }

			float ecm_range = 20f; //maybe depend on ecm power?
			if (playerDis <= ecm_range) {
				agent.speed = (1.0f - ecm.power / 4) * basespeed;
			} else {
				agent.speed = basespeed;
			}
        }
        //if can't see player
        else
        {
            finding = true;
            dis = Vector3.Distance(waypoint, transform.position);
            agent.SetDestination(waypoint);
            if (dis < reachDistance)
            {
                waypointList.Add(waypoint);
                while (finding)
                {
                    waypoint = new Vector3(Random.Range(-25f, 25f), 1, Random.Range(-25f, 25f));
                    foreach (Vector3 w in waypointList)
                    {
                        if (Vector3.Distance(waypoint, w) > 5f)
                        {
                            finding = false;
                            break;
                        }
                    }
                }
            }
        }
    }
}

﻿//Fernando Tapia
//Enemy looks at the player and shoots at them
//Remember to add rigid body to player for this to work
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyShoot : MonoBehaviour {

	public Transform bullet;
	public Transform player;
	public Transform origin;
	bool allowfire = true;

    public int enemyMag = 20;
    public float enemyReloadTime = 3.6f;
    private int magCap = 20;

    NavMeshAgent agent;

    void Start(){
        agent = GetComponent<NavMeshAgent>();
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
    }

	void FixedUpdate(){
		float playerDis = Vector3.Distance (this.transform.position, player.position);
		//looks at the player and once in range begins to fire
		//transform.LookAt (player.transform);
		if (playerDis <= 10 && (allowfire)) {
			StartCoroutine(Fire());
		}

        if(enemyMag <= 0)
        {
            StartCoroutine(Reload());
            allowfire = false;
        }

	}

	//rate of fire for the enemy
	IEnumerator Fire(){
		allowfire = false;
		Instantiate(bullet, origin.position + origin.forward, origin.rotation);
        enemyMag--;
		yield return new WaitForSeconds(1);
		allowfire = true;
	}

    //Reload for the enemy
    IEnumerator Reload()
    {
        yield return new WaitForSeconds(enemyReloadTime);
        enemyMag = magCap;
        //allowfire = true;
    }

}

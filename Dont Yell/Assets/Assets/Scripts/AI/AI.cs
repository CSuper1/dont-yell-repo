﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AI : MonoBehaviour {

    public Transform bullet;
    public Transform missile;
    public int area;

    private float enemyReloadTime = 3.6f;
    private bool allowfire = true;
    private int magCap = 20;
    private int enemyMag = 20;
    private int cd = 0;
    private int skillcd = 0;
    private GameObject player;
    private NavMeshAgent agent;
    private Vector3 newWaypoint;
    private int movementType;
    private Vector3 tempV;
    private bool desFound = false;
    private Transform management;
    private AudioSource gun;

    private int bulletNum = 1;

    // Use this for initialization
    void Start () {
        agent = GetComponent<NavMeshAgent>();
        player = GameObject.Find("Pilot");
        newWaypoint = new Vector3(0, 0, 0);
        management = GameObject.Find("BulletManagement").transform;
        gun = GetComponent<AudioSource>();
    }
	
	// Update is called once per frame
	void FixedUpdate () {

        /*
        if (transform.parent.childCount < 6)
        {
            Transform temp = Instantiate(bullet, new Vector3(0, 800, 0), transform.rotation);
            temp.SetParent(transform.parent);
            temp.gameObject.name = "newGlowBolt";
        }
        */

        //ECM
        if(GameObject.Find("TimeShield(Clone)") && Vector3.Distance(GameObject.Find("TimeShield(Clone)").transform.position, transform.position) < 130f)
        {
            agent.speed = 8;
        }
        else
        {
            agent.speed = 80;
        }

        //moving toward player
        if (Vector3.Distance(player.transform.position, transform.position) < 2000f)
        {
            //can't see player
            if (Physics.Linecast(transform.position + transform.up * 25f, player.transform.position, 3))
            {
                //find a new destination
                if (!desFound && area == 1)
                {
                    for (int i = 0; i < 100; i++)
                    {
                        tempV = new Vector3(Random.Range(-700, 320), 401, Random.Range(-1040, -200));
                        if (!Physics.Linecast(tempV, player.transform.position, 3))
                        {
                            agent.SetDestination(tempV);
                            if (agent.hasPath)
                            {
                                desFound = true;
                                break;
                            }
                        }
                    }
                    if (Vector3.Distance(transform.position, agent.destination) < 30f)
                    {
                        desFound = false;
                    }
                }

                agent.angularSpeed = 1000;
                skillcd = 0;
                cd = 0;
            }
            //see player
            else
            {
                desFound = false;
                agent.angularSpeed = 0;
                //stop
                if (cd == 0)
                {
                    agent.SetDestination(transform.position);
                }

                //look at player
                transform.LookAt(player.transform.position);

                //fire
                if (allowfire)
                {
                    StartCoroutine(Fire());
                }
                if (enemyMag <= 0)
                {
                    StartCoroutine(Reload());
                    allowfire = false;
                }

                //shoot missile
                skillcd++;
                if (skillcd % 200 == 0 && skillcd != 0)
                {
                    for (int i = 0; i < 2; i++)
                    {
                        Transform a = Instantiate(missile, transform.position + transform.up * 10, transform.rotation);
                        a.gameObject.GetComponent<EnemyMissileScript>().type = i % 2;

                    }
                }

                //moving behavior
                cd++;
                if (cd % 100 == 0)
                {
                    movementType = Random.Range(0, 3);
                    switch (movementType)
                    {
                        case 0:
                            newWaypoint = transform.position + transform.right * -40;
                            break;
                        case 1:
                            newWaypoint = transform.position + transform.right * 40;
                            break;
                        case 2:
                            newWaypoint = new Vector3(transform.position.x + Random.Range(-20f, 20f), transform.position.y, transform.position.z + Random.Range(-20f, 20f));
                            while (Vector3.Distance(newWaypoint, player.transform.position) < 5f)
                                newWaypoint = new Vector3(transform.position.x + Random.Range(-20f, 20f), transform.position.y, transform.position.z + Random.Range(-20f, 20f));
                            break;
                    }
                    if(Physics.Linecast(newWaypoint + transform.up * 25f, player.transform.position))
                    {
                        cd = 99;
                    }
                    else
                    {
                        agent.SetDestination(newWaypoint);
                    }
                    if (!agent.hasPath)
                    {
                        cd = 99;
                        agent.SetDestination(transform.position);
                    }
                }
            }

        }
	}

    IEnumerator Fire()
    {
        allowfire = false;
        //Instantiate(bullet, transform.position + transform.forward * 30 + transform.up * 10f, transform.rotation);
        transform.parent.GetChild(bulletNum).position = transform.position + transform.forward * 30 + transform.up * 10f;
        transform.parent.GetChild(bulletNum).rotation = transform.rotation;
        //management.GetChild(bulletNum).position = transform.position + transform.forward * 30 + transform.up * 10f;
        //management.GetChild(bulletNum).rotation = transform.rotation;
        bulletNum++;
        if (bulletNum == 6) bulletNum = 1;
        //if (bulletNum == 120) bulletNum = 0;
        gun.Play();
        enemyMag--;
        float shootTime = 1;
        shootTime = 0.7f + Random.Range(-0.3f, 0.3f);
        yield return new WaitForSeconds(shootTime);
        allowfire = true;
    }

    //Reload for the enemy
    IEnumerator Reload()
    {
        yield return new WaitForSeconds(enemyReloadTime);
        enemyMag = magCap;
    }
}

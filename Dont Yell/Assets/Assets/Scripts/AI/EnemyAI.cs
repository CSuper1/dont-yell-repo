﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAI : MonoBehaviour {

    // Use this for initialization
    void Start () {
        
    }
	
	// Update is called once per frame
	void FixedUpdate () {
        EnemyMove moveScript = GetComponent<EnemyMove>();
        EnemyShoot shootScript = GetComponent<EnemyShoot>();

        if (moveScript.closeEnough)
        {
            moveScript.enabled = false;
            shootScript.enabled = true;
        }
        else
        {
            moveScript.enabled = true;
            shootScript.enabled = false;
        }
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.AI;

//difficult adjustment
//adust to playing style
//area search

public class BehaviorTreeClass : MonoBehaviour
{

    //behavior tree structure
    public class Node
    {
        public Node()
        {
            //throw new System.Exception("Node not implemented");
        }

        public virtual bool execute(State s)
        {
            throw new System.Exception("Node execute not implemented");
        }
    }

    public class Composite : Node
    {

        public List<Node> child_nodes;
        public string name;

        public Composite()
        {
            child_nodes = null;
            name = null;
        }

        public Composite(List<Node> child_nodes, string name)
        {
            this.child_nodes = child_nodes;
            this.name = name;
        }

        public override bool execute(State s)
        {
            throw new System.Exception("Composite execute not implemented");
        }

        public string __str__()
        {
            if (name != null)
                return name;
            else
                return "";
        }
    }

    public class Selector : Composite
    {
        public Selector(List<Node> child_nodes, string name) : base(child_nodes, name) { }

        public override bool execute(State s)
        {
            foreach (Node child_node in child_nodes)
            {
                bool success = child_node.execute(s);
                if (success)
                    return true;
            }
            return false;
        }
    }

    public class Sequence : Composite
    {
        public Sequence(List<Node> child_nodes, string name) : base(child_nodes, name) { }

        public override bool execute(State s)
        {
            foreach (Node child_node in child_nodes)
            {
                bool continue_execution = child_node.execute(s);
                if (!continue_execution)
                    return false;
            }
            return true;
        }
    }

    public class Check : Node
    {

        public Func<State, bool> check_function;

        public Check(Func<State, bool> check_function)
        {
            this.check_function = check_function;
        }

        public override bool execute(State s)
        {
            return check_function.Invoke(s);
        }

        public string __str__()
        {
            return check_function.ToString();
        }
    }

    public class Action : Node
    {
        public Func<State, bool> action_function;

        public Action(Func<State, bool> action_function)
        {
            this.action_function = action_function;
        }

        public override bool execute(State s)
        {
            return action_function.Invoke(s);
        }

        public string __str__()
        {
            return action_function.ToString();
        }
    }

    const float still = 1;
    const float moving = 2;

    public class State
    {
        public GameObject thisEnemy;
        public List<Vector3> waypoints;
        public Vector3 waypoint;
        public bool reach;
        public Transform player;
        public NavMeshAgent agent;
        public float behaviorType;
        public float randomDis;
        public GameObject[] waypointObjects;
        public int cd = 0;
        public Vector3 pos;
        public GameObject elite;
        public float shootingRange;
        public int skillcd = 0;

        public State(GameObject t)
        {
            shootingRange = UnityEngine.Random.Range(-5f, 5f);
            thisEnemy = t;
            if (thisEnemy.tag == "Grunt")
            {
                pos = new Vector3(UnityEngine.Random.Range(-5f, 5f), 0, UnityEngine.Random.Range(-5f, 5f));
                GameObject[] elites = GameObject.FindGameObjectsWithTag("Elite");
                foreach(GameObject e in elites)
                {
                    if (e.GetComponent<BehaviorTreeClass>().TeamNum == thisEnemy.GetComponent<BehaviorTreeClass>().TeamNum)
                        elite = e;
                }
            }
            agent = thisEnemy.GetComponent<NavMeshAgent>();
            waypoints = new List<Vector3>();
            waypointObjects = GameObject.FindGameObjectsWithTag("Waypoint");
            foreach (GameObject w in waypointObjects)
            {
                waypoints.Add(w.transform.position);
            }
            player = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
            reach = false;
            behaviorType = Mathf.Ceil(UnityEngine.Random.Range(0, 2f));
            randomDis = UnityEngine.Random.Range(-2f, 2f);
            waypoint = waypoints[UnityEngine.Random.Range(0, waypoints.Count)];
            /*
			Vector3 temp = new Vector3(UnityEngine.Random.Range(-200f, 200f), 100f, UnityEngine.Random.Range (-200f, 200f));
			RaycastHit hit;
			while(true){
				if (Physics.Raycast (temp, Vector3.down, out hit, 200f)) {
					waypoint = hit.point;
					break;
				}
				else{
					temp = new Vector3(UnityEngine.Random.Range(-200f, 200f), 100f, UnityEngine.Random.Range (-200f, 200f));
				}
			}
            */
        }

        public void update()
        {
            if (Vector3.Distance(waypoint, thisEnemy.transform.position) < 10f)
            {
                waypoint = waypoints[UnityEngine.Random.Range(0, waypoints.Count)];
            }
            /*
            if (!agent.hasPath)
            {
                waypoint = waypoints[UnityEngine.Random.Range(0, waypoints.Count)];
                //thisEnemy.transform.Translate(thisEnemy.transform.forward);
            }
            */
            /*
            //reach waypoint
            if (Vector3.Distance(waypoint, thisEnemy.transform.position) < 5f)
            {
				RaycastHit hit;
				Vector3 temp = new Vector3(UnityEngine.Random.Range(-200f, 200f), 100f, UnityEngine.Random.Range(-200f, 200f));
                while (true)
                {
                    if (Physics.Raycast(temp, Vector3.down, out hit, 200f))
                    {
                        waypoint = hit.point;
                        break;
                    }
                    else
                    {
                        temp = new Vector3(UnityEngine.Random.Range(-200f, 200f), 100f, UnityEngine.Random.Range(-200f, 200f));
                    }
                }
            }
            */
        }
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////
    //behavior

    //check
    bool check_close_enough(State s)
    {
        float f = 0f;
        if (s.thisEnemy.tag != "Hacker" && s.thisEnemy.tag != "Grunt") f = 100f;
        else if (s.thisEnemy.tag == "Grunt") f = 30f;
        else if (s.thisEnemy.tag == "Hacker") f = 50f;
        float dis = Vector3.Distance(transform.position, s.player.transform.position);
        return dis < f + s.randomDis;
    }

    bool check_reach_waypoint(State s)
    {
        float dis = Vector3.Distance(transform.position, s.waypoint);
        return dis < 0.2;
    }

    bool see_player(State s)
    {
        return (!Physics.Linecast(transform.position, s.player.position, 2));// && Vector3.Angle(s.player.position - transform.position, transform.forward) <= 75f);
    }

    bool no_emeny_ahead(State s)
    {
        Vector3 fwd = transform.TransformDirection(Vector3.forward);
        RaycastHit hit;
        Physics.Raycast(transform.position, fwd, out hit);
        return true;
    }

    bool low_health(State s)
    {
        return s.thisEnemy.GetComponent<HealthScript>().hitPoints < 20;
    }

    bool hit_wall(State s)
    {
        Vector3 fwd = transform.TransformDirection(Vector3.forward);
        bool b = Physics.Raycast(transform.position, fwd, 2);
        if (b) s.thisEnemy.GetComponent<HealthScript>().hitPoints = 21;
        return !b;
    }

    bool hacked(State s)
    {
        //return GameObject.Find("Pilot").GetComponent<Control>().hacked;
        return GameObject.Find("HackerPanel").GetComponent<HackerController>().areHacked();
    }

    bool cool_down(State s)
    {
        return s.cd < coolDown;
    }

    bool cooling_down(State s)
    {
        return s.cd > doingHack && s.cd <= coolDown;
    }

    bool check_hacking(State s)
    {
        return s.cd < doingHack;
    }

    bool check_elite_attacking(State s)
    {
        return !s.elite.GetComponent<BehaviorTreeClass>().searching;
    }

    bool elite_not_exist(State s)
    {
        return s.elite == null;
    }

    bool turret_close_enough(State s)
    {
        return Vector3.Distance(s.thisEnemy.transform.position, s.player.transform.position) < 1500f;
    }

    //action
    bool fire(State s)
    {
        Vector3 fwd = transform.TransformDirection(Vector3.forward);
        /*
        if (Physics.Raycast(transform.position, fwd, 5))
        {
            s.agent.SetDestination(transform.position + Vector3.left);
        }
        */
        //s.agent.velocity = Vector3.zero;
        if (allowfire)
        {
            StartCoroutine(Fire());
        }
        if (enemyMag <= 0)
        {
            StartCoroutine(Reload());
            allowfire = false;
        }
        return true;
    }

    bool search(State s)
    {
        //s.agent.SetDestination(s.waypoint);
        s.agent.SetDestination(new Vector3(s.waypoint.x + UnityEngine.Random.Range(-2f, 2f), s.waypoint.y, s.waypoint.z + UnityEngine.Random.Range(-2f, 2f)));
        searching = true;
        return true;
    }

    bool move_toward(State s)
    {
        s.agent.SetDestination(s.player.position);
        return true;
    }

    bool fire_missile(State s)
    {
        if (s.cd % 100 == 0)
        {
            Transform a = Instantiate(missile, transform.position + transform.up * 35 + transform.forward * 60, transform.rotation);
            a.gameObject.GetComponent<EnemyMissileScript>().type = 3;
        }
        s.cd++;
        return true;
    }

    bool attacking_behavior(State s)
    {
        if (searching)
        {
            s.agent.SetDestination(transform.position);
        }
        searching = false;

        /*
        if(Vector3.Distance(transform.position, s.player.transform.position) <= 200f)
        {
            s.skillcd++;
        }
        */
        s.skillcd++;
        if(s.skillcd % 200 == 0 && s.skillcd != 0)
        {
            for(int i = 0; i < 2; i++) {
                Transform a = Instantiate(missile, transform.position + transform.up * 10, transform.rotation);
                a.gameObject.GetComponent<EnemyMissileScript>().type = i % 2;
            }
        }

        if(Vector3.Distance(transform.position, s.player.transform.position) > 100f)
        {
            s.agent.SetDestination(transform.position + transform.TransformDirection(Vector3.forward));
        }
        else if (s.cd > 200)
        {
            int movementType = UnityEngine.Random.Range(0,3);
            Vector3 newWaypoint = new Vector3(0,0,0);
            Vector3.Distance(transform.position, s.player.transform.position);
            switch (movementType)
            {
                case 0:
                    newWaypoint = transform.position + transform.TransformDirection(Vector3.left * 20);
                    break;
                case 1:
                    newWaypoint = transform.position + transform.TransformDirection(Vector3.right * 20);
                    break;
                case 2:
                    newWaypoint = new Vector3(transform.position.x + UnityEngine.Random.Range(-5f, 5f), transform.position.y, transform.position.z + UnityEngine.Random.Range(-5f, 5f));
                    while (Vector3.Distance(newWaypoint, s.player.transform.position) < 5f)
                        newWaypoint = new Vector3(transform.position.x + UnityEngine.Random.Range(-5f, 5f), transform.position.y, transform.position.z + UnityEngine.Random.Range(-5f, 5f));
                    break;
            }
            s.agent.SetDestination(newWaypoint);
            s.cd = 0;
        }
        else
            s.cd++;
        return true;
    }

    bool look_at_player(State s)
    {
        s.thisEnemy.transform.LookAt(s.player.transform.position);
        s.thisEnemy.transform.Rotate(5f, 0, 0);
        return true;
    }

    bool flee(State s)
    {
        float max = Vector3.Distance(s.player.transform.position, s.waypoints[0]);
        int location = 0;
        float f;
        for (int i = 1; i < s.waypoints.Count; i++)
        {
            f = Vector3.Distance(s.player.transform.position, s.waypoints[i]);
            if (f > max) location = i;
        }
        s.agent.SetDestination(s.waypoints[location] + new Vector3(UnityEngine.Random.Range(-5f, 5f), 0, UnityEngine.Random.Range(-5f, 5f)));
        return true;
    }
    
    bool hacker_flee(State s)
    {
        float max = Vector3.Distance(s.player.transform.position, s.waypoints[0]);
        int location = 0;
        float f;
        for(int i = 1; i < s.waypoints.Count; i++)
        {
            f = Vector3.Distance(s.player.transform.position, s.waypoints[i]);
            if (f > max) location = i;
        }
        s.agent.SetDestination(s.waypoints[location]);
        s.cd++;
        if (s.cd > coolDown && !GameObject.Find("Pilot").GetComponent<Control>().hacked) s.cd = 0;
        return true;
    }

    bool hack(State s)
    {
        /*
        GameObject.Find("Pilot").GetComponent<Control>().hacked = true;
        s.cd = doingHack + 1;
        return true;
        */
        GameObject.Find("HackerPanel").GetComponent<HackerController>().StartHack();
        return true;
    }

    bool hacking(State s)
    {
        s.agent.SetDestination(s.thisEnemy.transform.position);
        s.thisEnemy.transform.Rotate(0, Time.deltaTime * 30, 0, Space.Self);
        s.cd++;
        return true;
    }

    bool follow_elite(State s)
    {
        s.agent.SetDestination(s.elite.transform.position + s.pos);
        return true;
    }

    bool go_to_attack_pos(State s)
    {
        s.agent.SetDestination(new Vector3(s.player.transform.position.x + s.pos.x, transform.position.y, s.player.transform.position.z + s.pos.z));
        return true;
    }

    bool stand(State s)
    {
        s.agent.SetDestination(transform.position);
        return true;
    }

    bool grunt_close(State s)
    {
        if (Vector3.Distance(s.player.transform.position, transform.position) < 10f + s.shootingRange)
            s.agent.SetDestination(transform.position);
        return true;
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////

    public Transform bullet;
    public Transform missile;
    public int TeamNum;
    private int enemyMag = 20;
    private float enemyReloadTime = 3.6f;
    private bool allowfire = true;
    private int magCap = 20;
    private bool searching = true;
    const int doingHack = 400;
    const int coolDown = 2000;

    State s;
    Selector root;
    Sequence attack_seq;
    Selector move_sel;
    Sequence ready_to_attack_seq;
    Sequence aggressive_move_seq;
    Sequence flee_seq;
    Sequence hack_seq;
    Selector hacking_sel;
    Sequence hack_animation_seq;
    Selector check_flee_sel;
    Sequence no_elite_seq;
    Selector attack_or_stand_sel;
    Sequence fire_seq;
    Sequence shoot_seq;
    Check close_enough_node;
    Check reach_waypoint_node;
    Check see_player_node;
    Check low_health_node;
    Check hit_wall_node;
    Check hacked_node;
    Check cool_down_node;
    Check check_hacking_node;
    Check cooling_down_node;
    Check check_elite_attacking_node;
    Check elite_not_exist_node;
    Check turret_close_enough_node;
    Action fire_node;
    Action search_node;
    Action move_toward_node;
    Action attacking_behavior_node;
    Action look_at_player_node;
    Action flee_node;
    Action hacker_flee_node;
    Action hack_node;
    Action hacking_node;
    Action follow_elite_node;
    Action go_to_attack_pos_node;
    Action stand_node;
    Action grunt_close_node;
    Action fire_missile_node;


    // Use this for initialization
    void Start()
    {
        s = new State(gameObject);
        //s.agent.enabled = true;

        close_enough_node = new Check(check_close_enough);
        reach_waypoint_node = new Check(check_reach_waypoint);
        see_player_node = new Check(see_player);
        low_health_node = new Check(low_health);
        hit_wall_node = new Check(hit_wall);
        hacked_node = new Check(hacked);
        cool_down_node = new Check(cool_down);
        check_hacking_node = new Check(check_hacking);
        cooling_down_node = new Check(cooling_down);
        check_elite_attacking_node = new Check(check_elite_attacking);
        elite_not_exist_node = new Check(elite_not_exist);
        turret_close_enough_node = new Check(turret_close_enough);
        fire_node = new Action(fire);
        search_node = new Action(search);
        move_toward_node = new Action(move_toward);
        attacking_behavior_node = new Action(attacking_behavior);
        look_at_player_node = new Action(look_at_player);
        flee_node = new Action(flee);
        hacker_flee_node = new Action(hacker_flee);
        hack_node = new Action(hack);
        hacking_node = new Action(hacking);
        follow_elite_node = new Action(follow_elite);
        go_to_attack_pos_node = new Action(go_to_attack_pos);
        stand_node = new Action(stand);
        grunt_close_node = new Action(grunt_close);
        fire_missile_node = new Action(fire_missile);

        if (s.thisEnemy.tag == "Grunt")
        {
            root = new Selector(null, "Grunt AI");
            attack_seq = new Sequence(null, "Attack Strategy");
            move_sel = new Selector(null, "Move Strategy");
            flee_seq = new Sequence(null, "Feel Strategy");
            no_elite_seq = new Sequence(null, "No Elite Strategy");
            attack_or_stand_sel = new Selector(null, "Attack or Stand Selector");
            fire_seq = new Sequence(null, "Fire Sequence");

            root.child_nodes = new List<Node>();
            root.child_nodes.Add(flee_seq); //Flee Strategy
            root.child_nodes.Add(no_elite_seq); //No Elite Strategy
            root.child_nodes.Add(attack_seq); //Attack Strategy
            root.child_nodes.Add(move_sel); //Move Strategy

            //Flee Strategy
            flee_seq.child_nodes = new List<Node>();
            flee_seq.child_nodes.Add(low_health_node); //Check if low health
            flee_seq.child_nodes.Add(flee_node); //Run away

            //No Elite Strategy
            no_elite_seq.child_nodes = new List<Node>();
            no_elite_seq.child_nodes.Add(elite_not_exist_node);
            no_elite_seq.child_nodes.Add(attack_or_stand_sel);

            //Attack or Stand Strategy
            attack_or_stand_sel.child_nodes = new List<Node>();
            attack_or_stand_sel.child_nodes.Add(fire_seq);
            attack_or_stand_sel.child_nodes.Add(stand_node);

            fire_seq.child_nodes = new List<Node>();
            fire_seq.child_nodes.Add(see_player_node);
            fire_seq.child_nodes.Add(look_at_player_node);
            fire_seq.child_nodes.Add(fire_node);

            //Attack Strategy
            attack_seq.child_nodes = new List<Node>();
            attack_seq.child_nodes.Add(check_elite_attacking_node);
            attack_seq.child_nodes.Add(look_at_player_node);
            attack_seq.child_nodes.Add(move_toward_node);
            attack_seq.child_nodes.Add(grunt_close_node);
            attack_seq.child_nodes.Add(fire_node);

            //Move Strategy
            move_sel.child_nodes = new List<Node>();
            move_sel.child_nodes.Add(follow_elite_node);
        }
        else if(s.thisEnemy.tag == "Hacker")
        {
            root = new Selector(null, "Hacker AI");
            hack_seq = new Sequence(null, "Hack Strategy");
            hacking_sel = new Selector(null, "Hacking Selector");
            move_sel = new Selector(null, "Move Strategy");
            flee_seq = new Sequence(null, "Flee Strategy");
            hack_animation_seq = new Sequence(null, "Hack Animation Sequence");
            check_flee_sel = new Selector(null, "Check Flee Selector");

            root.child_nodes = new List<Node>();
            root.child_nodes.Add(flee_seq); //Flee Strategy
            root.child_nodes.Add(hack_seq); //Attack Strategy
            root.child_nodes.Add(move_sel); //Move Strategy

            //Flee Strategy
            flee_seq.child_nodes = new List<Node>();
            flee_seq.child_nodes.Add(check_flee_sel);
            flee_seq.child_nodes.Add(hacker_flee_node);

            //Check Flee Selector
            check_flee_sel.child_nodes = new List<Node>();
            check_flee_sel.child_nodes.Add(hacked_node);
            check_flee_sel.child_nodes.Add(cooling_down_node);

            //Hack Strategy
            hack_seq.child_nodes = new List<Node>();
            hack_seq.child_nodes.Add(close_enough_node);
            hack_seq.child_nodes.Add(cool_down_node);
            hack_seq.child_nodes.Add(hacking_sel);

            //Hacking Selector
            hacking_sel.child_nodes = new List<Node>();
            hacking_sel.child_nodes.Add(hack_animation_seq);
            hacking_sel.child_nodes.Add(hack_node);

            //Hack Animation Sequence
            hack_animation_seq.child_nodes = new List<Node>();
            hack_animation_seq.child_nodes.Add(check_hacking_node);
            hack_animation_seq.child_nodes.Add(hacking_node);

            //Move Strategy
            move_sel.child_nodes = new List<Node>();
            move_sel.child_nodes.Add(move_toward_node);
        }
        else if(s.thisEnemy.tag == "Turret")
        {
            root = new Selector(null, "Turret AI");
            shoot_seq = new Sequence(null, "Shoot Strategy");

            root.child_nodes = new List<Node>();
            root.child_nodes.Add(shoot_seq); //Shoot Strategy

            shoot_seq.child_nodes = new List<Node>();
            shoot_seq.child_nodes.Add(turret_close_enough_node);
            shoot_seq.child_nodes.Add(look_at_player_node);
            shoot_seq.child_nodes.Add(fire_missile_node);
        }
        else if(s.thisEnemy.tag == "Tank")
        {
            root = new Selector(null, "Tank AI");

            root.child_nodes = new List<Node>();


        }
        else if(s.thisEnemy.tag == "Elite")
        {
            root = new Selector(null, "Grunt AI");
            attack_seq = new Sequence(null, "Attack Strategy");
            ready_to_attack_seq = new Sequence(null, "Ready to Attack");
            move_sel = new Selector(null, "Move Strategy");

            root.child_nodes = new List<Node>();
            root.child_nodes.Add(attack_seq); //Attack Strategy
            root.child_nodes.Add(move_sel); //Move Strategy

            //Attack Strategy
            attack_seq.child_nodes = new List<Node>();
            //attack_seq.child_nodes.Add(see_player_node);
            attack_seq.child_nodes.Add(look_at_player_node);
            attack_seq.child_nodes.Add(fire_node);
            attack_seq.child_nodes.Add(attacking_behavior_node);

            //Move Strategy
            move_sel.child_nodes = new List<Node>();
            move_sel.child_nodes.Add(search_node);
        }
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        s.update();
        root.execute(s);
    }

    IEnumerator Fire()
    {
        allowfire = false;
        if(s.thisEnemy.tag != "Hacker" && s.thisEnemy.tag != "Grunt")
            Instantiate(bullet, transform.position + transform.forward * 30 + transform.up * 20, transform.rotation);
        else if(s.thisEnemy.tag == "Grunt")
            Instantiate(bullet, transform.position + transform.forward * 2, transform.rotation);
        enemyMag--;
        float shootTime = 1;
        if (s.thisEnemy.tag != "Hacker" && s.thisEnemy.tag != "Grunt") shootTime = 0.7f + UnityEngine.Random.Range(-0.3f, 0.3f); ;
        yield return new WaitForSeconds(shootTime);
        allowfire = true;
    }

    //Reload for the enemy
    IEnumerator Reload()
    {
        yield return new WaitForSeconds(enemyReloadTime);
        enemyMag = magCap;
    }
}

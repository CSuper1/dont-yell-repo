﻿// Seth Crawford
//1/28/17
//This generates shields for the player
//It needs in number of marbles, current shields, and what the max shield is.
//This will need to be called by each section (front, back, left, right, buckler)
//If we want to make enemy shields recharge we can use this script provided we give them a fixed
//marble number, and we won't need to worry about their marbles changing.
//3, 7, 11


//edited by Fernando Tapia 2/11/17, 2/16/17
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShieldGenScript : MonoBehaviour
{
	public float shields, shieldMax, shieldBank, shieldBankMax, shieldMin, shieldsAllocated;
	public int marbles = 0;
	public int shieldPower;
	public Slider shieldBar;
	private int timer;
	public List<Image> actMarbles;
	List<Image> a = new List<Image>();
	int posX = -15;
	int active = 0;
	public PoweredComponent pc;


	// Use this for initialization
	void Start()
	{
		shieldMax = 350;
		shieldMin = 50;
		shieldBankMax = 900;
		timer = 0;
		shieldBar.value = CalculateShield();
		/*for (int i = 0; i < 3; i++)
        {
            //Image m = Instantiate(actMarbles[i]);
            a.Add(m);
            m.transform.SetParent(this.transform, false);
            m.transform.localPosition = new Vector3(posX, 19, 0);
            posX += 24;
            //PoweredComponent shieldPower = GameObject.Find("Shields Component").GetComponent<PoweredComponent>();
            //if (shieldPower == null) {
            //	Debug.Log ("Shield power is null. - ShieldGenScript.cs line 40");
            //}
            shieldMax = 350;
            shieldMin = 50;
            shieldBankMax = 900;
            timer = 0;

        }*/
	}

	// Update is called once per frame
	void FixedUpdate()
	{
		/*       if(marbles == 0)
               {
                   active = 0;
                   if(shields > 0) //If shields have no power than it should decrement.
                   {
                       shields -= .09f;
                       if(shields < 0) //If shields went below 0 reset to 0.
                       {
                           shields = 0;
                       }
                   }
                   shieldBar.value = CalculateShield ();
               }*/


		if (this.gameObject.name == "ShieldUIBank")
		{

			if (pc.user_power == 1)
			{

				shieldBank += .03f;
				shieldBar.value = CalculateBank();
				if (shieldBank > shieldBankMax)
				{
					shieldBank = shieldBankMax;
				}
			}
			else if (pc.user_power == 2)
			{

				shieldBank += .09f;
				shieldBar.value = CalculateBank();
				if (shieldBank > shieldBankMax)
				{
					shieldBank = shieldBankMax;
				}
			}
			else if (pc.user_power == 3)
			{

				shieldBank += .21f;
				shieldBar.value = CalculateBank();
				if (shieldBank > shieldBankMax)
				{
					shieldBank = shieldBankMax;
				}
			}
			if (pc.heat >= 95)
			{
				shieldBank -= 1;
				shieldBar.value = CalculateBank();
			}

			if (shieldBank < 0)
			{
				shieldBank = 0;
				shieldBar.value = CalculateBank();
			}
		}
		else
		{

			if (shields > shieldMin)
			{

				shields -= .30f;
				shieldBar.value = CalculateShield();
			}

			if (shields < shieldMin && shieldPower >= 1)
			{
				shields += .01f;
				shieldBar.value = CalculateShield();
			}
			if (shields < 0)
			{
				shields = 0;
				shieldBar.value = CalculateShield();
			}
			if (pc.heat >= 85)
			{
				shields -= 1;
				shieldBar.value = CalculateShield();
			}
		}




		//used to remove marbles from shield bars when power is lowered
		/* if (GameObject.Find("ShieldMarblesUI").GetComponent<ShieldPower>().curActive < 0)
		 {
			 if (string.Equals(gameObject.name, "ShieldUIBuckler") && marbles != 0)
			 {
				 GameObject.Find("ShieldMarblesUI").GetComponent<ShieldPower>().used -= 1;
				 active -= 1;
				 marbles -= 1;
			 }
			 else if (string.Equals(gameObject.name, "ShieldUIRight") && marbles != 0)
			 {
				 GameObject.Find("ShieldMarblesUI").GetComponent<ShieldPower>().used -= 1;
				 active -= 1;
				 marbles -= 1;
			 }
			 else if (string.Equals(gameObject.name, "ShieldUILeft") && marbles != 0 && GameObject.Find("ShieldUIRight").GetComponent<ShieldGenScript>().marbles == 0)
			 {
				 GameObject.Find("ShieldMarblesUI").GetComponent<ShieldPower>().used -= 1;
				 active -= 1;
				 marbles -= 1;
			 }
			 else if (string.Equals(gameObject.name, "ShieldUIBack") && marbles != 0 && GameObject.Find("ShieldUILeft").GetComponent<ShieldGenScript>().marbles == 0)
			 {
				 GameObject.Find("ShieldMarblesUI").GetComponent<ShieldPower>().used -= 1;
				 active -= 1;
				 marbles -= 1;
			 }
		 }
		 */
		//activeMarbles();

	}

	public void shieldButtonPress()
	{
		if (pc.user_power >= 1)
		{

			if (shieldMax - shields < shieldBank)
			{
				shieldsAllocated = shieldMax - shields;
			}
			else
			{
				shieldsAllocated = GameObject.Find("ShieldUIBank").GetComponent<ShieldGenScript>().shieldBank;
			}


			GameObject.Find("ShieldUIBank").GetComponent<ShieldGenScript>().shieldBank -= shieldsAllocated;
			GameObject.Find("ShieldUIBank").GetComponent<ShieldGenScript>().shieldBar.value = CalculateBank();//shieldBar.value = CalculateBank();

			shields += shieldsAllocated;
			shieldBar.value = CalculateShield();


		}


	}

	float CalculateShield()
	{
		if (name == "ShieldUIFront")
		{
			//GameObject.Find("Pilot").GetComponent<HealthScript>().shieldF = shields;
			GameObject.Find("hitFront").GetComponent<PlayerShield>().shield = Mathf.Floor(shields);
		}
		else if (name == "ShieldUIBack")
		{
			//GameObject.Find("Pilot").GetComponent<HealthScript>().shieldB = shields;
			GameObject.Find("hitBack").GetComponent<PlayerShield>().shield = Mathf.Floor(shields);
		}
		else if (name == "ShieldUILeft")
		{
			//GameObject.Find("Pilot").GetComponent<HealthScript>().shieldL = shields;
			GameObject.Find("hitLeft").GetComponent<PlayerShield>().shield = Mathf.Floor(shields);
		}
		else if (name == "ShieldUIRight")
		{
			//GameObject.Find("Pilot").GetComponent<HealthScript>().shieldR = shields;
			GameObject.Find("hitRight").GetComponent<PlayerShield>().shield = Mathf.Floor(shields);
		}
		return shields / shieldMax;
	}
	float CalculateBank()
	{
		return GameObject.Find("ShieldUIBank").GetComponent<ShieldGenScript>().shieldBank / shieldBankMax;
	}
	//draws active marbles above shield bar
	/*
    void activeMarbles()
    {
        for (int i = 0; i < actMarbles.Count; i++)
        {
            if (i < active)
                a[i].color = Color.cyan;
            else
                a[i].color = Color.clear;
        }
    }
    */
}
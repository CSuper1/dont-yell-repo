﻿//Fernando Tapia
//Handles powering up and down of shield bar
//Edited Colin's power mani button script

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;


public class ShieldManiButton : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    private Button my_button;
    private ShieldGenScript shieldUI;
    public int powerAmount;
    private Outline o;
    private Text t;
    public ShieldGenScript sp;
    // Use this for initialization
    void Start()
    {
        my_button = GetComponent<Button>();
        shieldUI = transform.parent.GetComponent<ShieldGenScript>();
        o = GetComponent<Outline>();
        //o.effectColor = Color.black;
        t = GetComponentInChildren<Text>();
        //t.color = Color.clear;
        //sp = GameObject.Find("ShieldMarblesUI").GetComponent<ShieldPower>();
        if (powerAmount == -1)
        {
            my_button.interactable = false;
        }

    }

    void FixedUpdate()
    {
        //Deactives and reactivates buttons when necessary

        if (powerAmount >= 1) // && shieldUI.marbles < 1
        {  //changed shieldUI.marbles from <3
            my_button.interactable = true;
        }
        /*	else if (powerAmount == -1 && shieldUI.marbles > 0) {
                my_button.interactable = true;
            }*/
        else
            my_button.interactable = false;

        /*if (sp.poweredcomponent.power == 0)
        {
            shieldUI.marbles = 0;
        }
        */
        /*if (!GameObject.Find("support camera").GetComponent<CameraRaycast>().expand)
        {
            t.color = Color.clear;
            o.effectColor = Color.black;
        }
        else
        {
            t.color = Color.cyan;
            o.effectColor = Color.cyan;
        }
        */
    }
/*
    public void giveMarble()
    {
        //handles adding marbles to the bar and taking it away from the main stash
        if (GameObject.Find("ShieldMarblesUI").GetComponent<ShieldPower>().curActive > 0)
        {

            shieldUI.marbles += powerAmount;

            if (shieldUI.marbles == 1)
            {
                GameObject.Find("ShieldMarblesUI").GetComponent<ShieldPower>().used += powerAmount;
                my_button.interactable = true;
            }
            else if (shieldUI.marbles == 2)
            {
                GameObject.Find("ShieldMarblesUI").GetComponent<ShieldPower>().used += powerAmount;
            }
            else if (shieldUI.marbles == 3)
            {
                GameObject.Find("ShieldMarblesUI").GetComponent<ShieldPower>().used += powerAmount;
                if (powerAmount == 1)
                    my_button.interactable = false;
                else
                    my_button.interactable = true;
            }
            else if (shieldUI.marbles == 0)
            {
                if (powerAmount == -1)
                    my_button.interactable = false;
                GameObject.Find("ShieldMarblesUI").GetComponent<ShieldPower>().used += powerAmount;
            }
        }
        else if (GameObject.Find("ShieldMarblesUI").GetComponent<ShieldPower>().curActive == 0 && powerAmount == -1)
        {
            shieldUI.marbles += powerAmount;
            GameObject.Find("ShieldMarblesUI").GetComponent<ShieldPower>().used += powerAmount;
        }

        if (shieldUI.marbles < 0)
        {
            shieldUI.marbles = 0;
        }
        if (shieldUI.marbles > 3)
        {
            shieldUI.marbles = 3;
        }
    }
    */
    public void OnPointerEnter(PointerEventData eventData)
    {
        o.effectColor = Color.magenta;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        o.effectColor = Color.cyan;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class textDisappear : MonoBehaviour {
	private Text t;
	private Outline o;

	// Use this for initialization
	void Start () {
		t = GetComponentInChildren<Text> ();
		o = GetComponentInChildren<Outline> ();
		o.effectColor = Color.gray;
		t.color = Color.white;
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		if (GameObject.Find ("support camera").GetComponent<CameraRaycast> ().expand) {
			t.color = Color.clear;
			o.effectColor = Color.clear;	
		} else {
			t.color = Color.white;
			o.effectColor = Color.gray;
		}
	}
}

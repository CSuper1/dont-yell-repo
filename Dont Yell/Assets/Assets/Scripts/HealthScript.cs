﻿// Cory Super
// 1/15/07
// This gives a unit shields, armor, and hitpoints.
// This will only destroy itself when it is in contact with something else.
// Since tags are not currently defined in the project, we have to come back to edit this.
//edited by Fernando Tapia 2/20/17
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthScript : MonoBehaviour {
	public float hitPoints, shields, armorF, armorB, armorL, armorR, shieldF, shieldB, shieldL, shieldR;
	public bool shieldNotif = false, armorNotif = false, coreNotif = false;
	public PoweredComponent coreCom;
	public Light notifLight;
	private Rigidbody rb;
	float tempDam;

    public KillCountScript kills;


	private void Start()
	{
		rb = GetComponent<Rigidbody>();
        kills = GameObject.FindObjectOfType<KillCountScript>();
		//notifLight = GameObject.FindObjectOfType<Light>();
		// notifLight.color = Color.black;
				
	}

	void FixedUpdate(){
        
		//when core component too hot
		//should only work for pilot
		if(coreCom != null){
			if (coreCom.heat >= 85)
				hitPoints -= 0.15f;
		}
		
		//maybe move this to start - a note by fernando to fernando
        /*
		shieldF = GameObject.Find ("ShieldUIFront").GetComponent<ShieldGenScript> ().shields;
		shieldB = GameObject.Find ("ShieldUIBack").GetComponent<ShieldGenScript> ().shields;
		shieldL = GameObject.Find ("ShieldUILeft").GetComponent<ShieldGenScript> ().shields;
		shieldR = GameObject.Find ("ShieldUIRight").GetComponent<ShieldGenScript> ().shields;
        */

        
		if(hitPoints <= 0 && this.name == "Pilot"){
			gameOver();
		}
        
        
	}

	/*private void OnTriggerEnter(Collider other)
	{
		Debug.Log("HealthScript: I collided with another object, but since no weapon values are assigned yet I cannot take damage.");
		if(shields > 0) { }

		else if(armor > 0) { }

		if(hitPoints < 0)
		{
			Destroy(gameObject);
			Debug.LogError("HealthScript: Shields and armor if statements have not been defined yet.");
		}
	}*/

	public void takeDamage(float damage)
	{
		if (shields > 0) {
			shields -= damage;
			// If there's overkill on our shields, migrate damage over to something else.
			if (shields < 0) {
				damage = -shields;
				shields = 0;
			} else
				return;
		}
		else if (hitPoints > 0.0f) {
			hitPoints -= damage;
			coreNotif = true;
			StartCoroutine(damageNotification());
		}

		//check amount of health remaining
		if (hitPoints <= 0.0f)
		{
			ExplosiveEntityData explosion = gameObject.GetComponent<ExplosiveEntityData>();
            if(explosion != null)
            {
                explosion.playBoom();
            }
            if(kills != null)
            {
                kills.increaseKillCount();
            }

			GameObject crowd = GameObject.Find("Crowd");
			if (crowd) {
				CrowdExcitement excitement = crowd.GetComponent<CrowdExcitement>();
				excitement.addKill();
				excitement.addOverkill(-hitPoints);
			}

			Debug.Log("Object '" + gameObject.name + "' should now be dead.");
			if(explosion == null){
				Destroy(gameObject);
			}
			else{
				Destroy(gameObject, 1.35f);
				gameObject.GetComponent<MeshRenderer>().enabled = false;
				gameObject.GetComponent<Collider>().enabled = false;
			}
			enemySpawn.currentSpawn -= 1;

		}
		
	}


	//All these functions below are collider checkers for the pilot to determine which side was hit
	public void hitFront(float damage){
		StartCoroutine (flash ("frontI"));
		if (shieldF > 0) {
			shieldF -= damage;
			shieldNotif = true;
			StartCoroutine(damageNotification());
			if (shieldF < 0) {
				tempDam = shieldF;
				shieldF = 0;
				GameObject.Find ("ShieldUIFront").GetComponent<ShieldGenScript> ().shields = 0;
				if (armorF <= 0) {
					hitPoints += tempDam;
				}else{
					armorF += tempDam;
					if (armorF < 0) {
						tempDam = armorF;
						if(shieldF < 0)
							hitPoints += tempDam;
					}
				}
			}
			GameObject.Find ("ShieldUIFront").GetComponent<ShieldGenScript> ().shields -= damage;
		}
		else if(armorF > 0){
			armorF -= damage;
			armorNotif = true;
			StartCoroutine (damageNotification());
			if (armorF < 0) {
				tempDam = armorF;
				armorF = 0;
				hitPoints += tempDam;
			}
		}
		else if(hitPoints > 0){
			hitPoints -= damage;
			coreNotif = true;
			StartCoroutine(damageNotification());
		}
		else{
			
			Debug.Log("DEAD");
		}
	}

	public void hitBack(float damage){
		StartCoroutine (flash ("backI"));
		if (shieldB > 0) {
			shieldB -= damage;
			shieldNotif = true;
			StartCoroutine(damageNotification());
			if (shieldB < 0) {
				tempDam = shieldB;
				shieldB = 0;
				GameObject.Find ("ShieldUIBack").GetComponent<ShieldGenScript> ().shields = 0;
				if (armorB <= 0) {
					hitPoints += tempDam;
				}else{
					armorB += tempDam;
					if (armorB < 0) {
						tempDam = armorB;
						if(shieldB < 0)
							hitPoints += tempDam;
					}
				}
			}
			GameObject.Find ("ShieldUIBack").GetComponent<ShieldGenScript> ().shields -= damage;
		}
		else if(armorB > 0){
			armorB -= damage;
			armorNotif = true;
			StartCoroutine(damageNotification());
			if (armorB < 0) {
				tempDam = armorB;
				armorB = 0;
				hitPoints += tempDam;
			}

		}
		else if(hitPoints > 0){
			hitPoints -= damage;
			coreNotif = true;
			StartCoroutine(damageNotification());
		}
		else{

			Debug.Log("DEAD");
		}
	}

	public void hitLeft(float damage){
		StartCoroutine (flash ("leftI"));
		if (shieldL > 0) {
			shieldL -= damage;
			shieldNotif = true;
			StartCoroutine(damageNotification());
			if (shieldL < 0) {
				tempDam = shieldL;
				shieldL = 0;
				GameObject.Find ("ShieldUILeft").GetComponent<ShieldGenScript> ().shields = 0;
				if (armorL <= 0) {
					hitPoints += tempDam;
				}else{
					armorL += tempDam;
					if (armorL < 0) {
						tempDam = armorL;
						if(shieldL < 0)
							hitPoints += tempDam;
					}
				}
			}
			GameObject.Find ("ShieldUILeft").GetComponent<ShieldGenScript> ().shields -= damage;
		}
		else if(armorL > 0){
			armorL -= damage;
			armorNotif = true;
			StartCoroutine (damageNotification());
			if (armorL < 0) {
				tempDam = armorL;
				armorL = 0;
				hitPoints += tempDam;
			}
	}
		else if(hitPoints > 0){
			hitPoints -= damage;
			coreNotif = true;
			StartCoroutine(damageNotification());
		}
		else{

			Debug.Log("DEAD");
		}
	}

	public void hitRight(float damage){
		StartCoroutine (flash ("rightI"));
		if (shieldR > 0) {
			shieldR -= damage;
			shieldNotif = true;
			StartCoroutine(damageNotification());
			if (shieldR < 0) {
				tempDam = shieldR;
				shieldR = 0;
				GameObject.Find ("ShieldUIRight").GetComponent<ShieldGenScript> ().shields = 0;
				if (armorR <= 0) {
					hitPoints += tempDam;
				}else{
					armorR += tempDam;
					if (armorR < 0) {
						tempDam = armorR;
						if(shieldR < 0)
							hitPoints += tempDam;
					}
				}
			}
			GameObject.Find ("ShieldUIRight").GetComponent<ShieldGenScript> ().shields -= damage;
		}
		else if(armorR > 0){
			armorR -= damage;
			armorNotif = true;
			StartCoroutine (damageNotification());
			if (armorR < 0) {
				tempDam = armorR;
				armorR = 0;
				hitPoints += tempDam;
			}
		}
		else if(hitPoints > 0){
			hitPoints -= damage;
			coreNotif = true;
			StartCoroutine(damageNotification());
		}
		else{

			Debug.Log("DEAD");
		}
	}

	void gameOver(){
		Application.LoadLevel (Application.loadedLevel);
	}

	IEnumerator flash(string hitbox){
		GameObject.Find (hitbox).gameObject.GetComponent<Image> ().color = Color.red;
		yield return new WaitForSeconds (0.1f);
		GameObject.Find (hitbox).gameObject.GetComponent<Image> ().color = Color.white;
	}

	IEnumerator damageNotification(){
		if (notifLight != null) {
			if (shieldNotif)
				notifLight.color = Color.cyan ;
				// Debug.Log("shield Hit");
			if(armorNotif)
				notifLight.color = Color.yellow;
				// Debug.Log("armor hit");
			if(coreNotif)
				notifLight.color = Color.red;
				// Debug.Log("core hit");
			notifLight.color = Color.black;
		}
		shieldNotif = false;
		armorNotif = false;
		coreNotif = false;
		yield return new WaitForSeconds (1.35f);
	}
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerShieldsScript : MonoBehaviour {

    private int timer;

	// Use this for initialization
	void Start () {
        Physics.IgnoreCollision(GameObject.Find("Pilot").GetComponent<Collider>(), GetComponent<Collider>());
    }
	
	// Update is called once per frame
	void FixedUpdate () {
        timer++;
        if(timer > 200)
        {
            Destroy(this.gameObject);
        }
	}

    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.tag == "Bullet")
        {
            Destroy(col.gameObject);
            Debug.Log(col.gameObject.name);
        }

        if(col.gameObject.tag == "Elite")
        {
            Physics.IgnoreCollision(col.gameObject.GetComponent<Collider>(), GetComponent<Collider>());
        }
    }
}

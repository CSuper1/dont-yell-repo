﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretAI : MonoBehaviour {

    private GameObject player;
    public Transform missile;
    private int cd = 0;

	// Use this for initialization
	void Start () {
        player = GameObject.Find("Pilot");
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		if(Vector3.Distance(transform.position, player.transform.position) < 1500f)
        {
            transform.LookAt(player.transform.position);
            if (cd % 100 == 0)
            {
                Transform a = Instantiate(missile, transform.position + transform.up * 35 + transform.forward * 60, transform.rotation);
                a.gameObject.GetComponent<EnemyMissileScript>().type = 3;
            }
            cd++;
        }
	}
}

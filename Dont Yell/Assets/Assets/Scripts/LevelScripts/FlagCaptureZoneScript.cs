﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FlagCaptureZoneScript : MonoBehaviour {

    private int captures;
    private float radius;
    private GameObject flag;
    private FlagScript script;

    // Use this for initialization
    void Start () {
        //Spawn
        captures = 0;
        radius = 10.0f;
        flag = GameObject.Find("Flag");
        script = flag.GetComponent<FlagScript>();
	}
	
	// Update is called once per frame
	void Update () {
        if(Vector3.Distance(flag.transform.position, transform.position) <= radius)
        {
            Score();
        }
        if(captures >= 3)
        {
            SceneManager.LoadScene(0);
        }
        //Debug.Log(captures);
	}

    //Call this when player has brought flag in
    void Score()
    {
        captures++;
        script.setFollow(false);
        flag.transform.position = new Vector3(0.0f, 1.0f, -20.0f);
    }
}

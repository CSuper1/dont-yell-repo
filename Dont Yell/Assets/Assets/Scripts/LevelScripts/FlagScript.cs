﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlagScript : MonoBehaviour {

    private bool followPlayer = false;
    public GameObject player;
    

    // Use this for initialization
    void Start () {
    //Spawn
        player = GameObject.FindGameObjectWithTag("Player");
	}
	
	// Update is called once per frame
	void Update () {
        if(Vector3.Distance(player.transform.position, transform.position) < 2.0f)
        {
            followPlayer = true;
        }
        if (followPlayer)
        {
            transform.position = GameObject.FindGameObjectWithTag("Player").gameObject.transform.position + new Vector3(0.0f, 3.0f, 0.0f);
        }
	}

    //Call from outside, preferably FlagCaptureZoneScript
    public void setFollow(bool b)
    {
        followPlayer = b;
    }
}

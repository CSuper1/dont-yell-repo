﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KingOfTheHillScript : MonoBehaviour {

    public float time;
    public bool timerSwitch;

    //ticks while player is inside
    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            timerSwitch = true;
        }
    }

    //does not tick if player exits
    private void OnTriggerExit(Collider other)
    {
        if(other.tag == "Player")
        {
            timerSwitch = false;
        }
    }

    // Use this for initialization
    void Start () {
        time = 60.0f;
        timerSwitch = false;
	}
	
	// Update is called once per frame
	void Update () {
        if (timerSwitch)
        {
            //Update time
            time -= Time.deltaTime;
            //Update UI
        }
        //If timer reached 0
        if(time <= 0)
        {
            //Keep it at 0
            time = 0.0f;
            //Call completion
            objectiveCompleted();
        }
	}

    void objectiveCompleted()
    {
        Debug.Log("Completed");
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillCountScript : MonoBehaviour {

    public int kills;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void increaseKillCount()
    {
        kills++;
        Debug.Log("Kills: " + kills);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHealth : MonoBehaviour
{
    public float hitPoints;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
	void FixedUpdate()
    {
        if (hitPoints < 0) GameObject.Destroy(this);
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Bullet")
        {
            hitPoints -= 17;
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Bullet")
        {
            hitPoints -= 17;
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CameraRaycast : MonoBehaviour {

	public Camera myCamera;
	public bool expand;
    private bool b = false;
	// Use this for initialization
	void Start () {
		expand = true;
	}

	// Update is called once per frame
	void FixedUpdate () {

        if (b)
        {
            GameObject.Find("LaserBG").transform.GetChild(1).gameObject.SetActive(false);
            GameObject.Find("LaserBG").transform.GetChild(2).gameObject.SetActive(false);
            GameObject.Find("LaserBG").transform.GetChild(3).gameObject.SetActive(false);
        }
		if (Input.GetMouseButtonDown(0))
		{
			var ray = myCamera.ScreenPointToRay(Input.mousePosition);
			RaycastHit hit;
            if (Physics.Raycast(ray, out hit))
            {
                    b = false;
                    if (hit.transform.gameObject.tag == "HackerButton")
                    {
                        hit.transform.GetComponent<StickyNoteButton>().changeState();
                    }
                    else if (hit.transform.gameObject.tag == "WeaponButton")
                    {
                        hit.transform.GetComponentInParent<GridLayout>().chooseWeapon(hit.transform);
                    }
                    else if (hit.transform.gameObject.tag == "LaserButton")
                    {
                        hit.transform.GetComponentInParent<GridLayout>().chooseLaser(hit.transform);
                        b = true;
                    }
                    else if (hit.transform.gameObject.tag == "WeaponCheck")
                    {
                        hit.transform.GetComponentInParent<GridLayout>().changeWeapon();
                    }
                    else if (hit.transform.gameObject.tag == "HackerCheck")
                    {
                        hit.transform.GetComponent<CheckStateButton>().unhack();
                    }
                    else if (hit.transform.gameObject.tag == "UITube")
                    {
                        hit.transform.GetComponent<MissileStatus>().startLoadingMissile();
                    }
                    else if (hit.transform.gameObject.tag == "LaunchButton")
                    {
                        hit.transform.GetComponent<LaunchButton>().releaseTheBigKraken();
                    }
                    else if (hit.transform.gameObject.tag == "ShieldButton")
                    {
                        hit.transform.parent.GetComponent<ShieldGenScript>().shieldButtonPress();//GetComponent<ShieldGenScript>().shieldButtonPress();
                    }
                    else if (hit.transform.gameObject.tag == "xButton")
                    {
                        //GameObject.Find("supportInterface").GetComponent<uiPopup>().shrink(hit.transform.gameObject.GetComponent<Image>().transform.parent.gameObject.GetComponent<Image>());
                        hit.transform.gameObject.GetComponent<Image>().transform.parent.gameObject.GetComponent<Image>().GetComponent<Outline>().effectColor = Color.black;
                        expand = false;
                    }
                    else if (hit.transform.gameObject.tag == "mMatch")
                    {
                        hit.transform.GetComponentInParent<missileMatch>().guess(hit.transform.gameObject.name);
                    }
                    else if (hit.transform.gameObject.tag == "checkMatch")
                    {
                        hit.transform.GetComponentInParent<missileMatch>().checkGuess();
                    }
                    else if (hit.transform.gameObject.name == "doorButton")
                    {
                        hit.transform.GetComponentInParent<MissileUIController>().changeDoors();
                    }
                    else if (hit.transform.gameObject.tag == "PowerButton")
                    {
                        hit.transform.GetComponent<PowerManipButton>().powered();
                    }
			}
		}
	}
}
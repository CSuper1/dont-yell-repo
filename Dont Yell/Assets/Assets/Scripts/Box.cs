﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Box : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.name == "Pilot")
        {
            col.gameObject.transform.GetChild(2).GetChild(0).GetChild(4).GetComponent<GeneralShootingScript>().magazine++;
            Destroy(this.gameObject);
        }
    }
}

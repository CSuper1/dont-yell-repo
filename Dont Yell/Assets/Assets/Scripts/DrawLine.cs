﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DrawLine : MonoBehaviour {

    private GameObject line_object;
    private LineRenderer lr;
    private Vector3 a;
    private Vector3 b;
    private Transform canvas;
    //private string canvasName;
    public int start;
    public int end;

    // Use this for initialization
    void Start()
    {
        //canvasName = "MiniGame";
        line_object = new GameObject();
        line_object.transform.parent = this.transform;
        line_object.AddComponent<LineRenderer>();
        line_object.layer = 5;
        lr = line_object.GetComponent<LineRenderer>();
        line_object.transform.position = new Vector3(0,0,0);
        lr.material = new Material(Shader.Find("Particles/Alpha Blended Premultiply"));
        lr.startColor = Color.blue;
        lr.endColor = Color.blue;
        lr.startWidth = 0.1f;
        lr.endWidth = 0.1f;
        canvas = GameObject.Find("MiniGame").transform;
        a = new Vector3(canvas.GetChild(start).transform.position.x, canvas.GetChild(start).transform.position.y, canvas.GetChild(start).transform.position.z - 0.01f);
        b = new Vector3(canvas.GetChild(end).transform.position.x, canvas.GetChild(end).transform.position.y, canvas.GetChild(end).transform.position.z - 0.01f);
        LineHandler.line p;
        p.start_point = a;
        p.end_point = b;
        GameObject.Find("Lines").GetComponent<LineHandler>().line_array.Add(p);
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        a = new Vector3(canvas.GetChild(start).transform.position.x, canvas.transform.GetChild(start).transform.position.y, canvas.transform.GetChild(start).transform.position.z - 1f);
        b = new Vector3(canvas.GetChild(end).transform.position.x, canvas.transform.GetChild(end).transform.position.y, canvas.transform.GetChild(end).transform.position.z - 1f);
        lr.SetPosition(0, a);
        lr.SetPosition(1, b);
    }
}

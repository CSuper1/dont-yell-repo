﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewEnemySpawner : MonoBehaviour {

    public Transform elitePrefab;
    public Transform tankPrefab;
    private int random = 0;
    private int timer = 0;

    public int elitesNum;
    public int tankNum;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        timer++;
        GameObject[] elites = GameObject.FindGameObjectsWithTag("Elite");
        if(elites.Length < elitesNum && timer % 300 == 0)
        {
            random = Random.Range(0, transform.childCount);
            Transform temp = Instantiate(elitePrefab, transform.GetChild(random).transform.position, transform.rotation);
            temp.gameObject.GetComponent<AI>().area = 1;
        }
        GameObject[] tanks = GameObject.FindGameObjectsWithTag("Tank");
        if(tanks.Length < tankNum && timer % 100 == 0)
        {
            random = Random.Range(0, transform.childCount);
            Instantiate(tankPrefab, transform.GetChild(random).transform.position, transform.rotation);
        }
	}
}

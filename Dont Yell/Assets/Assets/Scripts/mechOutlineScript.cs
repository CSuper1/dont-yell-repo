﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class mechOutlineScript : MonoBehaviour {

	public PoweredComponent pc;
	private Image i;
	private Outline o;

	// Use this for initialization
	void Start () {
		i = this.gameObject.GetComponent<Image>();
		o = this.gameObject.GetComponent<Outline> ();
		o.effectColor = Color.black;
	}
	
	// Update is called once per frame
	void Update () {
		if (pc.GetComponentInChildren<onOffComponent> () != null) {
			if (pc.GetComponentInChildren<onOffComponent> ().onOff == true)
				o.effectColor = Color.green;
		}
		else
			o.effectColor = Color.black;

		if (pc.heat >= 85)
			i.color = Color.red;
		else if(pc.heat > 51)
			i.color = new  Color32(255, 125, 0, 255);
		else if(pc.heat > 1)
			i.color = Color.yellow;
		else
			i.color = Color.gray;
	}
}

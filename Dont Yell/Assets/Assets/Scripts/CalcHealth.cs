﻿//Created by Fernando Tapia 2/20/17

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CalcHealth : MonoBehaviour {

	public Slider healthBar;
	HealthScript core;
	float health, healthMax;

	// Use this for initialization
	void Start () {
		core = GameObject.Find("Pilot").GetComponent<HealthScript>();
		healthMax = core.hitPoints;
		healthBar.value = calc();
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		health = core.hitPoints;
		healthBar.value = calc ();
	}

	float calc(){
		return health / healthMax;
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMissileScript : MonoBehaviour {

    private GameObject player;
    private int time;
    private Vector3 p_initialPos;
    private Vector3 e_initialPos;
    private Vector3 newDir;
    public int type;
    private float speed;
    public Transform explosion;

	// Use this for initialization
	void Start () {
        player = GameObject.Find("Pilot");
        time = 0;
        p_initialPos = player.transform.position;
        e_initialPos = this.transform.position;
        speed = 100f;
	}
	
	// Update is called once per frame
	void FixedUpdate () {

        time++;
        if (type != 3)
        {
            if (time == 1)
            {
                if (type == 0)
                {
                    e_initialPos += new Vector3(-10f, 100f, 0);
                }
                else if (type == 1)
                {
                    e_initialPos += new Vector3(10f, 100f, 0);
                }
            }
            else if (time < 50)
            {
                transform.LookAt(e_initialPos);
                transform.position = Vector3.MoveTowards(transform.position, e_initialPos, speed * Time.deltaTime);
                speed = speed * 1.01f;
                if (Vector3.Distance(transform.position, e_initialPos) < 0.1f)
                {
                    time = 50;
                }
            }
            else if (time < 60)
            {
                speed = 100f;
                p_initialPos = player.transform.position;
                newDir = Vector3.RotateTowards(transform.forward, p_initialPos - transform.position, speed * Time.deltaTime / 6f, 0);
                transform.rotation = Quaternion.LookRotation(newDir);
            }
            else if (time > 60)
            {
                transform.rotation = Quaternion.LookRotation(newDir);
                transform.position = Vector3.MoveTowards(transform.position, p_initialPos, speed * Time.deltaTime);
                speed = speed * 1.02f;

                //explosion
                if (Vector3.Distance(transform.position, p_initialPos) < 1f)
                {
                    Instantiate(explosion, transform.position, transform.rotation);
                    damage();
                }
            }
        }
        else
        {
            transform.position = Vector3.MoveTowards(transform.position, p_initialPos, speed * Time.deltaTime);
            if(speed < 300)
                speed = speed * 1.05f;

            //explosion
            if (Vector3.Distance(transform.position, p_initialPos) < 1f)
            {
                Instantiate(explosion, transform.position, transform.rotation);
                damage();
            }

            if (Physics.Raycast(transform.position, transform.forward, 10f))
            {
                Instantiate(explosion, transform.position, transform.rotation);
                damage();
            }

            GameObject[] bullets = GameObject.FindGameObjectsWithTag("Bullet");
            foreach (GameObject b in bullets)
            {
                if(Vector3.Distance(transform.position, b.transform.position) < 10f)
                {
                    Instantiate(explosion, transform.position, transform.rotation);
                    damage();
                }
            }
        }
	}

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == "hitFront" || other.gameObject.name == "hitBack" || other.gameObject.name == "hitLeft" || other.gameObject.name == "hitRight")
        {
            Instantiate(explosion, transform.position, transform.rotation);
            damage();
        }

        else if(other.tag != "Elite")
        {
            damage();
        }
    }

    void damage()
    {
        if (Vector3.Distance(transform.position, player.transform.position) < 20f)
        {
            player.GetComponent<HealthScript>().hitPoints -= 30;
        }
        Destroy(this.gameObject);
    }
}

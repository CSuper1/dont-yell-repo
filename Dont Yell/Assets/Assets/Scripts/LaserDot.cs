﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserDot : MonoBehaviour {
	public GameObject gun;
	public GameObject camera;
	public Material linemat;

	public float scale_factor = 0.01f;

	private LineRenderer line;
	private GameObject dot;

	private void updateline() {
		Vector3[] positions = new Vector3[2];

		RaycastHit hit;
		bool didhit = Physics.Raycast (gun.transform.position, gun.transform.forward, out hit, 1000.0f);

		Vector3 hitpos = hit.point;
		if (!didhit) {
			hitpos = gun.transform.position + gun.transform.forward * 1000.0f;
		}


		float scaleamt = Vector3.Distance(camera.transform.position, hitpos)*scale_factor;

		positions[0] = gun.transform.position;
		positions[1] = hitpos;
		line.SetPositions (positions);

		dot.transform.position = hitpos;

		line.endWidth = line.startWidth*scaleamt*scale_factor*1000.0f;

		dot.transform.localScale = new Vector3 (scaleamt, scaleamt, scaleamt);
	}

	// Use this for initialization
	void Start () {
		line = gameObject.GetComponent<LineRenderer> ();
		line.startWidth = 0.01f;
		line.endWidth = line.startWidth;
		line.startColor = new Color (1.0f, 0.0f, 0.0f);
		line.endColor = line.startColor;
		line.material = linemat;
		dot = transform.Find ("LaserDot").gameObject;
		updateline ();
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		updateline ();
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Control : MonoBehaviour
{

    public bool hacked = false;
    private bool shouldmove = true;
    //public GameObject p;
    public Transform time;
    CharacterController controller;
    public AudioSource footstep;
	public float footstepMeter = 0.0f;
    public float speed = 50f;
    public float rotateSpeed = 1.0f;
    private float curSpeed_1;
    private float curSpeed_2;
    powerLegs legScript;
    //public Transform shield;
    private int sliding;
    private Vector3 s_speed;
    private Vector3 forward;
    private Vector3 right;
    private float up_speed;
    private Vector3 gravity;
    private Slider s;

    // Use this for initialization
    void Start()
    {
        s = GameObject.Find("BoostBar").GetComponent<Slider>();
        up_speed = 1f;
        sliding = 0;
        s_speed = transform.right;
        gravity = new Vector3(0, -0.1f, 0);
        controller = GetComponent<CharacterController>();
        if(GameObject.Find("Legs Component") != null)
            legScript = GameObject.Find("Legs Component").GetComponent<powerLegs>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        /*
        if (OVRInput.Get(OVRInput.Button.One) && GameObject.Find("BoostBar").GetComponent<Image>().fillAmount == 1)
        {
            Instantiate(shield, transform.position + transform.forward * 15, transform.rotation);
            GameObject.Find("BoostBar").GetComponent<Image>().fillAmount = 0;
        }
        */

        if (OVRInput.Get(OVRInput.RawButton.LHandTrigger) && s.value >0)
        {
            //s.value -= 50;
            controller.Move(transform.up * up_speed);
            up_speed = up_speed * 1.01f;
        }
        else
        {
            up_speed = 1f;
        }

        Vector2 primaryAxis = OVRInput.Get(OVRInput.Axis2D.PrimaryThumbstick) * Time.deltaTime * 60;//30;
        Vector2 secondaryAxis = OVRInput.Get(OVRInput.Axis2D.SecondaryThumbstick) * Time.deltaTime * 3;//3;

        if (shouldmove && !(OVRInput.Get(OVRInput.Button.SecondaryHandTrigger) && OVRInput.Get(OVRInput.Button.PrimaryHandTrigger)) && sliding == 0)
        {
            if (!hacked)
            {
                transform.Rotate(new Vector3(0.0f, secondaryAxis.x * 30f, 0.0f));
                GameObject.Find("PlayerDot").transform.Rotate(0.0f, 0.0f, -secondaryAxis.x * 30f);
                forward = transform.TransformDirection(Vector3.forward);
                right = transform.TransformDirection(Vector3.right);
                curSpeed_1 = speed * primaryAxis.y * legScript.speed;
                curSpeed_2 = speed * primaryAxis.x * legScript.speed;

                if (!controller.isGrounded && (!OVRInput.Get(OVRInput.RawButton.LHandTrigger) || s.value == 0))
                {
                    if(gravity.y > -6f)
                        gravity = gravity * 1.05f;
                }
                else
                {
                    gravity = new Vector3(0, -0.2f, 0);
                }
                controller.Move(forward* curSpeed_1 * Time.deltaTime + right* curSpeed_2 * Time.deltaTime + gravity);

                
                footstepMeter += Mathf.Pow(curSpeed_1, 2.0f) + Mathf.Pow(curSpeed_2, 2.0f);
				if(footstepMeter >= 100000.0f){
					if(footstep.panStereo >= 0.0f){
						footstep.panStereo = -0.80f;
					}
					else{
						footstep.panStereo = 0.80f;
					}
					footstep.Play();
					footstepMeter = 0.0f;
				}
                
            }
            else
            {
                transform.Rotate(new Vector3(0.0f, primaryAxis.x* 60f, 0.0f));
                Vector3 forward = transform.TransformDirection(Vector3.forward);
                Vector3 right = transform.TransformDirection(Vector3.right);
                float curSpeed_1 = speed * secondaryAxis.y * legScript.speed;
                float curSpeed_2 = speed * secondaryAxis.x * legScript.speed;
                controller.SimpleMove(forward* curSpeed_1);
                controller.SimpleMove(right* curSpeed_2);
            }
        }

        if (OVRInput.Get(OVRInput.RawButton.A) && sliding == 0 && s.value > 0)
        {
            s_speed = (forward* curSpeed_1 * Time.deltaTime + right* curSpeed_2 * Time.deltaTime) * 10;
            s.value -= 1000;//GameObject.Find("BoostBar").GetComponent<Image>().fillAmount -= 0.20f;
            controller.Move(s_speed);
            sliding = 1;
        }

        if (sliding > 0)
        {
            sliding++;
            controller.Move(s_speed);
            s_speed = s_speed* 0.9f;
        }
        if (sliding > 30)
        {
            sliding = 0;
        }

        if (OVRInput.GetUp(OVRInput.RawButton.B) && GameObject.Find("ECMBar").GetComponent<Slider>().value == 5000)
        {
            Instantiate(time, GameObject.Find("Muzzle").transform.position, GameObject.Find("Muzzle").transform.rotation);
            GameObject.Find("ECMBar").GetComponent<Slider>().value = 0;
        }

    }
}

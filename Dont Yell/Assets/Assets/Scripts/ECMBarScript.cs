﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ECMBarScript : MonoBehaviour {

    private Slider ecm;
    private PoweredComponent pc;
    private int value = 0;

	// Use this for initialization
	void Start () {
        ecm = GetComponent<Slider>();
        pc = GameObject.Find("ECM Component").GetComponent<PoweredComponent>();
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		if(pc.user_power == 0)
        {
            value = 0;
        }
        else if(pc.user_power == 1)
        {
            value = 2;
        }
        else if(pc.user_power == 2)
        {
            value = 4;
        }
        else if(pc.user_power == 3)
        {
            value = 8;
        }
        ecm.value += value;
	}
}

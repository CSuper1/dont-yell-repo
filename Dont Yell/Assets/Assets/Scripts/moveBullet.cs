﻿//Fernando Tapia
//Move the bullet after it is shot

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class moveBullet : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}

	//time this object exists
	int timeIn = 50;

	// Update is called once per frame
	void FixedUpdate () {
		timeIn -= 1;
		transform.Translate (0,0,0.5f);
		if(timeIn <= 0){
			Destroy(this.gameObject);
		}
	}
}

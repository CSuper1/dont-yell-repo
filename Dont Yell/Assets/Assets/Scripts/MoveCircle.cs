﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MoveCircle : MonoBehaviour {

    private float x;
    private float y;
    private Vector2 location;
    private int timer;
    private CharacterController c;
    private GameObject power_cir;
    private GameObject cam;
    private Slider bar;

	// Use this for initialization
	void Start () {
        location = new Vector2(Random.Range(45, 120), Random.Range(-7, 15));
        x = (location.x - transform.position.x) / 100;
        y = (location.y - transform.position.y) / 100;
        timer = 6;
        c = GameObject.Find("Pilot").GetComponent<CharacterController>();
        power_cir = GameObject.Find("PowerCir");
        cam = GameObject.Find("support camera");
        bar = GameObject.Find("BoostBar").GetComponent<Slider>();
    }
	
	// Update is called once per frame
	void FixedUpdate () {

        if(near(location, new Vector2(transform.position.x, transform.position.y)))
        {
            timer = 0;
            location = new Vector2(Random.Range(60f, 120f), Random.Range(-7f, 15f));
        }

        if(timer < 20)
        {
            timer++;
            x = (location.x - transform.position.x) / 100;
            y = (location.y - transform.position.y) / 100;
        }
        else
        {
            transform.position += new Vector3(x, y, 0);
            x = x * 1.1f;
            y = y * 1.1f;
        }

        if(Vector3.Distance(transform.position, power_cir.transform.position) < 10f && CaminPos(cam.transform.position) && Input.GetMouseButton(0))
        {
            GetComponent<Image>().color = Color.red;
            GetComponent<LineRenderer>().SetPosition(0, transform.position + new Vector3(0, 0, -1f));
            GetComponent<LineRenderer>().SetPosition(1, power_cir.transform.position + new Vector3(0, 0, -1f));
            if (c.isGrounded)
            {
                bar.value += 25;
            }
        }
        else
        {
            GetComponent<Image>().color = Color.blue;
            GetComponent<LineRenderer>().SetPosition(0, new Vector3(0, 0, 0));
            GetComponent<LineRenderer>().SetPosition(1, new Vector3(0, 0, 0));
        }
    }

    bool CaminPos(Vector3 v)
    {
        return v.x > 80 && v.x < 90 && v.y > -1 && v.y < 1;
    }

    bool near(Vector2 a, Vector2 b)
    {
        return a.x - b.x < 1f && a.x - b.x > -1f && a.y - b.y < 1f && a.y - b.y > -1f;
    }

}

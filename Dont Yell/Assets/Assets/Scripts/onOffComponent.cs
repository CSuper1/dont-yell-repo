﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class onOffComponent : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler {

	public bool onOff;
	private GameObject oButton;
	private Text t;
	private Image com;
	private PoweredComponent pc;

	// Use this for initialization
	void Start () {
		onOff = false;
		oButton = this.gameObject;
		t = oButton.GetComponentInChildren<Text>();
		com = oButton.transform.parent.gameObject.GetComponentInChildren<Image> ();
		pc = oButton.transform.parent.gameObject.GetComponent<PoweredComponent>();
	}
	
	// Update is called once per frame
	public void onOffFun() {
		if (!onOff) {
			oButton.GetComponent<Image> ().color = Color.green;
			com.GetComponent<Outline> ().effectColor = Color.green;
			t.text = "ON";
			onOff = true;
		} else {
			oButton.GetComponent<Image> ().color = Color.gray;
			com.GetComponent<Outline> ().effectColor = Color.gray;
			t.text = "OFF";
			onOff = false;
			pc.user_power = 0;
		}
	}

	void Update(){
		if (!onOff)
			pc.user_power = 0;
	}

	public void OnPointerEnter(PointerEventData eventData){
		oButton.GetComponent<Outline>().effectColor = Color.magenta;

	}

	public void OnPointerExit(PointerEventData eventData){
		oButton.GetComponent<Outline>().effectColor = Color.black;	
	}
}

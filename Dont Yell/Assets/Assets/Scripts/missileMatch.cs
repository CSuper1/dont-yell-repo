﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class missileMatch : MonoBehaviour
{

    public Image partA, partB, partC, partD, partE, partF, ansA, ansB, ansC;
    public int numA, numB, numC, guessA, guessB, guessC;
    public int[] guesses;
    public MissileStatus[] missileTubes;
    private int missileCount = 0;
    public int getLoaded()
    {
        return missileCount;
    }
    //int maxMissCount = 16;
    int counter;
    Text loadCount;
    Text textA, textB, textC;
	public Image iA, iB, iC;
    Text doorText;
    public MissileUIController doorOC;

	Image fill;
	RectTransform fillButton;

    // Use this for initialization
    void Start()
    {
        guesses = new int[3];
        counter = 0;
        loadCount = GameObject.Find("loadCountText").GetComponent<Text>();
        textA = GameObject.Find("aText").GetComponent<Text>();
        textB = GameObject.Find("bText").GetComponent<Text>();
        textC = GameObject.Find("cText").GetComponent<Text>();
		iA = GameObject.Find("ansA").GetComponent<Image>();
		iB = GameObject.Find("ansB").GetComponent<Image>();
		iC = GameObject.Find("ansC").GetComponent<Image>();
        doorText = GameObject.Find("doorStatus").GetComponent<Text>();
        loadCount.text = "0";
        doorOC = GameObject.Find("MissileLoading").GetComponent<MissileUIController>();
		fill = GameObject.Find ("buttonFill").GetComponent<Image>();
		fillButton = GameObject.Find ("buttonFill").GetComponent<RectTransform>();
		randOrder();
    }

    // Update is called once per frame
	void FixedUpdate()
    {

		if (counter == 3)
			counter = 0;

		if (guesses [0] != 0)
			iA.color = Color.white;
		if (guesses [1] != 0)
			iB.color = Color.white;
		if (guesses [2] != 0)
			iC.color = Color.white;

		if (Input.GetKeyDown("q"))
		{
			guesses[counter] = 1;
			counter++;
		}
		else if (Input.GetKeyDown("w"))
		{
			guesses[counter] = 2;
			counter++;
		}
		else if (Input.GetKeyDown("e"))
		{
			guesses[counter] = 3;
			counter++;
		}
		else if (Input.GetKeyDown("a"))
		{
			guesses[counter] = 4;
			counter++;
		}
		else if (Input.GetKeyDown("s"))
		{
			guesses[counter] = 5;
			counter++;
		}
		else if (Input.GetKeyDown("d"))
		{
			guesses[counter] = 6;
			counter++;
		}

        if (missileTubes[0].missileReady == false)
            missileCount = 0;
        loadCount.text = missileCount.ToString();
		if (guesses [0] != 0)
			textA.text = guesses [0].ToString ();
		else
			textA.text = numA.ToString();
		if (guesses [1] != 0)
			textB.text = guesses [1].ToString ();
		else
			textB.text = numB.ToString();
		if (guesses [2] != 0)
			textC.text = guesses [2].ToString ();
		else
			textC.text = numC.ToString();
        if (doorOC.doorsOpen)
        {
            doorText.text = "OPEN";
        }
        else
        {
            doorText.text = "CLOSED";
        }
    }

    void randOrder()
    {
		guesses [0] = 0;
		guesses [1] = 0;
		guesses [2] = 0;
        numA = Random.Range(1, 6);
        numB = Random.Range(1, 6);
        numC = Random.Range(1, 6);
		iA.color = Color.grey;
		iB.color = Color.grey;
		iC.color = Color.grey;
    }

    public void guess(string guessNum)
    {
        if (counter == 3)
            counter = 0;

        if (string.Equals(guessNum, "A"))
        {
            guesses[counter] = 1;
            counter++;
        }
        else if (string.Equals(guessNum, "B"))
        {
            guesses[counter] = 2;
            counter++;
        }
        else if (string.Equals(guessNum, "C"))
        {
            guesses[counter] = 3;
            counter++;
        }
        else if (string.Equals(guessNum, "D"))
        {
            guesses[counter] = 4;
            counter++;
        }
        else if (string.Equals(guessNum, "E"))
        {
            guesses[counter] = 5;
            counter++;
        }
        else if (string.Equals(guessNum, "F"))
        {
            guesses[counter] = 6;
            counter++;
        }
    }

    //in here you would load the missile when the if statement is true
    public void checkGuess()
    {
        if (guesses[0] == numA && guesses[1] == numB && guesses[2] == numC && !doorOC.doorsOpen)
        {
            if (missileCount < 8)
            {
                randOrder();
                if (missileTubes[0].missileReady == false)
                    missileCount = 0;

                missileTubes[missileCount].startLoadingMissile();
                missileCount++;
            }
        }
        else
        {
			randOrder();
            counter = 0;
        }
    }
		


}

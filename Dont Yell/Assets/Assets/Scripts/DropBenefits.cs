﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DropBenefits : MonoBehaviour {

    [SerializeField] private GameObject box;
    private GameObject _box;
    Vector3 dropPoint;
    Vector3 p1;
    Vector3 p2;
    Vector3 p3;
    Vector3 p4;
    RaycastHit hit;
    RaycastHit h1;
    RaycastHit h2;
    RaycastHit h3;
    RaycastHit h4;
    float x;
    float z;
    bool found = false;

    // Use this for initialization
    void Start () {
        x = UnityEngine.Random.Range(-200f, 200f);
        z = UnityEngine.Random.Range(-200f, 200f);
    }
	
	// Update is called once per frame
	void Update () {
        while (!found)
        {
            dropPoint = new Vector3(x, 100f, z);
            p1 = new Vector3(x + 1f, 100f, z + 1f);
            p2 = new Vector3(x + 1f, 100f, z - 1f);
            p3 = new Vector3(x - 1f, 100f, z + 1f);
            p4 = new Vector3(x - 1f, 100f, z - 1f);
            if (Physics.Raycast(dropPoint, Vector3.down, out hit, 200f) && Physics.Raycast(p1, Vector3.down, out h1, 200f)
                && Physics.Raycast(p2, Vector3.down, out h2, 200f) && Physics.Raycast(p3, Vector3.down, out h3, 200f)
                && Physics.Raycast(p4, Vector3.down, out h4, 200f))
            {
                _box = Instantiate(box) as GameObject;
                _box.transform.position = new Vector3(hit.point.x, hit.point.y + 20f, hit.point.z);
                _box.GetComponent<Rigidbody>().velocity = new Vector3(0, -40f, 0);
                found = true;
            }
            else
            {
                x = UnityEngine.Random.Range(-200f, 200f);
                z = UnityEngine.Random.Range(-200f, 200f);
            }
        }

    }
}

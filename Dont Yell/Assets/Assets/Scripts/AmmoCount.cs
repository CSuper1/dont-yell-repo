﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AmmoCount : MonoBehaviour {

    //private Renderer r;
    private float health;
    private HealthScript core;
    private string text = "";
    private int weaponIndex;

    // Use this for initialization
    void Start () {
        /*
        r = GetComponent<Renderer>();
        r.material = new Material(Shader.Find("Transparent/Diffuse"));
        r.material.color = new Color(1, 0, 0, 0.5f);
        */
        core = GameObject.Find("Pilot").GetComponent<HealthScript>();
    }
	
	// Update is called once per frame
	void FixedUpdate () {
        weaponIndex = GameObject.Find("Pilot").GetComponent<WeaponChangeScript>().weaponIndex;
        if (weaponIndex == 1)
            text = "Machine Gun  " + GameObject.Find("Gun").GetComponent<GeneralShootingScript>().magazine.ToString();
        else if (weaponIndex == 2)
            text = "Laser Gun  " + GameObject.Find("LaserPink").GetComponent<GeneralShootingScript>().magazine.ToString();
        else if (weaponIndex == 3)
            text = "Laser Gun  " + GameObject.Find("LaserYellow").GetComponent<GeneralShootingScript>().magazine.ToString();
        else if (weaponIndex == 4)
            text = "Laser Gun  " + GameObject.Find("LaserCyan").GetComponent<GeneralShootingScript>().magazine.ToString();
        else if (weaponIndex == 5)
            text = "Rocket  " + GameObject.Find("TestFireController2").GetComponent<GeneralShootingScript>().magazine.ToString();
        else
            text = "";
        GetComponent<TextMesh>().text = text;
        health = GameObject.Find("Pilot").GetComponent<HealthScript>().hitPoints;
        GameObject.Find("HealthText").GetComponent<Text>().text = health.ToString();
        GameObject.Find("Bar").GetComponent<Image>().fillAmount = health / 1000;
        GameObject.Find("ArmorCanvas_F").transform.GetChild(1).GetComponent<Image>().fillAmount = core.armorF / 100;
        GameObject.Find("ArmorCanvas_B").transform.GetChild(1).GetComponent<Image>().fillAmount = core.armorB / 100;
        GameObject.Find("ArmorCanvas_L").transform.GetChild(1).GetComponent<Image>().fillAmount = core.armorL / 100;
        GameObject.Find("ArmorCanvas_R").transform.GetChild(1).GetComponent<Image>().fillAmount = core.armorR / 100;
        //GetComponent<TextMesh>().text += "\n" + "Laser" + GameObject.Find("LaserGun").GetComponent<GeneralShootingScript>().magazine.ToString();
        //GetComponent<TextMesh>().text += "\n" + "Rocket" + GameObject.Find("RocketLauncher").GetComponent<GeneralShootingScript>().magazine.ToString();
    }
}

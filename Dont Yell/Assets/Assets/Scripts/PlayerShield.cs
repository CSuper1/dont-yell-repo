﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShield : MonoBehaviour
{

    public float shield, armor;
    PlayerHealth ph;

    // Use this for initialization
    void Start()
    {
        ph = GameObject.Find("Pilot").GetComponent<PlayerHealth>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerEnter(Collider other)
    {
        if(other.tag == "EnemyBullet")
        {
            other.transform.position = new Vector3(0, 2000, 0);
            shield -= 17f;
            if(shield < 0)
            {
                armor += shield;
                shield = 0;
            }
            if(armor < 0)
            {
                ph.hitPoints += armor;
                armor = 0;
            }
        }
    }
}

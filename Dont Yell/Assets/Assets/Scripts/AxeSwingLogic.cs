﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AxeSwingLogic : MonoBehaviour {

    public float minAngle;
    public float maxAngle;
    public float currAngle;
    public float rotateAmount;

	// Use this for initialization
	void Start () {
        minAngle = -60.0f;
        maxAngle = 60.0f;
        transform.localEulerAngles = new Vector3(0, 0, Random.Range(minAngle + 10, maxAngle - 10));
        currAngle = Quaternion.Angle(Quaternion.identity, transform.rotation);
        rotateAmount = 35.0f;
	}
	
	// Update is called once per frame
	void Update () {
        currAngle = Quaternion.Angle(Quaternion.identity, transform.rotation);
        if (currAngle > maxAngle || currAngle < minAngle)
        {
            rotateAmount *= -1;
        }
        transform.Rotate(Vector3.forward * rotateAmount * Time.deltaTime);
	}

}

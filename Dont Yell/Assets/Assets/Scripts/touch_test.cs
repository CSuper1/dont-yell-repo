﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class touch_test : MonoBehaviour {

	public OVRInput.Controller which;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		transform.localPosition = OVRInput.GetLocalControllerPosition (which);
		transform.localRotation = OVRInput.GetLocalControllerRotation (which);
	}
}

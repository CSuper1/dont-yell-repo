﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LineHandler : MonoBehaviour {

    public List<line> line_array = new List<line>();
    public List<int> intersect_array;
    public bool no_line_intersect;
    private line l;
    private string level_name;
    private int line_array_count;
    public int timer;
    private DrawLine[] m;
    private MiniGameScript miniGame;

    public struct line
    {
        public Vector3 start_point;
        public Vector3 end_point;
    }

    // Use this for initialization
    void Start () {
        l = new line();
        timer = 0;
        miniGame = GameObject.Find("MiniGame").GetComponent<MiniGameScript>();
    }
	
	// Update is called once per frame
	void FixedUpdate () {
        no_line_intersect = true;

        if(miniGame.level == 1)
        {
            level_name = "Level_1";
            line_array_count = 8;
        }
        else if (miniGame.level == 2)
        {
            level_name = "Level_2";
            line_array_count = 12;
        }
        else if (miniGame.level == 3)
        {
            level_name = "Level_3";
            line_array_count = 12;
        }
        else if (miniGame.level == 4)
        {
            level_name = "Level_4";
            line_array_count = 12;
        }
        else if (miniGame.level == 5)
        {
            level_name = "Level_5";
            line_array_count = 15;
        }
        else if (miniGame.level == 6)
        {
            level_name = "Level_6";
            line_array_count = 16;
        }
        else if (miniGame.level == 7)
        {
            level_name = "Level_7";
            line_array_count = 0;
        }

        if (timer > 2)
        {
            for (int i = 0; i < line_array_count; i++)
            {
                l.start_point = GameObject.Find(level_name).transform.GetChild(i).GetComponent<LineRenderer>().GetPosition(0);
                l.end_point = GameObject.Find(level_name).transform.GetChild(i).GetComponent<LineRenderer>().GetPosition(1);
                line_array[i] = l;
                changeColor(i, Color.white);
            }
            for (int i = 0; i < line_array_count; i++)
            {
                for (int j = i + 1; j < line_array_count; j++)
                {
                    if (not_same_point(line_array[i], line_array[j]) && PowerSystem.AreLineSegmentsCrossing(line_array[i].start_point, line_array[i].end_point, line_array[j].start_point, line_array[j].end_point))
                    {
                        changeColor(i, Color.red);
                        changeColor(j, Color.red);
                        no_line_intersect = false;
                    }
                }
            }
        }
        else
        {
            timer++;
        }
        
    }

    void changeColor(int a, Color c)
    {
        GameObject.Find(level_name).transform.GetChild(a).GetComponent<LineRenderer>().startColor = c;
        GameObject.Find(level_name).transform.GetChild(a).GetComponent<LineRenderer>().endColor = c;
    }

    bool not_same_point(line a, line b)
    {
        if (a.start_point == b.start_point || a.start_point == b.end_point || a.end_point == b.start_point || a.end_point == b.end_point)
            return false;
        return true;
    }
}

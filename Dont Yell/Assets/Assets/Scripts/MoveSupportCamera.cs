﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveSupportCamera : MonoBehaviour {

	public float dragSpeed = 2.0f;
	private Vector3 dragO;
	private bool pan;
	public Camera cam;
	public float boundL = -10f;
	public float boundR = 10f;
	public float boundB = -10f;
	public float boundT = 10f;

	void Start(){
		boundL += cam.transform.position.x;
		boundR += cam.transform.position.x;
		boundB += cam.transform.position.y;
		boundT += cam.transform.position.y;
	}

	void FixedUpdate(){
		if(Input.GetMouseButtonDown(2)){
			dragO = Input.mousePosition;
			pan = true;
		}

		if (!Input.GetMouseButton (2)) {
			pan = false;
		}

		if (pan) {
			Vector3 pos = cam.ScreenToViewportPoint (Input.mousePosition - dragO);
			Vector3 move = new Vector3 (pos.x * dragSpeed, pos.y * dragSpeed, 0);


			cam.transform.Translate (move, Space.World);

			cam.transform.position = new Vector3 (Mathf.Clamp(cam.transform.position.x, boundL, boundR),Mathf.Clamp(cam.transform.position.y, boundB, boundT),cam.transform.position.z);

		}
	}
}

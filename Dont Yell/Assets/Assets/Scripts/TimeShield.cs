﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeShield : MonoBehaviour {

    private bool expand = false;
    private bool moving = true;
    private int timer = 0;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        timer++;
        if(timer > 1000)
        {
            Destroy(this.gameObject);
        }

        if(moving)
            transform.Translate(transform.forward);

        if (OVRInput.GetUp(OVRInput.RawButton.B))
        {
            expand = true;
        }

        if (expand && transform.localScale.x < 3)
        {
            GameObject.Find("BulletManagement").GetComponent<BulletManagement>().time = true;
            GameObject.Find("BulletManagement").GetComponent<BulletManagement>().timeLocation = transform.position;
            transform.localScale += new Vector3(0.1f, 0.1f, 0.1f);
            moving = false;
        }
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Laservisibility : MonoBehaviour {
	public GameObject pilot;
	private WeaponChangeScript weaponsystem = null;
	public int weaponmin = -1;
	public int weaponmax = -1;
	private MeshRenderer renderer;

	public Color[] colors;

	// Use this for initialization
	void Start () {
		weaponsystem = pilot.GetComponent<WeaponChangeScript> ();
		renderer = GetComponent<MeshRenderer> ();
	}

	// Update is called once per frame
	void FixedUpdate () {
		renderer.enabled = weaponsystem.weaponIndex >= weaponmin && weaponsystem.weaponIndex <= weaponmax;
        if(weaponsystem.weaponIndex-weaponmin > -1 && weaponsystem.weaponIndex-weaponmin < 3)
		    renderer.material.SetColor ("_EmissionColor", colors[weaponsystem.weaponIndex-weaponmin]);
	}
}

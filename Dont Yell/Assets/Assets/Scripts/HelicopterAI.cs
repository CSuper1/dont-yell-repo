﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HelicopterAI : MonoBehaviour {

    public Transform bullet;
    public Transform explosion;
    private float enemyReloadTime = 3.6f;
    private bool allowfire = true;
    private int magCap = 20;
    private int enemyMag = 20;

    private GameObject player;
    private bool reach = false;
    private Vector3 targetPostition;
    private float speed;
    private int timer = 0;
    private int r;

    // Use this for initialization
    void Start () {
        player = GameObject.Find("Pilot");
    }
	
	// Update is called once per frame
	void FixedUpdate () {
        timer++;

        if (GameObject.Find("TimeShield(Clone)") && Vector3.Distance(GameObject.Find("TimeShield(Clone)").transform.position, transform.position) < 130f)
        {
            speed = 600;
        }
        else
        {
            speed = 180;
        }

        if (!Physics.Linecast(transform.position, player.transform.position, 3))
        {
            targetPostition = new Vector3(player.transform.position.x, this.transform.position.y, player.transform.position.z);
            this.transform.LookAt(targetPostition);
            this.transform.GetChild(2).LookAt(player.transform.position);
            //this.transform.LookAt(player.transform.position);
            reach = true;
        }

        if (Vector3.Distance(transform.position, player.transform.position) > 800f || Physics.Linecast(transform.position, player.transform.position, 3))
        {
            transform.Translate(new Vector3((transform.position.x - player.transform.position.x) / speed, 0, (transform.position.z - player.transform.position.z) / speed));
        }
        else
        {
            if(timer == 100)
                r = Random.Range(0, 2);
            if (timer > 100)
            {
                if (r == 1)
                    transform.Translate(1, 0, 0);
                else
                    transform.Translate(-1, 0, 0);
                transform.Rotate(transform.forward * Time.deltaTime, Space.Self);
            }
            if (timer == 160)
                timer = 0;

            //fire
            if (allowfire)
            {
                StartCoroutine(Fire());
            }
            if (enemyMag <= 0)
            {
                StartCoroutine(Reload());
                allowfire = false;
            }
        }

        GameObject[] bullets = GameObject.FindGameObjectsWithTag("Bullet");
        foreach (GameObject b in bullets)
        {
            if (Vector3.Distance(transform.position, b.transform.position) < 20f && b.name == "Bu(Clone)")
            {
                Instantiate(explosion, transform.position, transform.rotation);
                Destroy(this.gameObject);
            }
        }
    }

    IEnumerator Fire()
    {
        allowfire = false;
        if (tag == "Elite")
            Instantiate(bullet, transform.position + transform.forward * 30 + transform.up * 20, transform.rotation);
        else if (tag == "Grunt")
            Instantiate(bullet, transform.position + transform.forward * 2, transform.rotation);
        else if (tag == "Tank")
            Instantiate(bullet, transform.GetChild(1).transform.position + transform.GetChild(1).transform.forward, transform.GetChild(1).transform.rotation);
        else if (tag == "Helicopter")
            Instantiate(bullet, this.transform.GetChild(2).position + this.transform.GetChild(2).forward * 20, this.transform.GetChild(2).rotation);
        enemyMag--;
        float shootTime = 1;
        if (tag != "Hacker" && tag != "Grunt") shootTime = 0.7f + Random.Range(-0.3f, 0.3f);
        if (tag == "Tank") shootTime = 2;
        yield return new WaitForSeconds(shootTime);
        allowfire = true;
    }

    //Reload for the enemy
    IEnumerator Reload()
    {
        yield return new WaitForSeconds(enemyReloadTime);
        enemyMag = magCap;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletManagement : MonoBehaviour {

    private GameObject[] bullets;
    public Transform bullet;
    public bool time = false;
    public Vector3 timeLocation;
    private RaycastHit hit;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        bullets = GameObject.FindGameObjectsWithTag("EnemyBullet");

        if (bullets.Length < 100)
        {
                Transform temp = Instantiate(bullet, new Vector3(0, 800, 0), transform.rotation);
                temp.SetParent(transform);
        }

        foreach(GameObject b in bullets)
        {
            if (time && Vector3.Distance(b.transform.position, timeLocation) < 130f)
                b.transform.Translate(0, 0, 0.5f);
            else if (b.transform.position.y < 2000)
                b.transform.Translate(0, 0, 10f);
        }
	}
}

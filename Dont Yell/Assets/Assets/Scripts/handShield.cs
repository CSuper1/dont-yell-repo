﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class handShield : MonoBehaviour {

	MeshCollider hs;
	public int hitpts;
	public handShield h;
	int maxHits = 5;
	public bool startRegen = true;
	public bool takeHit = false;
	private bool tookHit = false;
    GameObject shieldM;
	// Use this for initialization
	void Start () {
		hs = this.gameObject.GetComponent<Collider>() as MeshCollider;
		h = GameObject.Find ("Shield Model").GetComponent<handShield> ();
        shieldM = GameObject.Find("Shield Model");
        hitpts = maxHits;
		InvokeRepeating ("regen", 1, 1);
        shieldM.transform.localScale = new Vector3(10, 10, 10);
    }
	
	// Update is called once per frame
	void FixedUpdate () {
        shieldM.transform.localScale = new Vector3(30, 30, 30);
        if (OVRInput.Get(OVRInput.RawButton.LIndexTrigger)) {
            if (hitpts > 0)
            {
                hs.enabled = true;
                shieldM.transform.localScale = new Vector3(100,100,100);
            }
            else
            {
                hs.enabled = false;
                
            }
		}
		else
			hs.enabled = false;
		
		if (takeHit) {
			takeHit = false;
			tookHit = true;
			CancelInvoke ();
			Invoke ("stopRegen", 5);
			InvokeRepeating ("regen", 1, 1);
		}

	}

	void regen(){
		if (startRegen && hitpts < maxHits)
			hitpts++;
	}

	void stopRegen(){
		h.startRegen = true;
	} 

	public bool readHit()
	{
		if (tookHit)
		{
			tookHit = false;
			return true;
		}

		return false;
	}

}

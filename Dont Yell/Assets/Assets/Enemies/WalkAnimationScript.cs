﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WalkAnimationScript : MonoBehaviour {
	private Vector3 lastposition;

	public float walk_speed = 2.0f;

	public GameObject anim_obj;
	private Animator animator;	

	// Use this for initialization
	void Start () {
		animator = anim_obj.GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
		float speed = Vector3.Magnitude(transform.position - lastposition);
        float spd = walk_speed * speed;
        if (spd < 0.5f) spd = 0.0f;
        else spd = 1.0f;
        animator.speed = spd;

		lastposition = transform.position;
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class uiZoom : MonoBehaviour {

	public Camera cam;
	public GameObject cOrigin, PowerAnchor, MissileAnchor, RadarAnchor;
	CameraRaycast crScript;
	// Use this for initialization
	void Start () {
		crScript = GameObject.Find ("support camera").GetComponent<CameraRaycast> ();
		cam.transform.localPosition = cOrigin.transform.position;
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.Q)) {
			crScript.expand = true;
			cam.orthographicSize = 8;
			cam.transform.localPosition = PowerAnchor.transform.position;
		}
		if (Input.GetKeyDown (KeyCode.W)) {
			crScript.expand = true;
			cam.orthographicSize = 7;
			cam.transform.localPosition = MissileAnchor.transform.position;
		}
		if (Input.GetKeyDown (KeyCode.E)) {
			crScript.expand = true;
			cam.orthographicSize = 3;
			cam.transform.localPosition = RadarAnchor.transform.position;
		}
		if (Input.GetKeyDown (KeyCode.Space)) {
			crScript.expand = false;
			cam.orthographicSize = 14;
			cam.transform.localPosition = cOrigin.transform.position;
		}
	}
}
